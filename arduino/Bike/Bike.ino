#include <Arduino.h>
#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>
#include <ArduinoJson.h> // https://github.com/bblanchon/ArduinoJson
#define USE_SERIAL Serial

#define RELAY_PIN_1 2
#define RELAY_PIN_2 4

WiFiMulti wifiMulti;

void setup() {
  USE_SERIAL.begin(921600);

  USE_SERIAL.println();
  USE_SERIAL.println();
  USE_SERIAL.println();

  for (uint8_t t = 4; t > 0; t--) {
    USE_SERIAL.printf("[SETUP] WAIT %d...\n", t);
    USE_SERIAL.flush();
    delay(1000);
  }

  //

  pinMode(RELAY_PIN_1, OUTPUT);
  pinMode(RELAY_PIN_2, OUTPUT);

  //
  
  wifiMulti.addAP("kewtea_pad", "50226436");

}

void on9v() {
  digitalWrite(RELAY_PIN_1, HIGH);
}

void off9v() {
  digitalWrite(RELAY_PIN_1, LOW);
}

void signal19v() {
  digitalWrite(RELAY_PIN_2, LOW);
  delay(100);
  digitalWrite(RELAY_PIN_2, HIGH);
  delay(5000);
  digitalWrite(RELAY_PIN_2, LOW);
}

void updateReq(long ids[], int idsSize) {
USE_SERIAL.println("updateReq();");
  
  String postData ="{\"ids\":[";
  for (int i=0; i<idsSize; i++) {
    long id = ids[i];
    postData += id;
    if (i < idsSize-1) {
      postData += ",";
    }
  }
  postData += "]}";

  HTTPClient http;
  http.begin("http://34.85.118.236/api/bike_request_complete");
  http.addHeader("Content-Type", "text/plain");
  int httpCode = http.POST(postData);
  if (httpCode > 0) {
    String response = http.getString();
    USE_SERIAL.println(response);
  } else {
    USE_SERIAL.printf("updateReq HTTP ... failed, error: %s\n", http.errorToString(httpCode).c_str());
  }

  http.end();
}

void checkReq() {
  USE_SERIAL.println("checkReq();");
  
  HTTPClient http;
  http.begin("http://34.85.118.236/api/bike_request");
  int httpCode = http.GET();
  
  // httpCode will be negative on error
  if (httpCode > 0) {
    if (httpCode == HTTP_CODE_OK) {
      String payload = http.getString();
      USE_SERIAL.println(payload);
      
      // prase to Json
      DynamicJsonDocument doc(1024);
      deserializeJson(doc, payload);
      
      JsonArray requestList = doc.as<JsonArray>();
      if (requestList.size() > 0) {
        long ids[requestList.size()];
        int index = 0;
        for (JsonVariant request : requestList) {
          JsonObject object = request.as<JsonObject>();

          String requestType = object["requestType"];

          if (requestType.equals("saddle_open")) {
            signal19v();
          }
          if (requestType.equals("9v_on")) {
            on9v();
          }
          if (requestType.equals("9v_off")) {
            off9v();
          }
          
          long id = object["id"];
          ids[index] = id;
          index++;
        }
        updateReq(ids, requestList.size());
      }
      
    } else {
      USE_SERIAL.println("checkDoorOpenReq // httpCode != HTTP_CODE_OK");
    }
  } else {
    USE_SERIAL.printf("checkReq HTTP ... failed, error: %s\n", http.errorToString(httpCode).c_str());
  }
  
  http.end();
}

void loop() {
  if ((wifiMulti.run() == WL_CONNECTED)) {
    checkReq();
  }
  delay(2500);
}
