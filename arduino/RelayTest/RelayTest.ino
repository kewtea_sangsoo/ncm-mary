#define RELAY_PIN_1 2
#define RELAY_PIN_2 4

//

void setup() {
  Serial.begin(921600);

  Serial.println();
  Serial.println();
  Serial.println();

  for (uint8_t t = 4; t > 0; t--) {
    Serial.printf("[SETUP] WAIT %d...\n", t);
    Serial.flush();
    delay(1000);
  }

  //

  pinMode(RELAY_PIN_1, OUTPUT);
  pinMode(RELAY_PIN_2, OUTPUT);
//  pinMode(RELAY_PIN_3, OUTPUT);
  
}

void loop() {

  digitalWrite(RELAY_PIN_1, LOW);
  digitalWrite(RELAY_PIN_2, LOW);
//  digitalWrite(RELAY_PIN_3, LOW);
  delay(3000);
  digitalWrite(RELAY_PIN_1, HIGH);
  digitalWrite(RELAY_PIN_2, HIGH);
//  digitalWrite(RELAY_PIN_3, HIGH);
  delay(3000);
  
}
