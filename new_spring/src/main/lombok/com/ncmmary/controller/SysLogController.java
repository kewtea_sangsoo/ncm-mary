package com.ncmmary.controller;

import com.ncmmary.entity.SysLog;
import com.ncmmary.entitymanager.SysLogManager;
import com.ncmmary.parameters.RegisterSysLogForm;
import com.ncmmary.parameters.UpdateSysLogForm;
import com.ncmmary.parameters.DeleteSysLogsForm;
import com.ncmmary.parameters.GetSysLogsParameters;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.http.MediaType;

import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Data;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TimeZone;
import java.util.Locale;

import springfox.documentation.annotations.ApiIgnore;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiModelProperty;

@Api(tags = {"SysLog"})
@Controller
public class SysLogController{

    private static final Logger log = LoggerFactory.getLogger(SysLogController.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private SysLogManager sysLogManager;

    @ApiOperation(value = "${ncmmary.doc.registerSysLog.summary}", notes = "${ncmmary.doc.registerSysLog.notes}", tags = {"SysLog"})
    @RequestMapping(path={"/api/syslog/register"}, method={RequestMethod.POST}, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public RegisterSysLogRestApiResponseBody registerSysLog(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute RegisterSysLogForm registerSysLogForm, BindingResult bindingResult, @ApiIgnore Locale locale){
        SysLog sysLog = null;

        if(!bindingResult.hasErrors()){
            sysLog = sysLogManager.register(registerSysLogForm, bindingResult);
        }

        RegisterSysLogRestApiResponseBody responseBody 
            = new RegisterSysLogRestApiResponseBody(bindingResult, messageSource, locale);

        responseBody.setSysLog(sysLog);

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class RegisterSysLogRestApiResponseBody extends RestApiResponseBody
    {
        @ApiModelProperty(value = "${ncmmary.doc.registerSysLog.response.sysLog}")
        private SysLog sysLog;

        public RegisterSysLogRestApiResponseBody() {
            super();
        }

        public RegisterSysLogRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }

    @ApiOperation(value = "${ncmmary.doc.updateSysLog.summary}", notes = "${ncmmary.doc.updateSysLog.notes}")
    @RequestMapping(path={"/api/syslog/update"}, method={RequestMethod.POST}, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public UpdateSysLogRestApiResponseBody updateSysLog(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute UpdateSysLogForm updateSysLogForm, BindingResult bindingResult, @ApiIgnore Locale locale){

        SysLog sysLog = null;

        if(!bindingResult.hasErrors()){
            sysLog = sysLogManager.update(updateSysLogForm, bindingResult);
        }

        UpdateSysLogRestApiResponseBody responseBody
            = new UpdateSysLogRestApiResponseBody(bindingResult, messageSource, locale);

        responseBody.setSysLog(sysLog);

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class UpdateSysLogRestApiResponseBody extends RestApiResponseBody
    {
        @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.response.sysLog.desc}")
        private SysLog sysLog;

        public UpdateSysLogRestApiResponseBody() {
            super();
        }

        public UpdateSysLogRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }

    @ApiOperation(value = "${ncmmary.doc.deleteSysLogs.summary}", notes = "${ncmmary.doc.deleteSysLogs.notes}", tags = {"SysLog"})
    @RequestMapping(path={"/api/syslog/delete"}, method={RequestMethod.POST}, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public DeleteSysLogsRestApiResponseBody deleteSysLogs(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute DeleteSysLogsForm deleteSysLogsForm, BindingResult bindingResult, @ApiIgnore Locale locale){

        if(!bindingResult.hasErrors()){
            sysLogManager.delete(deleteSysLogsForm, bindingResult);
        }

        DeleteSysLogsRestApiResponseBody responseBody
            = new DeleteSysLogsRestApiResponseBody(bindingResult, messageSource, locale);

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class DeleteSysLogsRestApiResponseBody extends RestApiResponseBody
    {

        public DeleteSysLogsRestApiResponseBody() {
            super();
        }

        public DeleteSysLogsRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }

    @ApiOperation(value = "${ncmmary.doc.getSysLogs.summary}", notes = "${ncmmary.doc.getSysLogs.notes}", tags = {"SysLog"})
    @RequestMapping(path={"/api/syslogs"}, method={RequestMethod.GET}, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public GetSysLogsRestApiResponseBody getSysLogs(@Valid @ModelAttribute GetSysLogsParameters params, BindingResult bindingResult, @ApiIgnore Locale locale){

        SysLogManager.SysLogList sysLogList = null;

        if(!bindingResult.hasErrors()){
             sysLogList = sysLogManager.getSysLogs(params);
        }

        GetSysLogsRestApiResponseBody responseBody
            = new GetSysLogsRestApiResponseBody(bindingResult, messageSource, locale);
        responseBody.setSysLogs(sysLogList.getSysLogs());
        responseBody.setParams(params);
        responseBody.setIsLoading(sysLogList.getIsLoading());
        responseBody.setPrev(sysLogList.getPrev());
        responseBody.setNext(sysLogList.getNext());

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class GetSysLogsRestApiResponseBody extends RestApiResponseBody
    {
        @ApiModelProperty(value = "${ncmmary.doc.getSysLogs.response.sysLogs.desc}")
        private List<SysLog> sysLogs;
        @ApiModelProperty(value = "${ncmmary.doc.response.isLoading.desc}")
        private Boolean isLoading;
        @ApiModelProperty(value = "${ncmmary.doc.response.prev.desc}")
        private String prev;
        @ApiModelProperty(value = "${ncmmary.doc.response.next.desc}")
        private String next;
        @ApiModelProperty(value = "${ncmmary.doc.response.params.desc}")
        private GetSysLogsParameters params;

        public GetSysLogsRestApiResponseBody() {
            super();
        }

        public GetSysLogsRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }
}


