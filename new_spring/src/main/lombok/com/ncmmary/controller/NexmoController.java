package com.ncmmary.controller;

import com.ncmmary.parameters.NexmoSMSCallbackForm;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.http.MediaType;

import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TimeZone;
import java.util.Locale;

import springfox.documentation.annotations.ApiIgnore;

import lombok.Data;


@Controller
public class NexmoController{

    private static final Logger log
        = LoggerFactory.getLogger(NexmoController.class);

    @RequestMapping(path={"/nexmo-sms-callback"}, method={RequestMethod.POST})
    @ResponseBody
    public NexmoSMSCallbackResponseBody nexmoSMSCallback(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute NexmoSMSCallbackForm nexmoSMSCallbackForm, BindingResult bindingResult, @ApiIgnore Locale locale){


        NexmoSMSCallbackResponseBody responseBody
            = new NexmoSMSCallbackResponseBody();

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class NexmoSMSCallbackResponseBody 
    {


    }
}
