package com.ncmmary.controller;

import com.ncmmary.entity.User;
import com.ncmmary.entity.AuthToken;
import com.ncmmary.entity.UserAppInstance;
import com.ncmmary.entity.UserAppInstanceAssociation;
import com.ncmmary.entitymanager.UserManager;

import com.ncmmary.parameters.LoginUserForm;
import com.ncmmary.parameters.WithdrawUserForm;
import com.ncmmary.parameters.RegisterUserForm;
import com.ncmmary.parameters.UpdateUserForm;
import com.ncmmary.parameters.DeleteUsersForm;
import com.ncmmary.parameters.GetUsersParameters;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.http.MediaType;

import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TimeZone;
import java.util.Locale;

import springfox.documentation.annotations.ApiIgnore;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;


@Api(tags = {"User"})
@Controller
public class UserController{

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private UserManager userManager;

    @ApiOperation(value = "${ncmmary.doc.me.summary}", notes = "${ncmmary.doc.me.notes}")
    @RequestMapping(path={"/api/me"}, method={RequestMethod.GET}, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public MeRestApiResponseBody me(HttpServletRequest request, HttpServletResponse response, @ApiIgnore Locale locale){

        User user = userManager.getCurrentUser();

        MeRestApiResponseBody responseBody
            = new MeRestApiResponseBody(null, messageSource, locale);

        responseBody.setUser(user);

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class MeRestApiResponseBody extends RestApiResponseBody
    {
        @ApiModelProperty(value = "${ncmmary.doc.me.response.user.desc}")
        private User user;

        public MeRestApiResponseBody() {
            super();
        }

        public MeRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }


    @ApiOperation(value = "${ncmmary.doc.logout.summary}", notes = "${ncmmary.doc.logout.notes}")
    @RequestMapping(path={"/api/logout"}, method={RequestMethod.POST}, /*consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE,*/ produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public LogoutUserRestApiResponseBody logoutUser(HttpServletRequest request, HttpServletResponse response, @ApiIgnore Locale locale) throws Exception{

        request.logout();

        LogoutUserRestApiResponseBody responseBody = new LogoutUserRestApiResponseBody();

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class LogoutUserRestApiResponseBody extends RestApiResponseBody
    {

        public LogoutUserRestApiResponseBody() {
            super();
        }

        public LogoutUserRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }



    @ApiOperation(value = "${ncmmary.doc.login.summary}", notes = "${ncmmary.doc.login.notes}")
    @RequestMapping(path={"/api/login"}, method={RequestMethod.POST}, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public LoginUserRestApiResponseBody loginUser(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute LoginUserForm loginUserForm, BindingResult bindingResult, @ApiIgnore Locale locale){

        UserManager.LoginReturn loginReturn = null;

        if(!bindingResult.hasErrors()){
            loginReturn = userManager.login(loginUserForm, bindingResult);
        }

        LoginUserRestApiResponseBody responseBody
            = new LoginUserRestApiResponseBody(bindingResult, messageSource, locale);

        responseBody.setUser(loginReturn.getUser());
        responseBody.setAuthToken(loginReturn.getAuthToken());

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class LoginUserRestApiResponseBody extends RestApiResponseBody
    {
        @ApiModelProperty(value = "${ncmmary.doc.loginUser.response.user.desc}")
        private User user;
        @ApiModelProperty(value = "${ncmmary.doc.loginUser.response.authToken.desc}")
        private AuthToken authToken;

        public LoginUserRestApiResponseBody() {
            super();
        }

        public LoginUserRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }


    @ApiOperation(value = "${ncmmary.doc.withdrawUser.summary}", notes = "${ncmmary.doc.withdrawUser.notes}")
    @RequestMapping(path={"/api/user/withdraw"}, method={RequestMethod.POST}, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public WithdrawUserRestApiResponseBody withdrawUser(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute WithdrawUserForm withdrawUserForm, BindingResult bindingResult, @ApiIgnore Locale locale){

        User user = null;

        if(!bindingResult.hasErrors()){
            user = userManager.withdraw(withdrawUserForm, bindingResult);
        }

        WithdrawUserRestApiResponseBody responseBody
            = new WithdrawUserRestApiResponseBody(bindingResult, messageSource, locale);

        responseBody.setUser(user);

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class WithdrawUserRestApiResponseBody extends RestApiResponseBody
    {
        @ApiModelProperty(value = "${ncmmary.doc.withdrawUser.response.user.desc}")
        private User user;

        public WithdrawUserRestApiResponseBody() {
            super();
        }

        public WithdrawUserRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }

    
    @ApiOperation(value = "${ncmmary.doc.deleteUsers.summary}", notes = "${ncmmary.doc.deleteUsers.notes}")
    @RequestMapping(path={"/api/user/delete"}, method={RequestMethod.POST}, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public DeleteUsersRestApiResponseBody deleteUsers(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute DeleteUsersForm deleteUsersForm, BindingResult bindingResult, @ApiIgnore Locale locale){

        if(!bindingResult.hasErrors()){
            userManager.delete(deleteUsersForm, bindingResult);
        }

        DeleteUsersRestApiResponseBody responseBody
            = new DeleteUsersRestApiResponseBody(
                bindingResult, messageSource, locale);

        return responseBody;

    }


    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class DeleteUsersRestApiResponseBody extends RestApiResponseBody
    {
        public DeleteUsersRestApiResponseBody() {
            super();
        }

        public DeleteUsersRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }


    @ApiOperation(value = "${ncmmary.doc.registerUser.summary}", notes = "${ncmmary.doc.registerUser.notes}")
    @RequestMapping(path={"/api/user/register"}, method={RequestMethod.POST}, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public RegisterUserRestApiResponseBody registerUser(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute RegisterUserForm registerUserForm, BindingResult bindingResult, @ApiIgnore Locale locale){
        UserManager.RegisterReturn registerReturn = null;

        if(!bindingResult.hasErrors()){
            registerReturn 
                = userManager.register(registerUserForm, bindingResult);
        }

        RegisterUserRestApiResponseBody responseBody 
            = new RegisterUserRestApiResponseBody(
                bindingResult, messageSource, locale);

        responseBody.setUser(registerReturn.getUser());
        responseBody.setAuthToken(registerReturn.getAuthToken());
        responseBody.setUserAppInstance(registerReturn.getUserAppInstance());
        responseBody.setUserAppInstanceAssociation(
            registerReturn.getUserAppInstanceAssociation());

        return responseBody;
    }


    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class RegisterUserRestApiResponseBody extends RestApiResponseBody
    {
        @ApiModelProperty(value = "${ncmmary.doc.registerUser.response.user.desc}")
        private User user;
        @ApiModelProperty(value = "${ncmmary.doc.registerUser.response.authToken.desc}")
        private AuthToken authToken;
        @ApiModelProperty(value = "${ncmmary.doc.registerUser.response.userAppInstance.desc}")
        private UserAppInstance userAppInstance;
        @ApiModelProperty(value = "${ncmmary.doc.registerUser.response.userAppInstanceAssociation.desc}")
        private UserAppInstanceAssociation userAppInstanceAssociation;

        public RegisterUserRestApiResponseBody() {
            super();
        }

        public RegisterUserRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }


    @ApiOperation(value = "${ncmmary.doc.updateUser.summary}", notes = "${ncmmary.doc.updateUser.notes}")
    @RequestMapping(path={"/api/user/update"}, method={RequestMethod.POST}, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public UpdateUserRestApiResponseBody updateUser(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute UpdateUserForm updateUserForm, BindingResult bindingResult, @ApiIgnore Locale locale){

        User user = null;

        if(!bindingResult.hasErrors()){
            user = userManager.update(updateUserForm, bindingResult);
        }

        UpdateUserRestApiResponseBody responseBody
            = new UpdateUserRestApiResponseBody(bindingResult, messageSource, locale);

        responseBody.setUser(user);

        return responseBody;
    }


    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class UpdateUserRestApiResponseBody extends RestApiResponseBody
    {
        @ApiModelProperty(value = "${ncmmary.doc.updateUser.response.user.desc}")
        private User user;

        public UpdateUserRestApiResponseBody() {
            super();
        }

        public UpdateUserRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }

    
    @ApiOperation(value = "${ncmmary.doc.getUsers.summary}", notes = "${ncmmary.doc.getUsers.notes}")
    @RequestMapping(path={"/api/users"}, method={RequestMethod.GET}, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public GetUsersRestApiResponseBody getUsers(@Valid @ModelAttribute GetUsersParameters params, BindingResult bindingResult, @ApiIgnore Locale locale){

        UserManager.UserList userList = null;

        if(!bindingResult.hasErrors()){
            userList = userManager.getUsers(params);
        }

        GetUsersRestApiResponseBody responseBody
            = new GetUsersRestApiResponseBody(bindingResult, messageSource, locale);
        responseBody.setUsers(userList.getUsers());
        responseBody.setParams(params);
        responseBody.setIsLoading(userList.getIsLoading());
        responseBody.setPrev(userList.getPrev());
        responseBody.setNext(userList.getNext());

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class GetUsersRestApiResponseBody extends RestApiResponseBody
    {
        @ApiModelProperty(value = "${ncmmary.doc.getUsers.response.users}")
        private List<User> users;
        @ApiModelProperty(value = "${ncmmary.doc.response.isLoading.desc}")
        private Boolean isLoading;
        @ApiModelProperty(value = "${ncmmary.doc.response.prev.desc}")
        private String prev;
        @ApiModelProperty(value = "${ncmmary.doc.response.next.desc}")
        private String next;
        @ApiModelProperty(value = "${ncmmary.doc.response.params.desc}")
        private GetUsersParameters params;

        public GetUsersRestApiResponseBody() {
            super();
        }

        public GetUsersRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }
}

