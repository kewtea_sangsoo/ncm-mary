package com.ncmmary.controller;

import com.ncmmary.entity.Product;
import com.ncmmary.entitymanager.ProductManager;
import com.ncmmary.parameters.RegisterProductForm;
import com.ncmmary.parameters.UpdateProductForm;
import com.ncmmary.parameters.DeleteProductsForm;
import com.ncmmary.parameters.GetProductsParameters;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.http.MediaType;

import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TimeZone;
import java.util.Locale;

import springfox.documentation.annotations.ApiIgnore;

import lombok.Data;


@Controller
public class ProductController{

    private static final Logger log = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ProductManager productManager;

    @RequestMapping(path={"/api/product/register"}, method={RequestMethod.POST}, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public RegisterProductRestApiResponseBody registerProduct(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute RegisterProductForm registerProductForm, BindingResult bindingResult, @ApiIgnore Locale locale){

        Product product = null;

        if(!bindingResult.hasErrors()){
            product = productManager.register(registerProductForm, bindingResult);
        }

        RegisterProductRestApiResponseBody responseBody 
            = new RegisterProductRestApiResponseBody(bindingResult, messageSource, locale);

        responseBody.setProduct(product);

        return responseBody;


    }


    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class RegisterProductRestApiResponseBody extends RestApiResponseBody
    {
        private Product product;

        public RegisterProductRestApiResponseBody(){
            super();
        }

        public RegisterProductRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }


    @RequestMapping(path={"/api/product/update"}, method={RequestMethod.POST}, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public UpdateProductRestApiResponseBody updateProduct(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute UpdateProductForm updateProductForm, BindingResult bindingResult, @ApiIgnore Locale locale){

        Product product = null;

        if(!bindingResult.hasErrors()){
            product = productManager.update(updateProductForm, bindingResult);
        }

        UpdateProductRestApiResponseBody responseBody
            = new UpdateProductRestApiResponseBody(bindingResult, messageSource, locale);

        responseBody.setProduct(product);

        return responseBody;

    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class UpdateProductRestApiResponseBody extends RestApiResponseBody
    {
        private Product product;

        public UpdateProductRestApiResponseBody(){
            super();
        }

        public UpdateProductRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }

    }

    @RequestMapping(path={"/api/product/delete"}, method={RequestMethod.POST}, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public DeleteProductsRestApiResponseBody deleteProducts(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute DeleteProductsForm deleteProductsForm, BindingResult bindingResult, @ApiIgnore Locale locale){

        if(!bindingResult.hasErrors()){
            productManager.delete(deleteProductsForm, bindingResult);
        }

        DeleteProductsRestApiResponseBody responseBody
            = new DeleteProductsRestApiResponseBody(bindingResult, messageSource, locale);

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class DeleteProductsRestApiResponseBody extends RestApiResponseBody
    {
        public DeleteProductsRestApiResponseBody(){
            super();
        }

        public DeleteProductsRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }

    }

    @RequestMapping(path={"/api/products"}, method={RequestMethod.GET}, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public GetProductsRestApiResponseBody getProducts(@Valid @ModelAttribute GetProductsParameters params, BindingResult bindingResult, @ApiIgnore Locale locale){

        List<Product> products = null;

        if(!bindingResult.hasErrors()){
            ProductManager.ProductList productList = productManager.getProducts(params);
        }

        GetProductsRestApiResponseBody responseBody
            = new GetProductsRestApiResponseBody(bindingResult, messageSource, locale);
        responseBody.setProducts(products);

        return responseBody;

    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class GetProductsRestApiResponseBody extends RestApiResponseBody
    {
        private List<Product> products;

        public GetProductsRestApiResponseBody(){
            super();
        }

        public GetProductsRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }


}


