package com.ncmmary.controller;

import com.ncmmary.entity.User;
import com.ncmmary.entity.AuthToken;
import com.ncmmary.entity.UserValidationKey;
import com.ncmmary.entitymanager.UserValidationKeyManager;

import com.ncmmary.parameters.ValidateUserForm;
import com.ncmmary.parameters.IssueNewUserValidationKeyForm;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.http.MediaType;

import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TimeZone;
import java.util.Locale;

import springfox.documentation.annotations.ApiIgnore;

import lombok.Data;


@Controller
public class UserValidationKeyController{

    private static final Logger log 
        = LoggerFactory.getLogger(UserValidationKeyController.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private UserValidationKeyManager userValidationKeyManager;

    @RequestMapping(path={"/api/validate-user"}, method={RequestMethod.POST}, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public ValidateUserRestApiResponseBody validateUser(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute ValidateUserForm validateUserForm, BindingResult bindingResult, @ApiIgnore Locale locale){

        UserValidationKeyManager.ValidateReturn validateReturn = null;

        if(!bindingResult.hasErrors()){
            validateReturn
                = userValidationKeyManager.validate(validateUserForm, bindingResult);
        }

        ValidateUserRestApiResponseBody responseBody
            = new ValidateUserRestApiResponseBody(bindingResult, messageSource, locale);

        responseBody.setUser(validateReturn.getUser());
        responseBody.setAuthToken(validateReturn.getAuthToken());
        responseBody.setUserValidationKey(validateReturn.getUserValidationKey());

        return responseBody;

    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class ValidateUserRestApiResponseBody extends RestApiResponseBody
    {
        private User user;
        private AuthToken authToken;
        private UserValidationKey userValidationKey;

        public ValidateUserRestApiResponseBody() {
            super();
        }

        public ValidateUserRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }

    @RequestMapping(path={"/api/issue-new-user-validation-key"}, method={RequestMethod.POST}, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public IssueNewUserValidationKeyRestApiResponseBody issueNewUserValidationKey(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute IssueNewUserValidationKeyForm issueNewUserValidationKeyForm, BindingResult bindingResult, @ApiIgnore Locale locale){

        log.info("!@#$");

        if(!bindingResult.hasErrors()){
            userValidationKeyManager.issueNew(issueNewUserValidationKeyForm, bindingResult);
        }

        IssueNewUserValidationKeyRestApiResponseBody responseBody
            = new IssueNewUserValidationKeyRestApiResponseBody(bindingResult, messageSource, locale);

        return responseBody;
    }


    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class IssueNewUserValidationKeyRestApiResponseBody extends RestApiResponseBody
    {
        public IssueNewUserValidationKeyRestApiResponseBody() {
            super();
        }

        public IssueNewUserValidationKeyRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }


}
