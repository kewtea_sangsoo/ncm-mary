package com.ncmmary.controller;

import com.ncmmary.entity.AssetRental;
import com.ncmmary.entitymanager.AssetRentalManager;
import com.ncmmary.parameters.GetAssetRentalsParameters;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.http.MediaType;

import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.Base64;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TimeZone;
import java.util.Locale;

import springfox.documentation.annotations.ApiIgnore;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiModelProperty;


import lombok.Data;

@Api(tags = {"AssetRental"})
@Controller
public class AssetRentalController {

    private static final Logger log = LoggerFactory.getLogger(AssetRentalController.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private AssetRentalManager assetRentalManager;

    @ApiOperation(value = "${ncmmary.doc.getAssetRentals.summary}", notes = "${ncmmary.doc.getAssetRentals.notes}")
    @RequestMapping(path = {"/api/asset-rentals"}, method = {RequestMethod.GET}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public GetAssetRentalsRestApiResponseBody getAssetRentals(@Valid @ModelAttribute GetAssetRentalsParameters params, BindingResult bindingResult, @ApiIgnore Locale locale){

        AssetRentalManager.AssetRentalList assetRentalList = null;

        if (!bindingResult.hasErrors()) {
            assetRentalList = assetRentalManager.getAssetRentals(params);
        }

        GetAssetRentalsRestApiResponseBody responseBody
            = new GetAssetRentalsRestApiResponseBody(bindingResult, messageSource, locale);
        responseBody.setAssetRentals(assetRentalList.getAssetRentals());
        responseBody.setParams(params);
        responseBody.setIsLoading(assetRentalList.getIsLoading());
        responseBody.setPrev(assetRentalList.getPrev());
        responseBody.setNext(assetRentalList.getNext());

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class GetAssetRentalsRestApiResponseBody extends RestApiResponseBody
    {
        @ApiModelProperty(value = "${ncmmary.doc.getAssetRentals.response.assetRentals.desc}")
        private List<AssetRental> assetRentals;
        @ApiModelProperty(value = "${ncmmary.doc.response.isLoading.desc}")
        private Boolean isLoading;
        @ApiModelProperty(value = "${ncmmary.doc.response.prev.desc}")
        private String prev;
        @ApiModelProperty(value = "${ncmmary.doc.response.next.desc}")
        private String next;
        @ApiModelProperty(value = "${ncmmary.doc.response.params.desc}")
        private GetAssetRentalsParameters params;

        public GetAssetRentalsRestApiResponseBody() {
            super();
        }

        public GetAssetRentalsRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }

}


