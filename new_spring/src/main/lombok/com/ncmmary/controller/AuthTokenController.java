package com.ncmmary.controller;

import com.ncmmary.entity.AuthToken;
import com.ncmmary.entitymanager.AuthTokenManager;
import com.ncmmary.parameters.RevokeAuthTokensForm;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.http.MediaType;

import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TimeZone;
import java.util.Locale;
import java.util.Collection;

import springfox.documentation.annotations.ApiIgnore;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiModelProperty;


import lombok.Data;

@Api(tags = {"AuthToken"})
@Controller
public class AuthTokenController{

    private static final Logger log = LoggerFactory.getLogger(AuthTokenController.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private AuthTokenManager authTokenManager;

    @ApiOperation(value = "${ncmmary.doc.revokeAuthTokens.summary}", notes = "${ncmmary.doc.revokeAuthTokens.notes}", tags = {"AuthToken"})
    @RequestMapping(path={"/api/revoke-auth-tokens"}, method={RequestMethod.POST}, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public RevokeAuthTokensRestApiResponseBody revokeAuthTokens(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute RevokeAuthTokensForm revokeAuthTokensForm, BindingResult bindingResult, @ApiIgnore Locale locale){

        Collection<AuthToken> revokedAuthTokens = null;

        if(!bindingResult.hasErrors()){
            revokedAuthTokens 
                = authTokenManager.revoke(revokeAuthTokensForm, bindingResult);

        }

        RevokeAuthTokensRestApiResponseBody responseBody
            = new RevokeAuthTokensRestApiResponseBody(
                bindingResult, messageSource, locale);

        responseBody.setAuthTokens(revokedAuthTokens);

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class RevokeAuthTokensRestApiResponseBody extends RestApiResponseBody
    {
        @ApiModelProperty(value = "${ncmmary.doc.revokeAuthTokens.response.authTokens.desc}")
        private Collection<AuthToken> authTokens;

        public RevokeAuthTokensRestApiResponseBody() {
            super();
        }

        public RevokeAuthTokensRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale) {
            super(errors, messageSource, locale);
        }
    }


}

