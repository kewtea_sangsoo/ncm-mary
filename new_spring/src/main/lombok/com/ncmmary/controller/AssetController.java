package com.ncmmary.controller;

import com.ncmmary.entity.Asset;
import com.ncmmary.entitymanager.AssetManager;
import com.ncmmary.parameters.RegisterAssetForm;
import com.ncmmary.parameters.UpdateAssetForm;
import com.ncmmary.parameters.DeleteAssetsForm;
import com.ncmmary.parameters.GetAssetsParameters;
import com.ncmmary.parameters.RentAssetForm;
import com.ncmmary.parameters.SuspendAssetForm;
import com.ncmmary.parameters.ReturnAssetForm;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.http.MediaType;

import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.Base64;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TimeZone;
import java.util.Locale;

import springfox.documentation.annotations.ApiIgnore;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiModelProperty;


import lombok.Data;

@Api(tags = {"Asset"})
@Controller
public class AssetController{

    private static final Logger log = LoggerFactory.getLogger(AssetController.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private AssetManager assetManager;

    @ApiOperation(value = "${ncmmary.doc.registerAsset.summary}", notes = "${ncmmary.doc.registerAsset.notes}", tags = {"Asset"})
    @RequestMapping(path={"/api/asset/register"}, method={RequestMethod.POST}, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public RegisterAssetRestApiResponseBody registerAsset(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute RegisterAssetForm registerAssetForm, BindingResult bindingResult, @ApiIgnore Locale locale){
        Asset asset = null;

        if(!bindingResult.hasErrors()){
            asset = assetManager.register(registerAssetForm, bindingResult);
        }

        RegisterAssetRestApiResponseBody responseBody 
            = new RegisterAssetRestApiResponseBody(bindingResult, messageSource, locale);

        responseBody.setAsset(asset);

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class RegisterAssetRestApiResponseBody extends RestApiResponseBody
    {
        @ApiModelProperty(value = "${ncmmary.doc.registerAsset.response.asset}")
        private Asset asset;

        public RegisterAssetRestApiResponseBody() {
            super();
        }

        public RegisterAssetRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }

    @ApiOperation(value = "${ncmmary.doc.updateAsset.summary}", notes = "${ncmmary.doc.updateAsset.notes}")
    @RequestMapping(path={"/api/asset/update"}, method={RequestMethod.POST}, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public UpdateAssetRestApiResponseBody updateAsset(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute UpdateAssetForm updateAssetForm, BindingResult bindingResult, @ApiIgnore Locale locale){

        Asset asset = null;

        if(!bindingResult.hasErrors()){
            asset = assetManager.update(updateAssetForm, bindingResult);
        }

        UpdateAssetRestApiResponseBody responseBody
            = new UpdateAssetRestApiResponseBody(bindingResult, messageSource, locale);

        responseBody.setAsset(asset);

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class UpdateAssetRestApiResponseBody extends RestApiResponseBody
    {
        @ApiModelProperty(value = "${ncmmary.doc.updateAsset.response.asset}")
        private Asset asset;

        public UpdateAssetRestApiResponseBody(){
            super();
        }

        public UpdateAssetRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }

    @ApiOperation(value = "${ncmmary.doc.deleteAssets.summary}", notes = "${ncmmary.doc.deleteAssets.notes}")
    @RequestMapping(path={"/api/asset/delete"}, method={RequestMethod.POST}, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public DeleteAssetsRestApiResponseBody deleteAssets(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute DeleteAssetsForm deleteAssetsForm, BindingResult bindingResult, @ApiIgnore Locale locale){

        if(!bindingResult.hasErrors()){
            assetManager.delete(deleteAssetsForm, bindingResult);
        }

        DeleteAssetsRestApiResponseBody responseBody
            = new DeleteAssetsRestApiResponseBody(bindingResult, messageSource, locale);

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class DeleteAssetsRestApiResponseBody extends RestApiResponseBody
    {
        public DeleteAssetsRestApiResponseBody(){
            super();
        }

        public DeleteAssetsRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }

    @ApiOperation(value = "${ncmmary.doc.getAssets.summary}", notes = "${ncmmary.doc.getAssets.notes}")
    @RequestMapping(path={"/api/assets"}, method={RequestMethod.GET}, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public GetAssetsRestApiResponseBody getAssets(@Valid @ModelAttribute GetAssetsParameters params, BindingResult bindingResult, @ApiIgnore Locale locale){

        AssetManager.AssetList assetList = null;

        if(!bindingResult.hasErrors()){
             assetList = assetManager.getAssets(params);
        }

        GetAssetsRestApiResponseBody responseBody
            = new GetAssetsRestApiResponseBody(bindingResult, messageSource, locale);
        responseBody.setAssets(assetList.getAssets());
        responseBody.setParams(params);
        responseBody.setIsLoading(assetList.getIsLoading());
        responseBody.setPrev(assetList.getPrev());
        responseBody.setNext(assetList.getNext());

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class GetAssetsRestApiResponseBody extends RestApiResponseBody
    {
        @ApiModelProperty(value = "${ncmmary.doc.getAssets.response.assets}")
        private List<Asset> assets;
        @ApiModelProperty(value = "${ncmmary.doc.getAssets.response.isLoading}")
        private Boolean isLoading;
        @ApiModelProperty(value = "${ncmmary.doc.getAssets.response.prev}")
        private String prev;
        @ApiModelProperty(value = "${ncmmary.doc.getAssets.response.next}")
        private String next;
        @ApiModelProperty(value = "${ncmmary.doc.getAssets.response.params}")
        private GetAssetsParameters params;

        public GetAssetsRestApiResponseBody(){
            super();
        }

        public GetAssetsRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }

    @ApiOperation(value = "${ncmmary.doc.rentAsset.summary}", notes = "${ncmmary.doc.rentAsset.notes}")
    @RequestMapping(path = {"/api/asset/rent"}, method={RequestMethod.POST}, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public RentAssetRestApiResponseBody rentAsset(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute RentAssetForm rentAssetForm, BindingResult bindingResult, @ApiIgnore Locale locale) {

        if(!bindingResult.hasErrors()) {
            assetManager.rent(rentAssetForm, bindingResult);

        }

        RentAssetRestApiResponseBody responseBody
            = new RentAssetRestApiResponseBody(bindingResult, messageSource, locale);

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class RentAssetRestApiResponseBody extends RestApiResponseBody {

        public RentAssetRestApiResponseBody() {
            super();
        }

        public RentAssetRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }

    @RequestMapping(path = {"/returnAsset"}, method={RequestMethod.POST, RequestMethod.GET}/*, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE*/)
    public String returnAsset_(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute ReturnAssetForm returnAssetForm, BindingResult bindingResult, Model model, TimeZone timeZone, @ApiIgnore Locale locale) throws Exception {

        
        String apiKey = "cf4dae5bb7b7606aa35e05aaba23b6a6";

        String orderNo = "2017060140292100";
        String orderDt = "20170601";
        String orderTm = "000000";
        String buyReqamt = "7000";

        String message = orderNo + orderDt + orderTm + buyReqamt;

        javax.crypto.Mac sha256HMAC = javax.crypto.Mac.getInstance("HmacSHA256");
        javax.crypto.spec.SecretKeySpec secretKey = new javax.crypto.spec.SecretKeySpec(apiKey.getBytes("UTF-8"), "HmacSHA256");

        sha256HMAC.init(secretKey);
        String hashed = org.apache.commons.codec.binary.Base64.encodeBase64String(sha256HMAC.doFinal(message.getBytes("UTF-8")));

        model.addAttribute("apiKey", apiKey);
        model.addAttribute("hashed", hashed);
        model.addAttribute("orderNo", orderNo);
        model.addAttribute("orderDt", orderDt);
        model.addAttribute("orderTm", orderTm);
        model.addAttribute("buyReqamt", buyReqamt);

        return "order";
    }

    @RequestMapping(path = {"/directpay/card/direct-cc-return"}, method={RequestMethod.POST, RequestMethod.GET}/*, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE*/)
    public String directCCReturn(HttpServletRequest request, HttpServletResponse response, /*@Valid @ModelAttribute ReturnAssetForm returnAssetForm, BindingResult bindingResult,*/ Model model, TimeZone timeZone, @ApiIgnore Locale locale) throws Exception {

        
        log.info("request.getParameterNames(): " + request.getParameterNames());
        log.info("request.getParameter(\"RESULT_CODE\"): " + request.getParameter("RESULT_CODE"));


        return "direct_cc_return";
    }

    @ApiOperation(value = "${ncmmary.doc.returnAsset.summary}", notes = "${ncmmary.doc.returnAsset.notes}")
    @RequestMapping(path = {"/api/asset/return"}, method={RequestMethod.POST}, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public ReturnAssetRestApiResponseBody returnAsset(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute ReturnAssetForm returnAssetForm, BindingResult bindingResult, @ApiIgnore Locale locale) {

        if(!bindingResult.hasErrors()) {
            assetManager.suspend(returnAssetForm, bindingResult);
        }

        ReturnAssetRestApiResponseBody responseBody
            = new ReturnAssetRestApiResponseBody(bindingResult, messageSource, locale);

        return responseBody;

    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class ReturnAssetRestApiResponseBody extends RestApiResponseBody
    {
        public ReturnAssetRestApiResponseBody() {
            super();
        }

        public ReturnAssetRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }
    

    @ApiOperation(value = "${ncmmary.doc.suspendAsset.summary}", notes = "${ncmmary.doc.suspendAsset.notes}")
    @RequestMapping(path = {"/api/asset/suspend"}, method={RequestMethod.POST}, consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public SuspendAssetRestApiResponseBody suspendAsset(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute SuspendAssetForm suspendAssetForm, BindingResult bindingResult, @ApiIgnore Locale locale) {

        if(!bindingResult.hasErrors()){
            assetManager.suspend(suspendAssetForm, bindingResult);
        }

        SuspendAssetRestApiResponseBody responseBody
            = new SuspendAssetRestApiResponseBody(bindingResult, messageSource, locale);

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class SuspendAssetRestApiResponseBody extends RestApiResponseBody
    {
        public SuspendAssetRestApiResponseBody(){
            super();
        }

        public SuspendAssetRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }
}

