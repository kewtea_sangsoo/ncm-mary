package com.ncmmary.controller;

import com.ncmmary.entity.Order;
import com.ncmmary.entitymanager.OrderManager;
import com.ncmmary.parameters.RegisterOrderForm;
import com.ncmmary.parameters.UpdateOrderForm;
import com.ncmmary.parameters.DeleteOrdersForm;
import com.ncmmary.parameters.GetOrdersParameters;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.http.MediaType;

import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TimeZone;
import java.util.Locale;

import springfox.documentation.annotations.ApiIgnore;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;

@Api(tags = {"Order"})
@Controller
public class OrderController{

    private static final Logger log = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private OrderManager orderManager;

    @RequestMapping("/order")
    public String order(HttpServletRequest request, HttpServletResponse response, BindingResult bindingResult, Model model, TimeZone timeZone, Locale locale) {

        return "order";
    }

    @ApiOperation(value = "${ncmmary.doc.registerOrder.summary}", notes = "${ncmmary.doc.registerOrder.notes}", tags = {"Order"})
    @RequestMapping(path={"/api/order/register"}, method={RequestMethod.POST}, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public RegisterOrderRestApiResponseBody registerOrder(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute RegisterOrderForm registerOrderForm, BindingResult bindingResult, @ApiIgnore Locale locale){

        Order order = null;

        if(!bindingResult.hasErrors()){
            order = orderManager.register(registerOrderForm, bindingResult);
        }

        RegisterOrderRestApiResponseBody responseBody 
            = new RegisterOrderRestApiResponseBody(bindingResult, messageSource, locale);

        responseBody.setOrder(order);

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class RegisterOrderRestApiResponseBody extends RestApiResponseBody
    {
        @ApiModelProperty(value = "${ncmmary.doc.registerOrder.response.order.desc}")
        private Order order;

        public RegisterOrderRestApiResponseBody(){
            super();
        }

        public RegisterOrderRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }

    @ApiOperation(value = "${ncmmary.doc.updateOrder.summary}", notes = "${ncmmary.doc.updateOrder.notes}")
    @RequestMapping(path={"/api/order/update"}, method={RequestMethod.POST}, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public UpdateOrderRestApiResponseBody updateOrder(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute UpdateOrderForm updateOrderForm, BindingResult bindingResult, @ApiIgnore Locale locale){

        Order order = null;

        if(!bindingResult.hasErrors()){
            order = orderManager.update(updateOrderForm, bindingResult);
        }

        UpdateOrderRestApiResponseBody responseBody
            = new UpdateOrderRestApiResponseBody(bindingResult, messageSource, locale);

        responseBody.setOrder(order);

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class UpdateOrderRestApiResponseBody extends RestApiResponseBody
    {
        @ApiModelProperty(value = "${ncmmary.doc.updateOrder.response.order.desc}")
        private Order order;

        public UpdateOrderRestApiResponseBody(){
            super();
        }

        public UpdateOrderRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }

    @ApiOperation(value = "${ncmmary.doc.deleteOrders.summary}", notes = "${ncmmary.doc.deleteOrders.notes}")
    @RequestMapping(path={"/api/order/delete"}, method={RequestMethod.POST}, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public DeleteOrdersRestApiResponseBody deleteOrders(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute DeleteOrdersForm deleteOrdersForm, BindingResult bindingResult, @ApiIgnore Locale locale){

        if(!bindingResult.hasErrors()){
            orderManager.delete(deleteOrdersForm, bindingResult);
        }

        DeleteOrdersRestApiResponseBody responseBody
            = new DeleteOrdersRestApiResponseBody(bindingResult, messageSource, locale);

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class DeleteOrdersRestApiResponseBody extends RestApiResponseBody
    {
        public DeleteOrdersRestApiResponseBody(){
            super();
        }

        public DeleteOrdersRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }

    }

    @ApiOperation(value = "${ncmmary.doc.getOrders.summary}", notes = "${ncmmary.doc.getOrders.notes}")
    @RequestMapping(path={"/api/orders"}, method={RequestMethod.GET}, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public GetOrdersRestApiResponseBody getOrders(@Valid @ModelAttribute GetOrdersParameters params, BindingResult bindingResult, @ApiIgnore Locale locale){

        List<Order> orders = null;

        if(!bindingResult.hasErrors()){
            OrderManager.OrderList orderList = orderManager.getOrders(params);
        }

        GetOrdersRestApiResponseBody responseBody
            = new GetOrdersRestApiResponseBody(bindingResult, messageSource, locale);
        responseBody.setOrders(orders);

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class GetOrdersRestApiResponseBody extends RestApiResponseBody
    {
        @ApiModelProperty(value = "${ncmmary.doc.getOrders.response.orders.desc}")
        private List<Order> orders;


        public GetOrdersRestApiResponseBody(){
            super();
        }

        public GetOrdersRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }

}


