package com.ncmmary.controller;

import com.ncmmary.entity.Transaction;
import com.ncmmary.entitymanager.TransactionManager;
import com.ncmmary.parameters.RegisterTransactionForm;
import com.ncmmary.parameters.UpdateTransactionForm;
import com.ncmmary.parameters.DeleteTransactionsForm;
import com.ncmmary.parameters.GetTransactionsParameters;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.http.MediaType;

import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TimeZone;
import java.util.Locale;

import springfox.documentation.annotations.ApiIgnore;

import lombok.Data;


@Controller
public class TransactionController{

    private static final Logger log = LoggerFactory.getLogger(TransactionController.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private TransactionManager transactionManager;

    @RequestMapping(path={"/api/transaction/register"}, method={RequestMethod.POST}, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public RegisterTransactionRestApiResponseBody registerTransaction(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute RegisterTransactionForm registerTransactionForm, BindingResult bindingResult, @ApiIgnore Locale locale){
        Transaction transaction = null;

        if(!bindingResult.hasErrors()){
            transaction = transactionManager.register(registerTransactionForm, bindingResult);
        }

        RegisterTransactionRestApiResponseBody responseBody 
            = new RegisterTransactionRestApiResponseBody(bindingResult, messageSource, locale);

        responseBody.setTransaction(transaction);

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class RegisterTransactionRestApiResponseBody extends RestApiResponseBody
    {
        private Transaction transaction;

        public RegisterTransactionRestApiResponseBody(){
            super();
        }

        public RegisterTransactionRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }



    @RequestMapping(path={"/api/transaction/update"}, method={RequestMethod.POST}, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public UpdateTransactionRestApiResponseBody updateTransaction(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute UpdateTransactionForm updateTransactionForm, BindingResult bindingResult, @ApiIgnore Locale locale){

        Transaction transaction = null;

        if(!bindingResult.hasErrors()){
            transaction = transactionManager.update(updateTransactionForm, bindingResult);
        }

        UpdateTransactionRestApiResponseBody responseBody
            = new UpdateTransactionRestApiResponseBody(bindingResult, messageSource, locale);

        responseBody.setTransaction(transaction);

        return responseBody;
    }


    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class UpdateTransactionRestApiResponseBody extends RestApiResponseBody
    {
        private Transaction transaction;

        public UpdateTransactionRestApiResponseBody() {
            super();
        }

        public UpdateTransactionRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale) {
            super(errors, messageSource, locale);
        }
    }

    @RequestMapping(path={"/api/transaction/delete"}, method={RequestMethod.POST}, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public DeleteTransactionsRestApiResponseBody deleteTransactions(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute DeleteTransactionsForm deleteTransactionsForm, BindingResult bindingResult, @ApiIgnore Locale locale){

        if(!bindingResult.hasErrors()){
            transactionManager.delete(deleteTransactionsForm, bindingResult);
        }

        DeleteTransactionsRestApiResponseBody responseBody
            = new DeleteTransactionsRestApiResponseBody(bindingResult, messageSource, locale);

        return responseBody;
    }


    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class DeleteTransactionsRestApiResponseBody extends RestApiResponseBody
    {

        public DeleteTransactionsRestApiResponseBody() {
            super();
        }

        public DeleteTransactionsRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale) {
            super(errors, messageSource, locale);
        }
    }

    @RequestMapping(path={"/api/transactions"}, method={RequestMethod.GET}, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public GetTransactionsRestApiResponseBody getTransactions(@Valid @ModelAttribute GetTransactionsParameters params, BindingResult bindingResult, @ApiIgnore Locale locale){

        List<Transaction> transactions = null;

        if(!bindingResult.hasErrors()){
            TransactionManager.TransactionList transactionList = transactionManager.getTransactions(params);
        }

        GetTransactionsRestApiResponseBody responseBody
            = new GetTransactionsRestApiResponseBody(bindingResult, messageSource, locale);
        responseBody.setTransactions(transactions);

        return responseBody;
    }


    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class GetTransactionsRestApiResponseBody extends RestApiResponseBody
    {
        private List<Transaction> transactions;

        public GetTransactionsRestApiResponseBody(){
            super();
        }

        public GetTransactionsRestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale){
            super(errors, messageSource, locale);
        }
    }



}


