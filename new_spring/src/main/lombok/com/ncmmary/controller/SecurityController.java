package com.ncmmary.controller;

import com.ncmmary.security.MyUserDetails;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import org.springframework.security.web.csrf.CsrfToken;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;

import springfox.documentation.annotations.ApiIgnore;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TimeZone;
import java.util.Locale;

@Api(tags = {"Security"})
@Controller
public class SecurityController{

    private static final Logger log = LoggerFactory.getLogger(SecurityController.class);

    //HTTPS
    @ApiOperation(value = "${ncmmary.doc.getCsrfToken.summary}", notes = "${ncmmary.doc.getCsrfToken.notes}", tags = {"Security"})
    @RequestMapping(path={"/api/security/get-csrf-token"}, method={RequestMethod.GET}, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public GetCsrfTokenRestApiResponseBody getCsrfToken(HttpServletRequest request){
        GetCsrfTokenRestApiResponseBody responseBody = new GetCsrfTokenRestApiResponseBody();
        
        CsrfToken csrfToken = (CsrfToken)request.getAttribute("_csrf");
        if(null != csrfToken)
        {
            responseBody.setHeaderName(csrfToken.getHeaderName());
            responseBody.setParameterName(csrfToken.getParameterName());
            responseBody.setToken(csrfToken.getToken());
        }

        return responseBody;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class GetCsrfTokenRestApiResponseBody extends RestApiResponseBody
    {
        @ApiModelProperty(value = "${ncmmary.doc.getCsrfToken.response.headerName.desc}")
        private String headerName;
        @ApiModelProperty(value = "${ncmmary.doc.getCsrfToken.response.parameterName.desc}")
        private String parameterName;
        @ApiModelProperty(value = "${ncmmary.doc.getCsrfToken.response.token.desc}")
        private String token;

        public GetCsrfTokenRestApiResponseBody()
        {
            super();
        }
    }

}

