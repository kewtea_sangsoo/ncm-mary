package com.ncmmary.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Transient;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Type;
import org.springframework.context.MessageSource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModelProperty;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class UserValidationKey implements Serializable
{
    private static final Logger log = LoggerFactory.getLogger(UserValidationKey.class);

    @Id
    private Long userId;

    @Column
    private String email;

    @Column
    private String mobilePhone;

    @Column
    private Short type;
    public static class Type
    {
        public static final short CODE = 1;
    }

    @Column
    private String value;

    @Column
    private Short Status;
    public static class Status
    {
        public static final short NOT_VALIDATED = 1;
        public static final short VALIDATED = 2;

        public static String toString(final Short id)
        {
            String s = null;

            if (null != id) {
                if (VALIDATED == id) {
                    s = "validated";
                } else if (NOT_VALIDATED == id) {
                    s = "not-validated";
                }
            }

            return s;
        }
    }

    
    @Column
    private Long statusLastUpdatedWhen;

    @Column
    private Long issuedWhen;

    @Column
    private Long expiredWhen;

    @Column
    private Long createdWhen;

}

