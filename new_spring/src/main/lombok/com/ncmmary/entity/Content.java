package com.ncmmary.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.StdConverter;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;

import io.swagger.annotations.ApiModelProperty;

import lombok.Data;

@MappedSuperclass
@Data
public abstract class Content {

    public static class IdToStringConverter extends StdConverter<Long, String> {
        @Override
        public String convert(Long id) {
            return id.toString();
        }
    }

    public static class StringToIdConverter extends StdConverter<String, Long> {
        @Override
        public Long convert(String id) {
            return Long.valueOf(id);
        }
    }

	// constructor
	public Content() {}
	
	@Id
    @JsonSerialize(converter=IdToStringConverter.class)
    @JsonDeserialize(converter=StringToIdConverter.class)
	//@GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(value = "${ncmmary.doc.entity.id.desc}")
	public Long id;
	
	// where it belongs? (3)
	
	@Column(nullable = true)
    @ApiModelProperty(value = "${ncmmary.doc.entity.modelVer.desc}")
	public String modelVer;
	
	// deprecated ?
	@Column(nullable = true)
    @ApiModelProperty(value = "${ncmmary.doc.entity.parentId.desc}")
	public Long parentId;

	@Column(nullable = true)
    @ApiModelProperty(value = "${ncmmary.doc.entity.projectScope.desc}")
	public String projectScope;
	
	
	// when-how created? (7)
	
	@Column(nullable = true)
    @ApiModelProperty(value = "${ncmmary.doc.entity.createdTzone.desc}")
	public Long createdTzone;
	@Column(nullable = true)
    @ApiModelProperty(value = "${ncmmary.doc.entity.createdWhen.desc}")
	public Long createdWhen;
	@Column(nullable = true)
    @ApiModelProperty(value = "${ncmmary.doc.entity.authorId.desc}")
	public Long authorId;
	
	@Column(nullable = true)
    @ApiModelProperty(value = "${ncmmary.doc.entity.updatedTzone.desc}")
	public Long updatedTzone;
	@Column(nullable = true)
    @ApiModelProperty(value = "${ncmmary.doc.entity.lastUpdatedWhen.desc}")
	public Long lastUpdatedWhen;
	@Column(nullable = true)
    @ApiModelProperty(value = "${ncmmary.doc.entity.updatedBy.desc}")
	public Long updatedBy;
	
	@Column(nullable = true)
    @ApiModelProperty(value = "${ncmmary.doc.entity.locale.desc}")
	public String locale;
	
	
	// content visibility (4)
	
	@Column(columnDefinition="tinyint(1) default 1", nullable = true) // 0 is false
	public Boolean active;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public Boolean draft;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public Boolean snapshot;
	@Column(columnDefinition="tinyint(1) default 0", nullable = true)
	public Boolean flag;
	
	
	// editability (2)
	
	@Column(nullable = true)
    @ApiModelProperty(value = "${ncmmary.doc.entity.readAccess.desc}")
	public String readAccess;
	@Column(nullable = true)
    @ApiModelProperty(value = "${ncmmary.doc.entity.writeAccess.desc}")
	public String writeAccess;
	
	
	// custom field (확장성을 위한 json 덩어리 필드)
	@Lob
	@Column (columnDefinition = "LONGTEXT", nullable=true)
    @ApiModelProperty(value = "${ncmmary.doc.entity.jsonBox.desc}")
	public String jsonBox; // ex) jsonbox = "{\"custom\": true}";
	
}
