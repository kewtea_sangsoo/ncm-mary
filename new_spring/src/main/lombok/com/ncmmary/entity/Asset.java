package com.ncmmary.entity;

import com.ncmmary.i18n.MessageView;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Transient;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Type;
import org.springframework.context.MessageSource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.StdConverter;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import io.swagger.annotations.ApiModelProperty;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class Asset extends Content implements Serializable
{
    private static final Logger log = LoggerFactory.getLogger(Asset.class);

    public static class IdToStringConverter extends StdConverter<Long, String> {
        @Override
        public String convert(Long id) {
            return id.toString();
        }
    }

    public static class StringToIdConverter extends StdConverter<String, Long> {
        @Override
        public Long convert(String id) {
            return Long.valueOf(id);
        }
    }

    @Id
    @JsonSerialize(converter=IdToStringConverter.class)
    @JsonDeserialize(converter=StringToIdConverter.class)
    @ApiModelProperty(value = "${ncmmary.doc.asset.id.desc}")
    private Long id;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.asset.type.desc}")
    private Short type;
    public static class Type
    {
        public static final short TRACKER = 1;
        public static final short BIKE = 2;
        public static final short LOCK = 3;

        public static String toString(final Short id)
        {
            String s = null;

            if (null != id ) {
                if (TRACKER == id) {
                    s = "tracker";
                } else if (BIKE == id) {
                    s = "bike";
                } else if (LOCK == id) {
                    s = "lock";
                }
            }

            return s;
        }
    }

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.asset.status.desc}")
    private Short status;
    public static class Status
    {
        public static final short AVAILABLE = 1;
        public static final short UNAVAILABLE = 2;

        public static String toString(final Short id)
        {
            String s = null;

            if (AVAILABLE == id) {
                s = "available";
            } else if (UNAVAILABLE == id) {
                s = "unavailable";
            }

            return s;
        }
    }

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.statusLastUpdatedWhen.desc}")
    private Long statusLastUpdatedWhen;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.asset.parentId.desc}")
    private Long parentId;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.createdWhen.desc}")
    private Long createdWhen;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.updatedWhen.desc}")
    private Long updatedWhen;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.asset.userId.desc}")
    private Long userId;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.asset.latitude.desc}")
    private Double latitude;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.asset.longitude.desc}")
    private Double longitude;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.asset.hasBattery.desc}")
    private Boolean hasBattery;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.asset.battery.desc}")
    private Double battery;

    @JsonProperty
    @ApiModelProperty(value = "${ncmmary.doc.asset.typeString.desc}")
    public String getTypeString()
    {
        return Type.toString(this.type);
    }

    @JsonProperty
    @ApiModelProperty(value = "${ncmmary.doc.asset.statusString.desc}")
    public String getStatusString()
    {
        return Status.toString(this.status);
    }

}


