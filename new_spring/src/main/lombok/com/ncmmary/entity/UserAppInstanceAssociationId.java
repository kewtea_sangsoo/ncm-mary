package com.ncmmary.entity;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Embeddable;

import java.io.Serializable;

@Embeddable
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class UserAppInstanceAssociationId implements Serializable
{
    @ApiModelProperty(value = "${ncmmary.doc.user.id.desc}")
    private Long userId;
    @ApiModelProperty(value = "${ncmmary.doc.userAppInstance.id.desc}")
    private Long userAppInstanceId;
}

