package com.ncmmary.entity;

import com.ncmmary.i18n.MessageView;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Transient;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Type;
import org.springframework.context.MessageSource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.StdConverter;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;


import io.swagger.annotations.ApiModelProperty;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class User extends Content implements Serializable
{
    private static final Logger log = LoggerFactory.getLogger(User.class);

    public static class IdToStringConverter extends StdConverter<Long, String> {
        @Override
        public String convert(Long id) {
            return id.toString();
        }
    }

    public static class StringToIdConverter extends StdConverter<String, Long> {
        @Override
        public Long convert(String id) {
            return Long.valueOf(id);
        }
    }

    @Id
    @JsonSerialize(converter=IdToStringConverter.class)
    @JsonDeserialize(converter=StringToIdConverter.class)
    @JsonView(value={MessageView.class})
    @ApiModelProperty(value = "${ncmmary.doc.user.id.desc}")
    private Long id;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.user.type.desc}")
    private Short type;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.user.status.desc}")
    private Short status;
    public static class Status
    {
        public static final short DELETED = -1;
        public static final short INACTIVE = 0;
        public static final short ACTIVE = 1;
        public static final short WITHDRAWN = 2;

        public static String toString(final Short id)
        {
            String s = null;

            if (null != id) {
                if (INACTIVE == id) {
                    s = "inactive";
                } else if (ACTIVE == id) {
                    s = "active";
                } else if (WITHDRAWN == id) {
                    s = "withdrawn";
                } else if (DELETED == id) {
                    s = "deleted";
                }
            }

            return s;
        }
    }

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.statusLastUpdatedWhen.desc}")
    private Long statusLastUpdatedWhen;

    @Column(nullable = false)
    @NotNull
    @ApiModelProperty(value = "${ncmmary.doc.user.role.desc}")
    private Short role;
    public static class Role
    {
        public final static short GENERAL = 0; //
        public final static short ADMIN = 99;//
        public final static short SUPER_ADMIN = 100;

        public static String toString(final Short id)
        {
            String s = null;

            if (null != id) {
                if (GENERAL == id) {
                    s = "general";
                } else if (ADMIN == id) {
                    s = "admin";
                } else if (SUPER_ADMIN == id) {
                    s = "super-admin";
                }
            }

            return s;
        }
    }

    private Long parentId;

    @ApiModelProperty(value = "${ncmmary.doc.createdWhen.desc}")
    private Long createdWhen;

    @ApiModelProperty(value = "${ncmmary.doc.updatedWhen.desc}")
    private Long updatedWhen;

    @ApiModelProperty(value = "${ncmmary.doc.user.name.desc}")
    private String name;

    @ApiModelProperty(value = "${ncmmary.doc.uniqueName.desc}")
    private String uniqueName;
     
    @ApiModelProperty(value = "${ncmmary.doc.user.password.desc}")
    private String password;

    @Column(unique=true)
    @ApiModelProperty(value = "${ncmmary.doc.user.email.desc}")
    private String email;

    @ApiModelProperty(value = "${ncmmary.doc.user.isEmailVerified.desc}")
    private Boolean isEmailVerified;

    @ApiModelProperty(value = "${ncmmary.doc.user.phone.desc}")
    private String phone;

    @Column(unique=true)
    @ApiModelProperty(value = "${ncmmary.doc.user.mobilePhone.desc}")
    private String mobilePhone;

    @ApiModelProperty(value = "${ncmmary.doc.user.isMobilePhoneVerified.desc}")
    private Boolean isMobilePhoneVerified;

    private String language;

    private String address;

    private String city;

    private String country;

    @JsonProperty
    @ApiModelProperty(value = "${ncmmary.doc.user.statusString.desc}")
    public String getStatusString()
    {
        return Status.toString(this.status);
    }

    @JsonProperty
    @ApiModelProperty(value = "${ncmmary.doc.user.roleString.desc}")
    public String getRoleString()
    {
        return Role.toString(this.role);
    }
}
