package com.ncmmary.entity;

import com.ncmmary.i18n.MessageView;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Transient;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Type;
import org.springframework.context.MessageSource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModelProperty;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class AuthToken implements Serializable
{
    private static final Logger log = LoggerFactory.getLogger(AuthToken.class);

    @Id
    @ApiModelProperty(value = "${ncmmary.doc.authToken.id.desc}")
    private String id;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.authToken.userId.desc}")
    private Long userId;

    @Column
    @ApiModelProperty(value = "${ncmamry.doc.authToken.type.desc}")
    private Short type;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.authToken.expiredWhen.desc}")
    private Long expiredWhen;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.authToken.status.desc}")
    private Short status;
    public static class Status
    {
        public static final short VALID = 1;
        public static final short EXPIRED = 2;
    }

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.authToken.statusLastUpdatedWhen.desc}")
    private Long statusLastUpdatedWhen;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.authToken.createdWhen.desc}")
    private Long createdWhen;
}
