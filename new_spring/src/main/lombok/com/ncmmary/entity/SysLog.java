package com.ncmmary.entity;

import com.ncmmary.i18n.MessageView;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Transient;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Type;
import org.springframework.context.MessageSource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.StdConverter;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import io.swagger.annotations.ApiModelProperty;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class SysLog extends Content implements Serializable
{
    private static final Logger log = LoggerFactory.getLogger(SysLog.class);

    public static class IdToStringConverter extends StdConverter<Long, String> {
        @Override
        public String convert(Long id) {
            return id.toString();
        }
    }

    public static class StringToIdConverter extends StdConverter<String, Long> {
        @Override
        public Long convert(String id) {
            return Long.valueOf(id);
        }
    }

    @Id
    @JsonSerialize(converter=IdToStringConverter.class)
    @JsonDeserialize(converter=StringToIdConverter.class)
    @ApiModelProperty(value = "${ncmmary.doc.syslog.id.desc}")
    private Long id;
    
    @Column
    @ApiModelProperty(value = "${ncmmary.doc.syslog.type.desc}")
    private Short type;
    public static class Type
    {
        public static final short FOO = 0;
        public static final short BAR = 0;

        public static String toString(final Short id)
        {
            String s = null;

            if (FOO == id) {
                s = "foo";
            } else if (BAR == id) {
                s = "bar";
            }

            return s;
        }
    }

    @JsonProperty
    @ApiModelProperty(value = "${ncmmary.doc.syslog.typeString.desc}")
    public String getTypeString()
    {
        return Type.toString(this.type);
    }

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.syslog.status.desc}")
    private Short status;
    public static class Status
    {
        public static final short DELETED = -1;
        public static final short STABLE = 1;

        public static String toString(final Short id)
        {
            String s = null;

            if (DELETED == id) {
                s = "deleted";
            } else if (STABLE == id) {
                s = "stable";
            }

            return s;
        }
    }

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.statusLastUpdatedWhen.desc}")
    private Long statusLastUpdatedWhen;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.syslog.targetDeviceId.desc}")
    private String targetDeviceId;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.syslog.moduleId.desc}")
    private String moduleId;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.syslog.power.desc}")
    private Double power;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.syslog.batteryCapacity.desc}")
    private Double batteryCapacity;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.syslog.longitude.desc}")
    private Double longitude;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.syslog.latitude.desc}")
    private Double latitude;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.syslog.time.desc}")
    private Long time;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.createdWhen.desc}")
    private Long createdWhen;
}

