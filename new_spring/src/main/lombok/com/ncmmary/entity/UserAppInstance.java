package com.ncmmary.entity;

import com.ncmmary.i18n.MessageView;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Transient;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.Type;
import org.springframework.context.MessageSource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModelProperty;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class UserAppInstance extends Content implements Serializable
{
    private static final Logger log = LoggerFactory.getLogger(UserAppInstance.class);

    @Id
    @JsonView(value={MessageView.class})
    @ApiModelProperty(value = "${ncmmary.doc.userAppInstance.id.desc}")
    private Long id;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.userAppInstance.appInstanceId.desc}")
    private String appInstanceId;

    @Column
    @JsonIgnore
    private String appInstanceIdShort;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.userAppInstance.os.desc}")
    private Short os;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.userAppInstance.mtid.desc}")
    private String mtid;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.userAppInstance.status.desc}")
    private Short status;
    public static class Status
    {
        public static final short DELETED = -1;
        public static final short STABLE = 1;

        public static String toString(final Short id)
        {
            String s = null;

            if (null != id) {
                if (DELETED == id) {
                    s = "deleted";
                }
                else if (STABLE == id) {
                    s = "stable";
                }
            }
            return s;
        }
    }

    @JsonProperty
    @ApiModelProperty(value = "${ncmmary.doc.userAppInstance.statusString.desc}")
    public String getStatusString()
    {
        return Status.toString(this.status);
    }

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.statusLastUpdatedWhen.desc}")
    private Long statusLastUpdatedWhen;

    @Column
    @ApiModelProperty(value = "${ncmmary.doc.createdWhen.desc}")
    private Long createdWhen;

}
