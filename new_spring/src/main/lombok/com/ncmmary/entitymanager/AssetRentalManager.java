package com.ncmmary.entitymanager;


import com.ncmmary.exception.NotFoundException;

import com.ncmmary.core.Clock;

import com.ncmmary.entity.AssetRental;
import com.ncmmary.entity.IdGenerator;

import com.ncmmary.repository.AssetRentalRepository;

import com.ncmmary.parameters.GetAssetRentalsParameters;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.querydsl.core.types.Predicate;

import lombok.Data;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Collection;
import java.util.Base64;


@Service
public class AssetRentalManager
{
    private static final Logger log
        = LoggerFactory.getLogger(AssetRentalManager.class);

    @Autowired
    private Clock clock;

    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    private AssetRentalRepository assetRentalRepository;

    @Data
    public static class AssetRentalList
    {
        @Data
        public static class Cursor
        {
            private Long id;

        }

        private List<AssetRental> assetRentals;
        private Boolean isLoading;
        private String prev;
        private String next;

        public void setPrev(Cursor prevCursor) {
            try{
                ObjectMapper mapper = new ObjectMapper();
                this.prev = Base64.getUrlEncoder().encodeToString(mapper.writeValueAsBytes(prevCursor));
            }catch(Exception e) {}
        }

        public void setNext(Cursor nextCursor) {
            try{
                ObjectMapper mapper = new ObjectMapper();
                this.next = Base64.getUrlEncoder().encodeToString(mapper.writeValueAsBytes(nextCursor));
            }catch(Exception e) {}
        }
    }

    public AssetRentalList getAssetRentals(GetAssetRentalsParameters params)
    {
        return null;
    }
}
