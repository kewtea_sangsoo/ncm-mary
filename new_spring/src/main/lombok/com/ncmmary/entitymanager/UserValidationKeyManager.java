package com.ncmmary.entitymanager;

import com.ncmmary.exception.NotFoundException;
import com.ncmmary.exception.UserValidationFailedException;
import com.ncmmary.auth.AuthService;

import com.ncmmary.entity.User;
import com.ncmmary.entity.AuthToken;
import com.ncmmary.entity.UserValidationKey;
import com.ncmmary.entitymanager.AuthTokenManager;
import com.ncmmary.entitymanager.UserManager;

import com.ncmmary.repository.UserValidationKeyRepository;

import com.ncmmary.core.Clock;


import com.ncmmary.parameters.ValidateUserForm;
import com.ncmmary.parameters.IssueNewUserValidationKeyForm;

import com.ncmmary.util.SMS;
import com.ncmmary.util.Catchall;


import com.querydsl.core.types.Predicate;

import lombok.Data;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import org.springframework.validation.Errors;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Collection;


@Service
public class UserValidationKeyManager
{
    private static final Logger log
        = LoggerFactory.getLogger(UserValidationKeyManager.class);

    @Autowired
    private Clock clock;


    @Autowired
    private AuthService authService;

    @Autowired
    private SMS sms;

    @Autowired
    @Lazy
    private AuthTokenManager authTokenManager;

    @Autowired
    @Lazy
    private UserManager userManager;

    @Autowired
    private UserValidationKeyRepository userValidationKeyRepository;

    @Transactional(readOnly=false)
    public UserValidationKey getById(Long id)
    {
        return userValidationKeyRepository.findById(id).orElse(null);
    }

    public ValidateReturn validate(ValidateUserForm form, Errors errors)
    {
        ValidateReturn validateReturn = new ValidateReturn();

        Long currentTimeInMillis = clock.getTime();

        UserValidationKey userValidationKey = validate(form.getUserId(), form.getCode());

        Boolean login = form.getLogin();
        login = (null == login ? false : login);

        if (login) {
            AuthToken authToken 
                = authTokenManager.register(
                    null, 
                    userValidationKey.getUserId(), 
                    AuthToken.Status.VALID,
                    currentTimeInMillis);

            authService.login(authToken.getId());

            validateReturn.setAuthToken(authToken);
            validateReturn.setUser(userManager.getCurrentUser());

        }

        validateReturn.setUserValidationKey(userValidationKey);

        return validateReturn;

    }

    @Data
    public static class ValidateReturn
    {
        private User user;
        private AuthToken authToken;
        private UserValidationKey userValidationKey;
    }

    @Transactional
    public UserValidationKey validate(Long userId, String value) throws UserValidationFailedException {

        UserValidationKey userValidationKey = getById(userId);

        if (null != userValidationKey && userValidationKey.getValue().equals(value)) {
            userValidationKey.setStatus(UserValidationKey.Status.VALIDATED);
        } else {
            throw new UserValidationFailedException();
        }

        return userValidationKey;
    }

    public void issueNew(IssueNewUserValidationKeyForm form, Errors errors)
    {
        Long userId = form.getUserId();
        String mobilePhone = form.getMobilePhone();

        User user = null;

        if (null != userId) {
            user = userManager.getById(userId);

        } else if (null != mobilePhone && !mobilePhone.equals("")) {
            user = userManager.getByMobilePhone(mobilePhone);
        }

        if (null != user) {

            Long currentTimeInMillis = clock.getTime();

            UserValidationKey userValidationKey 
                = issueNew(user.getId(), user.getEmail(), user.getMobilePhone(), UserValidationKey.Type.CODE, currentTimeInMillis);

        }
    }

    public UserValidationKey issueNew(final Long userId, final String email, final String mobilePhone, final Short type, Long when)
    {
        String value = null;

        if (UserValidationKey.Type.CODE == type) {
            Long randomLong = Math.abs(Catchall.generateRandomLong(when));
            value = randomLong.toString().substring(0, 6);
        }

        UserValidationKey userValidationKey 
            = registerOrUpdate(userId, email, mobilePhone, type, value, UserValidationKey.Status.NOT_VALIDATED, when);

        sms.sendMessage(userValidationKey.getMobilePhone(), userValidationKey.getValue());

        return userValidationKey;
    }

    @Transactional(readOnly=false)
    public UserValidationKey registerOrUpdate(final Long userId, final String email, final String mobilePhone, final Short type, final String value, final Short status, Long when)
    {
        UserValidationKey userValidationKey = null;
        try{
            userValidationKey = getById(userId);
            if (null == userValidationKey) {
                throw new NotFoundException();
            }
            userValidationKey = update(userId, true, type, true, value);
            if (!userValidationKey.getStatus().equals(status)) {
                userValidationKey = changeStatus(userValidationKey.getUserId(), status, when, true);
            }

        }catch(NotFoundException e){

            userValidationKey = register(userId, email, mobilePhone, type, value, when);

        }

        return userValidationKey;
    }

    @Transactional
    public UserValidationKey register(Long userId, String email, String mobilePhone, Short type, String value, Long createdWhen)
    {
        UserValidationKey userValidationKey = new UserValidationKey();
        userValidationKey.setUserId(userId);
        userValidationKey.setEmail(email);
        userValidationKey.setMobilePhone(mobilePhone);
        userValidationKey.setType(type);
        userValidationKey.setValue(value);
        userValidationKey.setStatus(UserValidationKey.Status.NOT_VALIDATED);
        userValidationKey.setStatusLastUpdatedWhen(createdWhen);
        userValidationKey.setCreatedWhen(createdWhen);

        userValidationKey = userValidationKeyRepository.persist(userValidationKey);

        return userValidationKey;

    }

    @Transactional(readOnly=false)
    public UserValidationKey update(Long userId,
                                    Boolean typeModified,
                                    Short type,
                                    Boolean valueModified,
                                    String value) throws NotFoundException
    {

        UserValidationKey userValidationKey = getById(userId);

        if (null == userValidationKey) {
            throw new NotFoundException();
        }

        if (null != typeModified && typeModified) {
            userValidationKey.setType(type);
        }

        if (null != valueModified && valueModified) {
            userValidationKey.setValue(value);
        }

        userValidationKey = userValidationKeyRepository.save(userValidationKey);

        return userValidationKey;
    }

    @Transactional(readOnly=false)
    private UserValidationKey changeStatus(Long userId, Short status, Long statusLastUpdatedWhen, boolean ignoreLoop) throws NotFoundException
    {
        UserValidationKey userValidationKey = getById(userId);

        if (null == userValidationKey)
        {
            throw new NotFoundException();
        }

        userValidationKey.setStatus(status);
        userValidationKey.setStatusLastUpdatedWhen(statusLastUpdatedWhen);

        userValidationKey = userValidationKeyRepository.save(userValidationKey);

        return userValidationKey;
    }
}
