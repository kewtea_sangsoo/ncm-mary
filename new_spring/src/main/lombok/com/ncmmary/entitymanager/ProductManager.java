package com.ncmmary.entitymanager;

import com.ncmmary.core.Clock;

import com.ncmmary.entity.Product;
import com.ncmmary.entity.IdGenerator;

import com.ncmmary.repository.ProductRepository;

import com.ncmmary.parameters.RegisterProductForm;
import com.ncmmary.parameters.UpdateProductForm;
import com.ncmmary.parameters.DeleteProductsForm;
import com.ncmmary.parameters.GetProductsParameters;

import com.querydsl.core.types.Predicate;

import lombok.Data;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.validation.Errors;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Collection;


@Service
public class ProductManager
{
    private static final Logger log 
        = LoggerFactory.getLogger(ProductManager.class);

    @Autowired
    private Clock clock;

    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    private ProductRepository productRepository;

    @Data
    public static class ProductList
    {
        private List<Product> products;

    }

    public ProductList getProducts(GetProductsParameters params)
    {
        ProductList productList = new ProductList();

        return productList;
    }

    public Product register(RegisterProductForm form, Errors errors)
    {
        Product product = null;


        return product;
    }

    public Product update(UpdateProductForm form, Errors errors)
    {
        Product product = null;


        return product;
    }

    public Product delete(DeleteProductsForm form, Errors errors)
    {
        Product product = null;


        return product;
    }

    private List<Product> asList(Page<Product> page)
    {
        List<Product> products = null;

        if(null != page)
        {
            products = page.getContent();
        }

        return products;
    }
}

