package com.ncmmary.entitymanager;

import com.ncmmary.core.Clock;

import com.ncmmary.entity.Transaction;
import com.ncmmary.entity.IdGenerator;

import com.ncmmary.repository.TransactionRepository;

import com.ncmmary.parameters.RegisterTransactionForm;
import com.ncmmary.parameters.UpdateTransactionForm;
import com.ncmmary.parameters.DeleteTransactionsForm;
import com.ncmmary.parameters.GetTransactionsParameters;

import com.querydsl.core.types.Predicate;

import lombok.Data;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Collection;


@Service(value="tManager") // To avoid conflicting with Spring Framework 'transactionManager'
public class TransactionManager
{
    private static final Logger log 
        = LoggerFactory.getLogger(TransactionManager.class);

    @Autowired
    private Clock clock;

    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    private TransactionRepository transactionRepository;

    @Data
    public static class TransactionList
    {
        private List<Transaction> transactions;

    }

    public TransactionList getTransactions(GetTransactionsParameters params)
    {
        TransactionList transactionList = new TransactionList();

        return transactionList;
    }

    public Transaction register(RegisterTransactionForm form, Errors errors)
    {
        Transaction transaction = null;


        return transaction;
    }

    public Transaction update(UpdateTransactionForm form, Errors errors)
    {
        Transaction transaction = null;


        return transaction;
    }

    public Transaction delete(DeleteTransactionsForm form, Errors errors)
    {
        Transaction transaction = null;


        return transaction;
    }
}

