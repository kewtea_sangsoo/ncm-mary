package com.ncmmary.entitymanager;

import com.ncmmary.exception.NotFoundException;

import com.ncmmary.core.Clock;

import com.ncmmary.entity.Asset;
import com.ncmmary.entity.IdGenerator;

import com.ncmmary.repository.AssetRepository;

import com.ncmmary.security.GlobalPermissionEvaluator;

import com.ncmmary.parameters.RegisterAssetForm;
import com.ncmmary.parameters.UpdateAssetForm;
import com.ncmmary.parameters.DeleteAssetsForm;
import com.ncmmary.parameters.GetAssetsParameters;
import com.ncmmary.parameters.RentAssetForm;
import com.ncmmary.parameters.SuspendAssetForm;
import com.ncmmary.parameters.ReturnAssetForm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.querydsl.core.types.Predicate;

import lombok.Data;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.PermissionEvaluator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Collection;
import java.util.Base64;


@Service
public class AssetManager
{
    private static final Logger log 
        = LoggerFactory.getLogger(AssetManager.class);

    @Autowired
    private Clock clock;

    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    private AssetRepository assetRepository;

    @Autowired
    private GlobalPermissionEvaluator globalPermissionEvaluator;

    @Data
    public static class AssetList
    {
        @Data
        public static class Cursor
        {
            private Long id;
            private Long ct;
            private Long lut;
        }

        private List<Asset> assets;
        private Boolean isLoading;
        private String prev;
        private String next;

        public void setPrev(Cursor prevCursor) {
            try{
                ObjectMapper mapper = new ObjectMapper();
                this.prev = Base64.getUrlEncoder().encodeToString(mapper.writeValueAsBytes(prevCursor));
            }catch(Exception e) {}
        }

        public void setNext(Cursor nextCursor) {
            try{
                ObjectMapper mapper = new ObjectMapper();
                this.next = Base64.getUrlEncoder().encodeToString(mapper.writeValueAsBytes(nextCursor));
            }catch(Exception e) {}
        }
    }

    
    public AssetList getAssets(GetAssetsParameters params)
    {

        String p = params.getP();
        Long pId = null;
        Long pCt = null;
        Long pLut = null;
        if (null != p && !p.equals("")) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                AssetList.Cursor cursor = mapper.readValue(Base64.getUrlDecoder().decode(p), AssetList.Cursor.class);
                pId = cursor.getId();
                pCt = cursor.getCt();
                pLut = cursor.getLut();

            }catch(Exception e) {}
        }

        AssetList assetList = new AssetList();

        Double latLowerBound = null;
        Double latUpperBound = null;
        Double lonLowerBound = null;
        Double lonUpperBound = null;

        Predicate predicate 
            = assetRepository.getPredicate(
                params.getIds(), 
                params.getT(), 
                params.getS(), 
                latLowerBound, 
                latUpperBound, 
                lonLowerBound, 
                lonUpperBound, 
                params.getCtl(),
                params.getCtu(),
                params.getLutl(),
                params.getLutu(),
                pId, 
                pCt,
                pLut,
                params.getO(), 
                params.getD());

        Pageable pageable = assetRepository.getPageable(
            0,
            params.getN(),
            params.getO(),
            params.getD());

        List<Asset> assets = asList(assetRepository.findAll(predicate, pageable));

        if (null != assets) {
            int size = assets.size();
            if (0 < size) {
                Asset first = assets.get(0);
                Asset last = assets.get(size-1);

                AssetList.Cursor prevCursor = new AssetList.Cursor();
                prevCursor.setId(first.getId());
                prevCursor.setCt(first.getCreatedWhen());
                prevCursor.setLut(first.getUpdatedWhen());
                assetList.setPrev(prevCursor);

                AssetList.Cursor nextCursor = new AssetList.Cursor();
                nextCursor.setId(last.getId());
                nextCursor.setCt(last.getCreatedWhen());
                nextCursor.setLut(last.getUpdatedWhen());
                assetList.setNext(nextCursor);
            }
        }
        
        assetList.setAssets(assets);
        assetList.setIsLoading(false);

        return assetList;
    }

    public Asset register(RegisterAssetForm form, Errors errors)
    {
        Asset asset = null;

        Short type = form.getType();
        Double latitude = form.getLatitude();
        Double longitude = form.getLongitude();

        Short status = Asset.Status.AVAILABLE;
        Boolean hasBattery = true;
        Double battery = 100.0;


        Long currentTimeInMillis = clock.getTime();

        
        asset = register(type, status, latitude, longitude, hasBattery, battery, currentTimeInMillis);

        return asset;
    }

    public Asset register(Short type, Short status, Double latitude, Double longitude, Boolean hasBattery, Double battery, Long createdWhen)
    {
        Asset asset = new Asset();
        asset.setId(idGenerator.genIdLong());
        asset.setType(type);
        asset.setStatus(status);
        asset.setStatusLastUpdatedWhen(createdWhen);
        asset.setLatitude(latitude);
        asset.setLongitude(longitude);
        asset.setHasBattery(hasBattery);
        asset.setBattery(battery);
        asset.setCreatedWhen(createdWhen);

        asset = assetRepository.persist(asset);

        return asset;
    }

    public Asset update(UpdateAssetForm form, Errors errors)
    {
        Asset asset = null;

        Long id = form.getId();
        Boolean typeModified = form.getTypeModified();
        Short type = form.getType();
        Boolean longitudeModified = form.getLongitudeModified();
        Double longitude = form.getLongitude();
        Boolean latitudeModified = form.getLatitudeModified();
        Double latitude = form.getLatitude();
        Boolean statusModified = form.getStatusModified();
        Short status = form.getStatus();
        Boolean hasBatteryModified = form.getHasBatteryModified();
        Boolean hasBattery = form.getHasBattery();
        Boolean batteryModified = form.getBatteryModified();
        Double battery = form.getBattery();

        Long currentTimeInMillis = clock.getTime();

        if (null != statusModified && statusModified) {
            asset = changeStatus(id, status, currentTimeInMillis, false);
        }

        asset = update(
                    id, 
                    typeModified, 
                    type, 
                    longitudeModified, 
                    longitude, 
                    latitudeModified, 
                    latitude, 
                    hasBatteryModified, 
                    hasBattery, 
                    batteryModified, 
                    battery);

        return asset;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @Transactional
    public Asset update(Long id, 
                        Boolean typeModified, 
                        Short type, 
                        Boolean longitudeModified, 
                        Double longitude, 
                        Boolean latitudeModified, 
                        Double latitude,
                        Boolean hasBatteryModified,
                        Boolean hasBattery,
                        Boolean batteryModified,
                        Double battery) throws NotFoundException
    {
        Asset asset = null;

        asset = assetRepository.findById(id).orElse(null);
        
        if (null == asset) {
            throw new NotFoundException();
        }

        if (null != typeModified && typeModified) {
            asset.setType(type);
        }

        if (null != longitudeModified && longitudeModified) {
            asset.setLongitude(longitude);
        }

        if (null != latitudeModified && latitudeModified) {
            asset.setLatitude(latitude);
        }

        if (null != hasBatteryModified && hasBatteryModified) {
            asset.setHasBattery(hasBattery);
        }

        if (null != batteryModified && batteryModified) {
            asset.setBattery(battery);
        }

        asset = assetRepository.save(asset);

        return asset;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @Transactional
    private Asset changeStatus(Long id, Short status, Long statusLastUpdatedWhen, boolean ignoreLoop) throws NotFoundException
    {
        Asset asset = assetRepository.findById(id).orElse(null);

        if (null == asset)
        {
            throw new NotFoundException();
        }

        asset.setStatus(status);
        asset.setStatusLastUpdatedWhen(statusLastUpdatedWhen);

        asset = assetRepository.save(asset);

        return asset;
    }

    public Asset delete(DeleteAssetsForm form, Errors errors)
    {
        Asset asset = null;


        return asset;
    }

    @Data
    public static class RentAssetReturn
    {
        private Asset asset;
    }

    public RentAssetReturn rent(RentAssetForm rentAssetForm, Errors errors)
    {
        RentAssetReturn rentAssetReturn = new RentAssetReturn();


        return rentAssetReturn;
    }

    @Data
    public static class SuspendAssetReturn
    {
        private Asset asset;

    }

    public SuspendAssetReturn suspend(SuspendAssetForm suspendAssetForm, Errors errors)
    {
        SuspendAssetReturn suspendAssetReturn = new SuspendAssetReturn();

        return suspendAssetReturn;
    }

    @Data
    public static class ReturnAssetReturn
    {
        private Asset asset;
    }

    public ReturnAssetReturn suspend(ReturnAssetForm returnAssetForm, Errors errors)
    {
        ReturnAssetReturn returnAssetReturn = new ReturnAssetReturn();

        return returnAssetReturn;
    }

    private List<Asset> asList(Page<Asset> page)
    {
        List<Asset> assets = null;

        if (null != page)
        {
            assets = page.getContent();
        }

        return assets;
    }

}

