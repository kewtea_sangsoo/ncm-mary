package com.ncmmary.entitymanager;

import com.ncmmary.core.Clock;

import com.ncmmary.entity.ServiceLog;
import com.ncmmary.entity.IdGenerator;

import com.ncmmary.repository.ServiceLogRepository;

import com.ncmmary.parameters.RegisterServiceLogForm;
import com.ncmmary.parameters.UpdateServiceLogForm;
import com.ncmmary.parameters.DeleteServiceLogsForm;
import com.ncmmary.parameters.GetServiceLogsParameters;

import com.querydsl.core.types.Predicate;

import lombok.Data;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Collection;


@Service
public class ServiceLogManager
{
    private static final Logger log 
        = LoggerFactory.getLogger(ServiceLogManager.class);

    @Autowired
    private Clock clock;

    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    private ServiceLogRepository serviceLogRepository;

    @Data
    public static class ServiceLogList
    {
        private List<ServiceLog> serviceLogs;

    }

    public ServiceLogList getServiceLogs(GetServiceLogsParameters params)
    {
        ServiceLogList serviceLogList = new ServiceLogList();

        return serviceLogList;
    }

    public ServiceLog register(RegisterServiceLogForm form)
    {
        ServiceLog serviceLog = null;


        return serviceLog;
    }

    public ServiceLog update(UpdateServiceLogForm form)
    {
        ServiceLog serviceLog = null;


        return serviceLog;
    }

    public ServiceLog delete(DeleteServiceLogsForm form)
    {
        ServiceLog serviceLog = null;


        return serviceLog;
    }
}

