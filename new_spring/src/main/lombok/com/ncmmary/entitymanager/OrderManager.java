package com.ncmmary.entitymanager;

import com.ncmmary.core.Clock;

import com.ncmmary.entity.Order;
import com.ncmmary.entity.IdGenerator;

import com.ncmmary.repository.OrderRepository;

import com.ncmmary.parameters.RegisterOrderForm;
import com.ncmmary.parameters.UpdateOrderForm;
import com.ncmmary.parameters.DeleteOrdersForm;
import com.ncmmary.parameters.GetOrdersParameters;

import com.querydsl.core.types.Predicate;

import lombok.Data;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Collection;


@Service
public class OrderManager
{
    private static final Logger log 
        = LoggerFactory.getLogger(OrderManager.class);

    @Autowired
    private Clock clock;

    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    private OrderRepository orderRepository;

    @Data
    public static class OrderList
    {
        private List<Order> orders;

    }

    public OrderList getOrders(GetOrdersParameters params)
    {
        OrderList orderList = new OrderList();

        return orderList;
    }

    public Order register(RegisterOrderForm form, Errors errors)
    {
        Order order = null;


        return order;
    }

    public Order update(UpdateOrderForm form, Errors errors)
    {
        Order order = null;


        return order;
    }

    public Order delete(DeleteOrdersForm form, Errors errors)
    {
        Order order = null;


        return order;
    }
}

