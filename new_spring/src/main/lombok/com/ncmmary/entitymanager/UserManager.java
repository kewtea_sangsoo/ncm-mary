package com.ncmmary.entitymanager;

import com.ncmmary.exception.NotFoundException;
import com.ncmmary.exception.EmailAlreadyInUseException;
import com.ncmmary.exception.MobilePhoneAlreadyInUseException;

import com.ncmmary.core.Clock;

import com.ncmmary.auth.AuthService;
import com.ncmmary.security.MyUserDetails;
import com.ncmmary.entity.User;
import com.ncmmary.entity.UserAppInstance;
import com.ncmmary.entity.UserAppInstanceAssociation;
import com.ncmmary.entity.UserValidationKey;
import com.ncmmary.entity.IdGenerator;
import com.ncmmary.entity.AuthToken;

import com.ncmmary.repository.UserRepository;
import com.ncmmary.entitymanager.UserAppInstanceManager;
import com.ncmmary.entitymanager.UserAppInstanceAssociationManager;
import com.ncmmary.entitymanager.AuthTokenManager;
import com.ncmmary.entitymanager.UserValidationKeyManager;

import com.ncmmary.parameters.LoginUserForm;
import com.ncmmary.parameters.WithdrawUserForm;
import com.ncmmary.parameters.RegisterUserForm;
import com.ncmmary.parameters.UpdateUserForm;
import com.ncmmary.parameters.DeleteUsersForm;
import com.ncmmary.parameters.GetUsersParameters;

import com.ncmmary.util.PhoneNumberUtil;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.querydsl.core.types.Predicate;

import lombok.Data;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import org.springframework.validation.Errors;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.PermissionEvaluator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.Base64;


@Service
public class UserManager
{
    private static final Logger log
        = LoggerFactory.getLogger(UserManager.class);

    @Autowired
    private Clock clock;

    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    @Lazy
    private PasswordEncoder passwordEncoder;

    @Autowired
    @Lazy
    private AuthService authService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    @Lazy
    private UserAppInstanceManager userAppInstanceManager;

    @Autowired
    @Lazy
    private UserAppInstanceAssociationManager userAppInstanceAssociationManager;

    @Autowired
    @Lazy
    private AuthTokenManager authTokenManager;

    @Autowired
    @Lazy
    private UserValidationKeyManager userValidationKeyManager;

    @Data
    public static class UserList
    {
        @Data
        private static class Cursor
        {
            private Long id;
            private Long ct;
            private Long lut;
        }

        private List<User> users;
        private Boolean isLoading;
        private String prev;
        private String next;

        public void setPrev(Cursor prevCursor) {
            try{
                ObjectMapper mapper = new ObjectMapper();
                this.prev = Base64.getUrlEncoder().encodeToString(mapper.writeValueAsBytes(prevCursor));
            }catch(Exception e) {}
        }

        public void setNext(Cursor nextCursor) {
            try{
                ObjectMapper mapper = new ObjectMapper();
                this.next = Base64.getUrlEncoder().encodeToString(mapper.writeValueAsBytes(nextCursor));
            }catch(Exception e) {}
        }
    }

    public User getCurrentUser()
    {
        User user = null;

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        if (principal instanceof UserDetails) {
            Long id = ((MyUserDetails)principal).getUserId();
            user = userRepository.findById(id).orElse(null);
        }

        return user;
    }

    @Transactional
    public User getById(Long id)
    {
         return userRepository.findById(id).orElse(null);
    }

    @Transactional
    public User getByEmail(String email)
    {
        return userRepository.findByEmail(email).orElse(null);
    }

    @Transactional
    public User getByMobilePhone(String mobilePhone)
    {
        return userRepository.findByMobilePhone(mobilePhone).orElse(null);
    }

    @Data
    public static class LoginReturn
    {
        private User user;
        private AuthToken authToken;
        private UserAppInstance userAppInstance;
        private UserAppInstanceAssociation userAppInstanceAssociation;
    }

    public LoginReturn login(LoginUserForm loginUserForm, Errors errors)
    {
        LoginReturn loginReturn = new LoginReturn();

        String authToken = loginUserForm.getAuthToken();
        String username = loginUserForm.getUsername();
        String password = loginUserForm.getPassword();

        Authentication authentication = null;

        if (null != username && null != password)
        {
            authentication = authService.login(username, password);
        }
        else
        {
            authentication = authService.login(authToken);
        }

        

        loginReturn.setUser(getCurrentUser());

        return loginReturn;
    }

    public User logout()
    {
        User user = null;

        return user;
    }

    //@PreAuthorize("hasRole('ADMIN') or principal ...)
    public User withdraw(WithdrawUserForm withdrawUserForm, Errors errors)
    {
        User user = null;

        Long id = withdrawUserForm.getId();
        String password = withdrawUserForm.getPassword();

        user = getById(id);

        if (null == user) {
            throw new NotFoundException();
        }

        Long currentTimeInMillis = clock.getTime();

        // check the password or the validation-code
        
        boolean isMatch = false;

        if (passwordEncoder.matches(password, user.getPassword())) {
            isMatch = true;
        } else {

            UserValidationKey userValidationKey 
                = userValidationKeyManager.validate(id, password);

            user = update(user.getId(), false, user.getRole(), false, user.getMobilePhone(), true, true, currentTimeInMillis);

            isMatch = true;
        }

        if (isMatch) {


            user = changeStatus(user.getId(), User.Status.WITHDRAWN, currentTimeInMillis, false);

        } else {
            throw new NotFoundException();
        }

        return user;
    }

    @Transactional
    private User changeStatus(Long id, Short status, Long statusLastUpdatedWhen, boolean ignoreLoop) throws NotFoundException
    {
        User user = userRepository.findById(id).orElse(null);

        if (null == user)
        {
            throw new NotFoundException();
        }

        user.setStatus(status);
        user.setStatusLastUpdatedWhen(statusLastUpdatedWhen);

        user = userRepository.save(user);

        return user;
    }

    public UserList getUsers(GetUsersParameters params)
    {
        String p = params.getP();
        Long pId = null;
        Long pCt = null;
        Long pLut = null;
        if (null != p && !p.equals("")) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                UserList.Cursor cursor = mapper.readValue(Base64.getUrlDecoder().decode(p), UserList.Cursor.class);
                pId = cursor.getId();
                pCt = cursor.getCt();
                pLut = cursor.getLut();

            }catch(Exception e) {}
        }

        UserList userList = new UserList();

        Predicate predicate 
            = userRepository.getPredicate(
                params.getIds(),
                params.getR(),
                params.getS(),
                params.getCtl(),
                params.getCtu(),
                params.getLutl(),
                params.getLutu(),
                pId,
                pCt,
                pLut,
                params.getO(),
                params.getD());

        Pageable pageable = userRepository.getPageable(
            0,
            params.getN(),
            params.getO(),
            params.getD());

        List<User> users = asList(userRepository.findAll(predicate, pageable));

        if (null != users) {
            int size = users.size();
            if (0 < size) {
                User first = users.get(0);
                User last = users.get(size-1);

                UserList.Cursor prevCursor = new UserList.Cursor();
                prevCursor.setId(first.getId());
                prevCursor.setCt(first.getCreatedWhen());
                prevCursor.setLut(first.getUpdatedWhen());
                userList.setPrev(prevCursor);

                UserList.Cursor nextCursor = new UserList.Cursor();
                nextCursor.setId(last.getId());
                nextCursor.setCt(last.getCreatedWhen());
                nextCursor.setLut(last.getUpdatedWhen());
                userList.setNext(nextCursor);
            }
        }

        userList.setUsers(users);
        userList.setIsLoading(false);

        return userList;
    }

    @Data
    public static class RegisterReturn
    {
        private User user;
        private AuthToken authToken;
        private UserAppInstance userAppInstance;
        private UserAppInstanceAssociation userAppInstanceAssociation;
        private UserValidationKey userValidationKey;
    }

    public RegisterReturn register(RegisterUserForm form, Errors errors)
    {
        RegisterReturn registerReturn = new RegisterReturn();

        User user = null;

        String email = idGenerator.genPseudoId(); //form.getEmail();
        String mobilePhone = form.getMobilePhone();
        if (null != mobilePhone && !mobilePhone.equals("")) {
            mobilePhone = PhoneNumberUtil.canonicalize(mobilePhone);
        }
        String password = idGenerator.genPseudoId(); //form.getPassword();
        String passwordConfirm = null; //form.getPasswordConfirm();
        String appInstanceId = form.getAppInstanceId();

        Long currentTimeInMillis = clock.getTime();

        String passwordEncoded = null;
        if (null != password && !password.equals(""))
        {
            passwordEncoded = passwordEncoder.encode(password);
        }

        if (null != email && !email.equals(""))
        {
            user = getByEmail(email);
            if (null != user) {
                throw new EmailAlreadyInUseException();
            }
        }

        if (null != mobilePhone && !mobilePhone.equals(""))
        {
            user = getByMobilePhone(mobilePhone);
            if (null != user) {
                throw new MobilePhoneAlreadyInUseException();
            }
        }

        user = register(User.Role.GENERAL, 
                            email, 
                            mobilePhone, 
                            passwordEncoded,
                            currentTimeInMillis);

        registerReturn.setUser(user);

        AuthToken authToken 
            = authTokenManager.register(
                null, 
                user.getId(), 
                AuthToken.Status.VALID, 
                currentTimeInMillis);

        registerReturn.setAuthToken(authToken);

        if (null != appInstanceId) {

            UserAppInstance userAppInstance 
                = userAppInstanceManager
                    .register(
                        appInstanceId, 
                        UserAppInstance.Status.STABLE, 
                        currentTimeInMillis);

            registerReturn.setUserAppInstance(userAppInstance);

            UserAppInstanceAssociation userAppInstanceAssociation 
                = userAppInstanceAssociationManager
                    .register(
                        user.getId(), 
                        userAppInstance.getId(), 
                        UserAppInstanceAssociation.Status.STABLE, 
                        currentTimeInMillis);

            registerReturn.setUserAppInstanceAssociation(userAppInstanceAssociation);
        }

        UserValidationKey userValidationKey = userValidationKeyManager.issueNew(user.getId(), user.getEmail(), user.getMobilePhone(), UserValidationKey.Type.CODE, currentTimeInMillis); 

        registerReturn.setUserValidationKey(userValidationKey);

        return registerReturn;
    }

    @Transactional
    public User register(Short role, String email, String mobilePhone, String passwordEncoded, Long createdWhen)
    {
        User user = new User();
        user.setId(idGenerator.genIdLong());
        user.setRole(role);
        user.setEmail(email);
        user.setIsEmailVerified(false);
        user.setMobilePhone(mobilePhone);
        user.setIsMobilePhoneVerified(false);
        user.setPassword(passwordEncoded);
        user.setStatus(User.Status.INACTIVE);
        user.setStatusLastUpdatedWhen(createdWhen);
        user.setCreatedWhen(createdWhen);

        user = userRepository.persist(user);

        return user;
    }

    public User update(UpdateUserForm form, Errors errors)
    {
        User user = null;

        Long id = form.getId();
        Boolean mobilePhoneModified = form.getMobilePhoneModified();
        String mobilePhone = form.getMobilePhone();
        Boolean isMobilePhoneVerifiedModified = form.getIsMobilePhoneVerifiedModified();
        Boolean isMobilePhoneVerified = form.getIsMobilePhoneVerified();
        Boolean roleModified = form.getRoleModified();
        Short role = form.getRole();
        Boolean statusModified = form.getStatusModified();
        Short status = form.getStatus();

        Long currentTimeInMillis = clock.getTime();

        if (null != statusModified && statusModified) {
            user = changeStatus(id, status, currentTimeInMillis, false);
        }

        user = update(
                    id,
                    roleModified,
                    role,
                    mobilePhoneModified,
                    mobilePhone,
                    isMobilePhoneVerifiedModified,
                    isMobilePhoneVerified,
                    currentTimeInMillis);

        return user;
    }

    @Transactional
    public User update(Long id, Boolean roleModified, Short role, Boolean mobilePhoneModified, String mobilePhone, Boolean isMobilePhoneVerifiedModified, Boolean isMobilePhoneVerified, Long when) throws NotFoundException
    {
        User user = getById(id);

        if (null == user) {
            throw new NotFoundException();
        }

        if (null != roleModified && roleModified) {
            user.setRole(role);
        }

        if (null != mobilePhoneModified && mobilePhoneModified) {
            user.setMobilePhone(mobilePhone);
        }

        if (null != isMobilePhoneVerifiedModified && isMobilePhoneVerifiedModified)
        {
            user.setIsMobilePhoneVerified(isMobilePhoneVerified);
        }

        user = userRepository.save(user);

        return user;

    }

    public List<User> delete(DeleteUsersForm form, Errors errors)
    {
        Collection<Long> userIds = form.getUserIds();

        List<User> users = new ArrayList<User>();

        Long currentTimeInMillis = clock.getTime();

        for (Long userId : userIds) {
            User user = delete(userId, currentTimeInMillis);
            users.add(user);
        }

        return users;
    }

    @Transactional
    public User delete(Long id, Long when)
    {

        User user = changeStatus(id, User.Status.DELETED, when, false);
        user = update(user.getId(), 
                        false, 
                        user.getRole(), 
                        true, 
                        "!" + user.getId().toString() + user.getMobilePhone(), 
                        false, 
                        user.getIsMobilePhoneVerified(), 
                        when);

        return user;
    }

    private List<User> asList(Page<User> page)
    {
        List<User> users = null;

        if(null != page)
        {
            users = page.getContent();
        }

        return users;
    }
}
