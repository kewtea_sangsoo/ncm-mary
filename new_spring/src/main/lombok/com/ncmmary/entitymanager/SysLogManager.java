package com.ncmmary.entitymanager;

import com.ncmmary.core.Clock;

import com.ncmmary.exception.NotFoundException;

import com.ncmmary.entity.SysLog;
import com.ncmmary.entity.IdGenerator;

import com.ncmmary.repository.SysLogRepository;

import com.ncmmary.parameters.RegisterSysLogForm;
import com.ncmmary.parameters.UpdateSysLogForm;
import com.ncmmary.parameters.DeleteSysLogsForm;
import com.ncmmary.parameters.GetSysLogsParameters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonAlias;

import com.querydsl.core.types.Predicate;

import lombok.Data;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.Collection;
import java.util.Base64;

@Service
public class SysLogManager
{
    private static final Logger log 
        = LoggerFactory.getLogger(SysLogManager.class);

    private MqttClient mqttClient;

    @Autowired
    private Clock clock;

    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    private SysLogRepository sysLogRepository;

    @Autowired
    private Environment env;

    @Data
    public static class SysLogList
    {
        @Data
        public static class Cursor
        {
            private Long id;
            private Long time;
        }

        private List<SysLog> sysLogs;
        private Boolean isLoading;
        private String prev;
        private String next;

        public void setPrev(Cursor prevCursor) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                this.prev = Base64.getUrlEncoder().encodeToString(mapper.writeValueAsBytes(prevCursor));
            }catch(Exception e) {}
        }

        public void setNext(Cursor nextCursor) {
            try{
                ObjectMapper mapper = new ObjectMapper();
                this.next = Base64.getUrlEncoder().encodeToString(mapper.writeValueAsBytes(nextCursor));
            }catch(Exception e) {}
        }

    }
    
    public String getMqttTopic() {

        return "mary1/report";

    }

    public String getMqttBrokerUrl() {

        String host = env.getProperty("ncmmary.mqtt.broker.host");
        String port = env.getProperty("ncmmary.mqtt.broker.port");

        if (null == host) {
            throw new IllegalStateException();
        }

        String url = "tcp://" + host;
        if (null != port && !port.isEmpty()) {
            url = url + ":" + port;
        }

        return url;
    }

    public String getMqttClientId() {
        return MqttClient.generateClientId();
    }

    @Transactional
    public SysLog getById(Long id)
    {
        return sysLogRepository.findById(id).orElse(null);

    }

    public SysLogList getSysLogs(GetSysLogsParameters params)
    {
        String p = params.getP();
        Long pId = null;
        Long pTime = null;
        if (null != p && !p.equals("")) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                SysLogList.Cursor cursor = mapper.readValue(Base64.getUrlDecoder().decode(p), SysLogList.Cursor.class);
                pId = cursor.getId();
                pTime = cursor.getTime();

            }catch(Exception e) {}
        }

        SysLogList sysLogList = new SysLogList();

        // 
        Double latLowerBound = null;
        Double latUpperBound = null;
        Double lonLowerBound = null;
        Double lonUpperBound = null;

        Predicate predicate 
            = sysLogRepository.getPredicate(
                params.getIds(), 
                params.getT(),
                params.getS(),
                params.getDevIds(),
                params.getModIds(),
                params.getPowerlb(),
                params.getPowerub(),
                latLowerBound, 
                latUpperBound, 
                lonLowerBound, 
                lonUpperBound, 
                params.getTlb(),
                params.getTub(),
                pId,
                pTime,
                params.getO(),
                params.getD());

        Pageable pageable = sysLogRepository.getPageable(
            0, 
            params.getN(), 
            params.getO(), 
            params.getD());

        List<SysLog> sysLogs 
            = asList(sysLogRepository.findAll(predicate, pageable));

        if (null != sysLogs) {
            int size = sysLogs.size();
            if (0 < size) {
                SysLog first = sysLogs.get(0);
                SysLog last = sysLogs.get(size-1);

                SysLogList.Cursor prevCursor = new SysLogList.Cursor();
                prevCursor.setId(first.getId());
                prevCursor.setTime(first.getTime());
                sysLogList.setPrev(prevCursor);

                SysLogList.Cursor nextCursor = new SysLogList.Cursor();
                nextCursor.setId(last.getId());
                sysLogList.setNext(nextCursor);
            }
        }

        sysLogList.setSysLogs(sysLogs);
        sysLogList.setIsLoading(false);

        return sysLogList;
    }

    public SysLog register(RegisterSysLogForm form, Errors errors)
    {
        Short type = form.getType();
        String targetDeviceId = form.getTargetDeviceId();
        String moduleId = form.getModuleId();
        Double power = form.getPower();
        Double batteryCapacity = form.getBatteryCapacity();
        Double longitude = form.getLongitude();
        Double latitude = form.getLatitude();
        Long time = form.getTime();

        Long currentTimeInMillis = clock.getTime();

        SysLog sysLog 
            = register(type, 
                        targetDeviceId, 
                        moduleId, 
                        power, 
                        batteryCapacity, 
                        longitude, 
                        latitude,
                        time,
                        currentTimeInMillis);

        return sysLog;
    }

    @Transactional
    public SysLog register(Short type, 
                            String targetDeviceId, 
                            String moduleId, 
                            Double power, 
                            Double batteryCapacity, 
                            Double longitude, 
                            Double latitude, 
                            Long time,
                            Long createdWhen)
    {
        SysLog sysLog = new SysLog();
        sysLog.setId(idGenerator.genIdLong());
        sysLog.setType(type);
        sysLog.setTargetDeviceId(targetDeviceId);
        sysLog.setModuleId(moduleId);
        sysLog.setPower(power);
        sysLog.setBatteryCapacity(batteryCapacity);
        sysLog.setLongitude(longitude);
        sysLog.setLatitude(latitude);
        sysLog.setTime(time);
        sysLog.setStatus(SysLog.Status.STABLE);
        sysLog.setStatusLastUpdatedWhen(createdWhen);
        sysLog.setCreatedWhen(createdWhen);

        sysLogRepository.persist(sysLog);

        return sysLog;
    }

    public SysLog update(UpdateSysLogForm form, Errors errors)
    {
        SysLog sysLog = null;

        Long id = form.getId();
        Boolean typeModified = form.getTypeModified();
        Short type = form.getType();
        Boolean statusModified = form.getStatusModified();
        Short status = form.getStatus();
        Boolean targetDeviceIdModified = form.getTargetDeviceIdModified();
        String targetDeviceId = form.getTargetDeviceId();
        Boolean powerModified = form.getPowerModified();
        Double power = form.getPower();
        Boolean batteryCapacityModified = form.getBatteryCapacityModified();
        Double batteryCapacity = form.getBatteryCapacity();
        Boolean longitudeModified = form.getLongitudeModified();
        Double longitude = form.getLongitude();
        Boolean latitudeModified = form.getLatitudeModified();
        Double latitude = form.getLatitude();
        Boolean timeModified = form.getTimeModified();
        Long time = form.getTime();

        Long currentTimeInMillis = clock.getTime();

        if (null != statusModified && statusModified) {
            sysLog = changeStatus(id, status, currentTimeInMillis, false);
        }

        sysLog = update(id, 
                        typeModified, 
                        type, 
                        targetDeviceIdModified, 
                        targetDeviceId, 
                        powerModified, 
                        power, 
                        batteryCapacityModified, 
                        batteryCapacity, 
                        longitudeModified, 
                        longitude,
                        latitudeModified,
                        latitude,
                        timeModified,
                        time);

        return sysLog;
    }

    @Transactional
    private SysLog changeStatus(Long id, Short status, Long statusLastUpdatedWhen, boolean ignoreLoop) throws NotFoundException
    {
        SysLog sysLog = getById(id);

        if (null == sysLog)
        {
            throw new NotFoundException();
        }

        sysLog.setStatus(status);
        sysLog.setStatusLastUpdatedWhen(statusLastUpdatedWhen);

        sysLog = sysLogRepository.save(sysLog);

        return sysLog;
    }


    @Transactional
    public SysLog update(Long id, 
                        Boolean typeModified, 
                        Short type, 
                        Boolean targetDeviceIdModified, 
                        String targetDeviceId, 
                        Boolean powerModified, 
                        Double power, 
                        Boolean batteryCapacityModified, 
                        Double batteryCapacity, 
                        Boolean longitudeModified, 
                        Double longitude, 
                        Boolean latitudeModified, 
                        Double latitude, 
                        Boolean timeModified, 
                        Long time) throws NotFoundException
    {
        SysLog sysLog = getById(id);

        if (null == sysLog) {
            throw new NotFoundException();
        }

        if (null != typeModified && typeModified) {
            sysLog.setType(type);
        }

        if (null != targetDeviceIdModified && targetDeviceIdModified) {
            sysLog.setTargetDeviceId(targetDeviceId);
        }

        if (null != powerModified && powerModified) {
            sysLog.setPower(power);
        }

        if (null != batteryCapacityModified && batteryCapacityModified) {
            sysLog.setBatteryCapacity(batteryCapacity);
        }

        if (null != longitudeModified && longitudeModified) {
            sysLog.setLongitude(longitude);
        }

        if (null != latitudeModified && latitudeModified) {
            sysLog.setLatitude(latitude);
        }

        if (null != timeModified && timeModified) {
            sysLog.setTime(time);
        }

        return sysLog;
    }

    public SysLog delete(DeleteSysLogsForm form, Errors errors)
    {
        SysLog sysLog = null;


        return sysLog;
    }

    public MqttClient getMqttClient() throws MqttException {

        if (null == mqttClient) {
            String brokerUrl = getMqttBrokerUrl();
            String clientId = getMqttClientId();
            mqttClient = new MqttClient(brokerUrl, clientId, null);
        }

        return mqttClient;
    }

    public String getMqttUserName() {
        return env.getProperty("ncmmary.mqtt.broker.username");
    }

    public String getMqttPassword() {
        return env.getProperty("ncmmary.mqtt.broker.password");
    }

    public class MqttMessageListener implements IMqttMessageListener 
    {
        @Data
        class Payload {
            @JsonAlias({"target_device_id"})
            private String targetDeviceId;
            @JsonAlias({"module_id"})
            private String moduleId;
            private Double power;
            private Double longitude;
            private Double latitude;
            private Long timestamp;
        }

        private CountDownLatch receivedMessageCount;

        public MqttMessageListener init(CountDownLatch receivedMessageCount) {
            this.receivedMessageCount = receivedMessageCount;

            return this;
        }

        public void messageArrived(String topic, MqttMessage message) {
            log.info("Message arrived! topic: " + topic + ", message: " + message);

            try {
                ObjectMapper mapper = new ObjectMapper();
                Payload payload = mapper.readValue(message.getPayload(), Payload.class);
                Short type = null;
                String targetDeviceId = payload.getTargetDeviceId();
                String moduleId = payload.getModuleId();
                Double power = payload.getPower();
                Double batteryCapacity = null;
                Double longitude = payload.getLongitude();
                Double latitude = payload.getLatitude();
                Long time = payload.getTimestamp();
                Long currentTimeInMillis = clock.getTime();
                SysLog sysLog 
                    = register(
                        type, 
                        targetDeviceId,
                        moduleId,
                        power,
                        batteryCapacity,
                        longitude,
                        latitude,
                        time,
                        currentTimeInMillis);
            } catch (Exception e) {
                e.printStackTrace();
            }

            receivedMessageCount.countDown();
        }
    }

    @Scheduled(fixedDelayString = "${ncmmary.syslogProcessing.in.millis}")
    public void processSysLogsFromBroker()
    {
        final int MAX_CONCURRENT_MESSAGE_PROCESSING = 1000;

        log.info("processSysLogsFromBroker() ...");

        try {
            MqttClient mqttClient = getMqttClient(); 

            if (mqttClient.isConnected()) {
                return;
            }
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            connOpts.setConnectionTimeout(5);
            connOpts.setAutomaticReconnect(true);
            String mqttUserName = getMqttUserName();
            if (null != mqttUserName && !mqttUserName.trim().isEmpty()) {
                connOpts.setUserName(mqttUserName);
            }
            String mqttPassword = getMqttPassword();
            if (null != mqttPassword && !mqttPassword.trim().isEmpty()) {
                connOpts.setPassword(mqttPassword.toCharArray());
            }
            log.info("Connecting to broker (client ID: " + mqttClient.getClientId() + ") : " + mqttClient.getServerURI());
            mqttClient.connect(connOpts);
            log.info("Connected broker: " + mqttClient.getCurrentServerURI());
            CountDownLatch receivedMessageCount = new CountDownLatch(MAX_CONCURRENT_MESSAGE_PROCESSING);
            mqttClient.subscribe("mary1/report", (new MqttMessageListener()).init(receivedMessageCount));

            try {
                receivedMessageCount.await(10, TimeUnit.SECONDS);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }

        } catch(MqttException me) {
            me.printStackTrace();

        }

    }

    private List<SysLog> asList(Page<SysLog> page)
    {
        List<SysLog> sysLogs = null;

        if(null != page)
        {
            sysLogs = page.getContent();
        }

        return sysLogs;
    }
}

