package com.ncmmary.security;

import lombok.Data;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Data
public class AuthTokenAuthenticationToken implements Authentication
{
    private Collection<? extends GrantedAuthority> authorities;
    private Object credentials;
    private Object details;
    private Object principal;
    private boolean isAuthenticated = false;
    private String name;

    public AuthTokenAuthenticationToken(Object principal, Object credentials)
    {
        setPrincipal(principal);
        setCredentials(credentials);
    }
}
