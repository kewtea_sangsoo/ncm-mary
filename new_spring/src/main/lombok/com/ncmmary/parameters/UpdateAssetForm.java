package com.ncmmary.parameters;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiModelProperty;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateAssetForm
{
    @ApiModelProperty(value = "${ncmmary.doc.updateAsset.request.id.desc}")
    private Long id;
    @ApiModelProperty(value = "${ncmmary.doc.updateAsset.request.typeModified.desc}")
    private Boolean typeModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateAsset.request.type.desc}")
    private Short type;
    @ApiModelProperty(value = "${ncmmary.doc.updateAsset.request.latitudeModified.desc}")
    private Boolean latitudeModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateAsset.request.latitude.desc}")
    private Double latitude;
    @ApiModelProperty(value = "${ncmmary.doc.updateAsset.request.longitudeModified.desc}")
    private Boolean longitudeModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateAsset.request.longitude.desc}")
    private Double longitude;
    @ApiModelProperty(value = "${ncmmary.doc.updateAsset.request.statusModified.desc}")
    private Boolean statusModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateAsset.request.status.desc}")
    private Short status;
    @ApiModelProperty(value = "${ncmmary.doc.updateAsset.request.hasBatteryModified.desc}")
    private Boolean hasBatteryModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateAsset.request.hasBattery.desc}")
    private Boolean hasBattery;
    @ApiModelProperty(value = "${ncmmary.doc.updateAsset.request.batteryModified.desc}")
    private Boolean batteryModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateAsset.request.battery.desc}")
    private Double battery;
}

