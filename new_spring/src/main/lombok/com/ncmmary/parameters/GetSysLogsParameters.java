package com.ncmmary.parameters;

import com.ncmmary.entity.SysLog;
import com.ncmmary.repository.SysLogRepository;

import io.swagger.annotations.ApiModelProperty;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Collection;
import java.util.Arrays;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class GetSysLogsParameters
{
    @ApiModelProperty(value = "${ncmmary.doc.getSysLogs.request.ids.desc}")
    private Collection<Long> ids;
    @ApiModelProperty(value = "${ncmmary.doc.getSysLogs.request.t.desc}")
    private Collection<Short> t; // types
    @ApiModelProperty(value = "${ncmmary.doc.getSysLogs.request.s.desc}")
    private Collection<Short> s; // statuses
    @ApiModelProperty(value = "${ncmmary.doc.getSysLogs.request.devIds.desc}")
    private Collection<String> devIds;
    @ApiModelProperty(value = "${ncmmary.doc.getSysLogs.request.modIds.desc}")
    private Collection<String> modIds;
    @ApiModelProperty(value = "${ncmmary.doc.getSysLogs.request.powerlb.desc}")
    private Double powerlb; //power lower bound
    @ApiModelProperty(value = "${ncmmary.doc.getSysLogs.request.powerub.desc}")
    private Double powerub; //power upper bound
    @ApiModelProperty(value = "${ncmmary.doc.getSysLogs.request.lon.desc}")
    private Double lon;
    @ApiModelProperty(value = "${ncmmary.doc.getSysLogs.request.lat.desc}")
    private Double lat;
    @ApiModelProperty(value = "${ncmmary.doc.getSysLogs.request.r.desc}")
    private Double r; // radius
    @ApiModelProperty(value = "${ncmmary.doc.getSysLogs.request.tlb.desc}")
    private Long tlb; // time lower bound
    @ApiModelProperty(value = "${ncmmary.doc.getSysLogs.request.tub.desc}")
    private Long tub; // time upper bound
    @ApiModelProperty(value = "${ncmmary.doc.request.p.desc}")
    private String p;
    @ApiModelProperty(value = "${ncmmary.doc.getSysLogs.request.o.desc}")
    private Short o; // order by
    @ApiModelProperty(value = "${ncmmary.doc.request.d.desc}")
    private Boolean d; // descending order ?
    @ApiModelProperty(value = "${ncmmary.doc.request.n.desc}")
    private Integer n;

    public Short getO(){
        if (null == o) {
            return SysLogRepository.OrderBy.DEFAULT;
        }
        return o;
    }

    public Integer getN(){
        if (null == n) {
            return SysLogRepository.DEFAULT_N;
        }
        return n;
    }

    public Boolean getD() {
        if (null == d) {
            if (SysLogRepository.OrderBy.ID == getO()) {
                return true;
            }
        }

        return d;
    }
            

}
