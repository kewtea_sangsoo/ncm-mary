package com.ncmmary.parameters;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoginUserForm
{
    @ApiModelProperty(value = "${ncmmary.doc.user.username.desc}")
    private String username;
    @ApiModelProperty(value = "${ncmmary.doc.user.password.desc}")
    private String password;
    @ApiModelProperty(value = "${ncmmary.doc.authToken.id.desc}")
    private String authToken;
}

