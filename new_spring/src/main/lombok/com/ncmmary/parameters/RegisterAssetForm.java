package com.ncmmary.parameters;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiModelProperty;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RegisterAssetForm
{
    @ApiModelProperty(value = "${ncmmary.doc.asset.type.desc}")
    private Short type;
    @ApiModelProperty(value = "${ncmmary.doc.asset.latitude.desc}")
    private Double latitude;
    @ApiModelProperty(value = "${ncmmary.doc.asset.longitude.desc}")
    private Double longitude;

}
