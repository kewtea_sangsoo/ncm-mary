package com.ncmmary.parameters;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RegisterUserForm
{
    @ApiModelProperty(value = "${ncmmary.doc.user.mobilePhone.desc}")
    private String mobilePhone;
    @ApiModelProperty(value = "${ncmmary.doc.userAppInstance.appInstanceId.desc}")
    private String appInstanceId;
    @ApiModelProperty(value = "${ncmmary.doc.userAppInstance.appVersion.desc}")
    private String appVersion;
    @ApiModelProperty(value = "${ncmmary.doc.userAppInstance.os.desc}")
    private String os;
    @ApiModelProperty(value = "${ncmmary.doc.userAppInstance.mtid.desc}")
    private String mtid;

    /*
    @ApiModelProperty(value = "${ncmmary.doc.user.email.desc}")
    private String email;
    @ApiModelProperty(value = "${ncmmary.doc.user.password.desc}")
    private String password;
    @ApiModelProperty(value = "${ncmmary.doc.user.passwordConfirm.desc}")
    private String passwordConfirm;
    */
}


