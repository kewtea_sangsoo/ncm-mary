package com.ncmmary.parameters;

import com.ncmmary.entity.AssetRental;
import com.ncmmary.repository.AssetRentalRepository;

import io.swagger.annotations.ApiModelProperty;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Collection;
import java.util.Arrays;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class GetAssetRentalsParameters
{
    @ApiModelProperty(value = "${ncmmary.doc.getAssetRentals.request.ids.desc}")
    private Collection<Long> ids;
}

