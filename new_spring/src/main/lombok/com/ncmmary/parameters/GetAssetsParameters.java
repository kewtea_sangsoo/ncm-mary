package com.ncmmary.parameters;

import com.ncmmary.entity.Asset;
import com.ncmmary.repository.AssetRepository;

import io.swagger.annotations.ApiModelProperty;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Collection;
import java.util.Arrays;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class GetAssetsParameters
{
    @ApiModelProperty(value = "${ncmmary.doc.getAssets.request.ids.desc}")
    private Collection<Long> ids;
    @ApiModelProperty(value = "${ncmmary.doc.getAssets.request.t.desc}")
    private Collection<Short> t; // type
    @ApiModelProperty(value = "${ncmmary.doc.getAssets.request.s.desc}")
    private Collection<Short> s;
    @ApiModelProperty(value = "${ncmmary.doc.getAssets.request.o.desc}")
    private Short o; // order by
    @ApiModelProperty(value = "${ncmmary.doc.getAssets.request.d.desc}")
    private Boolean d; // descending order ?
    @ApiModelProperty(value = "${ncmmary.doc.request.n.desc}")
    private Integer n;
    @ApiModelProperty(value = "${ncmmary.doc.getAssets.request.lat.desc}")
    private Double lat;
    @ApiModelProperty(value = "${ncmmary.doc.getAssets.request.lon.desc}")
    private Double lon;
    @ApiModelProperty(value = "${ncmmary.doc.getAssets.request.r.desc}")
    private Double r; // region radius (meter)
    @ApiModelProperty(value = "${ncmmary.doc.request.ctl.desc}")
    private Long ctl;
    @ApiModelProperty(value = "${ncmmary.doc.request.ctu.desc}")
    private Long ctu;
    @ApiModelProperty(value = "${ncmmary.doc.request.lutl.desc}")
    private Long lutl;
    @ApiModelProperty(value = "${ncmmary.doc.request.lutu.desc}")
    private Long lutu;
    @ApiModelProperty(value = "${ncmmary.doc.request.p.desc}")
    private String p;

    public Short getO(){
        if (null == o) {
            return AssetRepository.OrderBy.DEFAULT;
        }

        return o;
    }

    public Integer getN(){
        if (null == n) {
            return AssetRepository.DEFAULT_N;
        }
        return n;
    }

    public Boolean getD() {

        if (null == d) {

            d = true;
            Short o = getO();

            if (AssetRepository.OrderBy.ID == o) {
                d = true;
            } else if (AssetRepository.OrderBy.CREATED_TIME == o) {
                d = true;
            } else if (AssetRepository.OrderBy.LAST_UPDATED_TIME == o) {
                d = true;
            }
        }

        return d;
    }
}

