package com.ncmmary.parameters;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiModelProperty;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateSysLogForm
{
    @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.request.id.desc}")
    private Long id;
    @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.request.typeModified.desc}")
    private Boolean typeModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.request.type.desc}")
    private Short type;
    @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.request.statusModified.desc}")
    private Boolean statusModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.request.status.desc}")
    private Short status;
    @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.request.targetDeviceIdModified.desc}")
    private Boolean targetDeviceIdModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.request.targetDeviceId.desc}")
    private String targetDeviceId;
    @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.request.moduleIdModified.desc}")
    private Boolean moduleIdModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.request.moduleId.desc}")
    private String moduleId;
    @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.request.powerModified.desc}")
    private Boolean powerModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.request.power.desc}")
    private Double power;
    @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.request.batteryCapacityModified.desc}")
    private Boolean batteryCapacityModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.request.batteryCapacity.desc}")
    private Double batteryCapacity;
    @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.request.longitudeModified.desc}")
    private Boolean longitudeModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.request.longitude.desc}")
    private Double longitude;
    @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.request.latitudeModified.desc}")
    private Boolean latitudeModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.request.latitude.desc}")
    private Double latitude;
    @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.request.timeModified.desc}")
    private Boolean timeModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateSysLog.request.time.desc}")
    private Long time;
}
