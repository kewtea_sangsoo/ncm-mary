package com.ncmmary.parameters;

import com.ncmmary.entity.User;
import com.ncmmary.repository.UserRepository;

import lombok.Data;

import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Collection;
import java.util.Arrays;
import java.util.Set;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class GetUsersParameters
{
    @ApiModelProperty(value = "${ncmmary.doc.getUsers.request.ids.desc}")
    private Set<Long> ids;
    @ApiModelProperty(value = "${ncmmary.doc.request.p.desc}")
    private String p;
    @ApiModelProperty(value = "${ncmmary.doc.request.ctl.desc}")
    private Long ctl; 
    @ApiModelProperty(value = "${ncmmary.doc.request.ctu.desc}")
    private Long ctu;
    @ApiModelProperty(value = "${ncmmary.doc.request.lutl.desc}")
    private Long lutl;
    @ApiModelProperty(value = "${ncmmary.doc.request.lutu.desc}")
    private Long lutu;
    @ApiModelProperty(value = "${ncmmary.doc.getUsers.request.r.desc}")
    private Collection<Short> r;
    @ApiModelProperty(value = "${ncmmary.doc.getUsers.request.s.desc}")
    private Collection<Short> s;
    @ApiModelProperty(value = "${ncmmary.doc.getUsers.request.o.desc}")
    private Short o;
    @ApiModelProperty(value = "${ncmmary.doc.request.d.desc}")
    private Boolean d;
    @ApiModelProperty(value = "${ncmmary.doc.request.n.desc}")
    private Integer n;

    public Collection<Short> getS()
    {
        return null != s ? s : Arrays.asList(User.Status.ACTIVE);
    }

    public void setO(Short o)
    {
        this.o = o;
    }

    public Short getO()
    {
        if (null == o) {
            return UserRepository.OrderBy.DEFAULT;
        }
        return o;
    }

    public Boolean getD()
    {
        if (null == d) {
            d = true;
            Short o = getO();

            if (UserRepository.OrderBy.ID == getO()) {
                d = true;
            } else if (UserRepository.OrderBy.CREATED_TIME == o) {
                d = true;
            } else if (UserRepository.OrderBy.LAST_UPDATED_TIME == o) {
                d = true;
            }
        }

        return d;
    }

    public Integer getN()
    {
        if (null == n) {
            return UserRepository.DEFAULT_N;
        }
        return n;
    }
}
