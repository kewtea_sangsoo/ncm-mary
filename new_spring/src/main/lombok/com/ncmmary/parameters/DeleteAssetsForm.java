package com.ncmmary.parameters;

import io.swagger.annotations.ApiModelProperty;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

@Data
public class DeleteAssetsForm
{
    
    @ApiModelProperty(value = "${ncmmary.doc.deleteAssets.request.ids.desc}")
    private Collection<Long> ids;

}
