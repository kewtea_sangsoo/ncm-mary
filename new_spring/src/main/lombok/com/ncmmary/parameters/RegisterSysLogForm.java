package com.ncmmary.parameters;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiModelProperty;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class RegisterSysLogForm
{
    @ApiModelProperty(value = "${ncmmary.doc.syslog.type.desc}")
    private Short type;
    @ApiModelProperty(value = "${ncmmary.doc.syslog.targetDeviceId.desc}")
    private String targetDeviceId;
    @ApiModelProperty(value = "${ncmmary.doc.syslog.moduleId.desc}")
    private String moduleId;
    @ApiModelProperty(value = "${ncmmary.doc.syslog.power.desc}")
    private Double power;
    @ApiModelProperty(value = "${ncmmary.doc.syslog.batteryCapacity.desc}")
    private Double batteryCapacity;
    @ApiModelProperty(value = "${ncmmary.doc.syslog.longitude.desc}")
    private Double longitude;
    @ApiModelProperty(value = "${ncmmary.doc.syslog.latitude.desc}")
    private Double latitude;
    @ApiModelProperty(value = "${ncmmary.doc.syslog.time.desc}")
    private Long time;
}
