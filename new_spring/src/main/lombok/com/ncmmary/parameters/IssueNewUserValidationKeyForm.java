package com.ncmmary.parameters;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

@Data
public class IssueNewUserValidationKeyForm
{
    private Long userId;
    private String mobilePhone;
}
