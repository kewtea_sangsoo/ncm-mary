package com.ncmmary.parameters;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiModelProperty;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateUserForm
{
    @ApiModelProperty(value = "${ncmmary.doc.updateUser.request.id.desc}")
    private Long id;
    @ApiModelProperty(value = "${ncmmary.doc.updateUser.request.mobilePhoneModified.desc}")
    private Boolean mobilePhoneModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateUser.request.mobilePhone.desc}")
    private String mobilePhone;
    @ApiModelProperty(value = "${ncmmary.doc.updateUser.request.isMobilePhoneVerifiedModified.desc}")
    private Boolean isMobilePhoneVerifiedModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateUser.request.isMobilePhoneVerified.desc}")
    private Boolean isMobilePhoneVerified;
    @ApiModelProperty(value = "${ncmmary.doc.updateUser.request.statusModified.desc}")
    private Boolean statusModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateUser.request.status.desc}")
    private Short status;
    @ApiModelProperty(value = "${ncmmary.doc.updateUser.request.roleModified.desc}")
    private Boolean roleModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateUser.request.role.desc}")
    private Short role;

    /*
    @ApiModelProperty(value = "${ncmmary.doc.updateUser.request.passwordModified.desc}")
    private Boolean passwordModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateUser.request.newPassword.desc}")
    private String newPassword;
    @ApiModelProperty(value = "${ncmmary.doc.updateUser.request.password.desc}")
    private String password;
    @ApiModelProperty(value = "${ncmmary.doc.updateUser.request.emailModified.desc}")
    private Boolean emailModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateUser.request.email.desc}")
    private String email;
    @ApiModelProperty(value = "${ncmmary.doc.updateUser.request.typeModified.desc}")
    private Boolean typeModified;
    @ApiModelProperty(value = "${ncmmary.doc.updateUser.request.type.desc}")
    private Short type;
    */

}
