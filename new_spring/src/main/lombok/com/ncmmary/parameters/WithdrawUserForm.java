package com.ncmmary.parameters;

import io.swagger.annotations.ApiModelProperty;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

@Data
public class WithdrawUserForm
{
    @ApiModelProperty(value = "${ncmmary.doc.withdrawUser.request.id.desc}")
    private Long id;
    @ApiModelProperty(value = "${ncmmary.doc.withdrawUser.request.password.desc}")
    private String password;
}
