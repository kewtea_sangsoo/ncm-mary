package com.ncmmary.parameters;

import io.swagger.annotations.ApiModelProperty;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class DeleteProductsForm
{
    @ApiModelProperty(value = "${ncmmary.doc.deleteProducts.request.ids.desc}")
    private Collection<Long> ids;
}
