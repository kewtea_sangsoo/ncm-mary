package com.ncmmary.security;

import com.ncmmary.core.Clock;
import com.ncmmary.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Service
public class MyUserDetailsService implements UserDetailsService
{
    @Autowired
    private Clock clock;

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException{
        com.ncmmary.entity.User user = userRepository.findByEmail(username).orElse(null);

        if(null == user){
            throw new UsernameNotFoundException("Could not find user " + username);
        }

        MyUserDetails myUserDetails = new MyUserDetails(user, clock.getTime());
        
        return myUserDetails; 

    }


}
