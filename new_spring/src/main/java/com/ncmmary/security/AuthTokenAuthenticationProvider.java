package com.ncmmary.security;

import com.ncmmary.exception.UserAuthenticationException;
import com.ncmmary.core.Clock;
import com.ncmmary.entity.User;
import com.ncmmary.entitymanager.UserManager;

import com.ncmmary.security.AuthTokenAuthenticationToken;
import com.ncmmary.security.MyUserDetails;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Lazy;


import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.authentication.AuthenticationProvider;


@Component
public class AuthTokenAuthenticationProvider implements AuthenticationProvider {

    private static final Logger log = LoggerFactory.getLogger(AuthTokenAuthenticationProvider.class);

    @Autowired
    private Clock clock;

    @Autowired
    @Lazy
    private UserManager userManager;

    @Override
    public Authentication authenticate(Authentication auth) throws AuthenticationException {

        if (auth instanceof AuthTokenAuthenticationToken) {

            AuthTokenAuthenticationToken authTokenAuthenticationToken
                = (AuthTokenAuthenticationToken)auth;
            Long userId = (Long)authTokenAuthenticationToken.getPrincipal();
            User user = userManager.getById(userId);
            if (null == user) {
                throw new UserAuthenticationException("Could not find a user by ID: " + userId);
            }
            MyUserDetails myUserDetails = new MyUserDetails(user, clock.getTime());
            authTokenAuthenticationToken.setPrincipal(myUserDetails);
            authTokenAuthenticationToken.setAuthenticated(true);

        } else {
            throw new UserAuthenticationException("Unsupported authentication");
        }

        return auth;
    }

    @Override
    public boolean supports(Class<?> auth) {

        return auth.equals(AuthTokenAuthenticationToken.class);
    }
}
