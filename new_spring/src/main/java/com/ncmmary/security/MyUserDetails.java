package com.ncmmary.security;

import com.ncmmary.entity.User;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import java.util.List;

public class MyUserDetails extends org.springframework.security.core.userdetails.User 
{
    private Long userId;
    private Long authTime;
    private Long recentAccessTime;
    private Long lastSyncTime;

    public MyUserDetails(User user, Long authTime)
    {
        super(user.getEmail(), user.getPassword(), true/*is user enabled*/, true/*is accountNonExpired*/, true/*credentialsNonExpired*/, true/*accountNonLocked*/, AuthorityUtils.commaSeparatedStringToAuthorityList(user.getRole() == User.Role.ADMIN ? "ROLE_USER,ROLE_ADMIN" : "ROLE_USER"));
        this.setUserId(user.getId());
        this.setAuthTime(authTime);
        this.setRecentAccessTime(authTime);
        this.setLastSyncTime(authTime);
    }

    public synchronized void setUserId(Long userId)
    {
        this.userId= userId;
    }

    public synchronized Long getUserId()
    {
        return userId;
    }

    public synchronized void setAuthTime(Long authTime)
    {
        this.authTime = authTime;
    }

    public synchronized Long getAuthTime()
    {
        return authTime;
    }

    public synchronized void setRecentAccessTime(Long recentAccessTime)
    {
        this.recentAccessTime = recentAccessTime;
    }

    public synchronized Long getRecentAccessTime()
    {
        return recentAccessTime;
    }

    public synchronized void setLastSyncTime(Long lastSyncTime)
    {
        this.lastSyncTime = lastSyncTime;
    }

    public synchronized Long getLastSyncTime()
    {
        return lastSyncTime;
    }
}
