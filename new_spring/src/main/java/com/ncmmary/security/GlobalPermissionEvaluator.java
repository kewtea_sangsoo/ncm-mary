package com.ncmmary.security;

import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;

import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;


@Component
public class GlobalPermissionEvaluator implements PermissionEvaluator {

    private static final Logger log = LoggerFactory.getLogger(GlobalPermissionEvaluator.class);

    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission)
    {
        log.info("!!! in hasPermission(Authentication authentication, Object targetDomainObject, Object permission)");
        log.info("!!! authentication: " + authentication);
        log.info("!!! targetDomainObject: " + targetDomainObject);
        log.info("!!! permission: " + permission);
        /*
         * Example Code: 
         *
        hasPermission = false;

        if (null != authentication ) {
            if (targetDomainObject instanceof com.ncmmary.entity.Asset) {

                // Check the authentication's permission against asset

            } else if (targetDomainObject instanceof com.ncmmary.entity.Product) {

                // Check the authentication's permission against product

            } else if ( ...


        return hasPermission;

        */

        return true; // Temporarily allow all
    }

    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission)
    {
        log.info("!!! in hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission)");
        log.info("!!! authentication: " + authentication);
        log.info("!!! targetId: " + targetId);
        log.info("!!! permission: " + permission);

        return false;
    }
}

