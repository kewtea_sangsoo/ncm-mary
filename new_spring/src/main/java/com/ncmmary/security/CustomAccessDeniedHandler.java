package com.ncmmary.security;

import com.ncmmary.exception.PermissionDeniedException;
import com.ncmmary.controller.RestApiResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.http.MediaType;

import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.csrf.CsrfException;
import org.springframework.security.web.csrf.MissingCsrfTokenException;
import org.springframework.security.web.csrf.InvalidCsrfTokenException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;


public class CustomAccessDeniedHandler implements AccessDeniedHandler
{
    private static final Logger log = LoggerFactory.getLogger(CustomAccessDeniedHandler.class);

    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) {

        PermissionDeniedException permissionDeniedException 
            = new PermissionDeniedException(accessDeniedException);

        if (request.getHeader("Accept").contains(MediaType.APPLICATION_JSON_UTF8_VALUE)) {
            ObjectMapper mapper = new ObjectMapper();
            RestApiResponseBody.Error error = new RestApiResponseBody.Error();
            error.setType(permissionDeniedException.getClass().getSimpleName());
            error.setMessage(permissionDeniedException.getMessage());
            try {
                OutputStreamWriter osw = new OutputStreamWriter(response.getOutputStream(), "UTF-8");
                BufferedWriter bw = new BufferedWriter(osw);
                mapper.writeValue(bw, error);
            } catch(Exception e) {
                e.printStackTrace();
            }

        } else {

            // Refer the AccessDeniedHandlerImpl.java the default
            
            //response.sendError(HttpStatus.FORBIDDEN.value(), ... 


        }
    }

}
