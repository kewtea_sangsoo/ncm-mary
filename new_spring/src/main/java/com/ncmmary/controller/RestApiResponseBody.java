package com.ncmmary.controller;

import org.springframework.context.MessageSource;

import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.FieldError;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

import java.util.Locale;
import java.util.List;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class RestApiResponseBody
{
    private MessageSource messageSource;
    private Locale locale;

    @ApiModelProperty(value = "${ncmmary.doc.response.error}")
    private List<Error> errors;

    public RestApiResponseBody(){}

    public RestApiResponseBody(MessageSource messageSource, Locale locale)
    {
        this.messageSource = messageSource;
        this.locale = locale;
    }

    public RestApiResponseBody(List<Error> errors, MessageSource messageSource, Locale locale)
    {
        this.errors = errors;
        this.messageSource = messageSource;
        this.locale = locale;
    }

    public RestApiResponseBody(Errors errors, MessageSource messageSource, Locale locale)
    {
        this.messageSource = messageSource;
        this.locale = locale;

        setErrors(errors);
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(Include.NON_NULL)
    public static class Error
    {
        @ApiModelProperty(value = "${ncmmary.doc.response.error.message}")
        private String message;
        @ApiModelProperty(value = "${ncmmary.doc.response.error.type}")
        private String type; // Exception class
        @ApiModelProperty(value = "${ncmmary.doc.response.error.title}")
        private String title;// error msg window title 
        //private code; ?
        @ApiModelProperty(value = "${ncmmary.doc.response.error.objectName.desc}")
        private String objectName;
        @ApiModelProperty(value = "${ncmmary.doc.response.error.field}")
        private String field;

        // ...

        public Error(){}

        public void setMessage(String message)
        {
            this.message = message;
        }

        public String getMessage()
        {
            return message;
        }

        public void setType(String type)
        {
            this.type = type;
        }

        public String getType()
        {
            return type;
        }

        public void setTitle(String title)
        {
            this.title = title;
        }

        public String getTitle()
        {
            return title;
        }

        public void setObjectName(String objectName)
        {
            this.objectName = objectName;
        }

        public String getObjectName()
        {
            return objectName;
        }

        public void setField(String field)
        {
            this.field = field;
        }

        public String getField()
        {
            return field;
        }
    }

    @JsonProperty
    public void setErrors(List<Error> errors)
    {
        this.errors = errors;
    }

    public void setErrors(Errors errors)
    {
        if(null != errors)
        {
            List<ObjectError> globalErrors  = errors.getGlobalErrors();
            if(null != globalErrors)
            {

                for(ObjectError globalError : globalErrors)
                {
                    for(String code : globalError.getCodes())
                    {
                        try{
                            Error error = new Error();
                            error.setMessage(messageSource.getMessage(code, null, locale));
                            error.setObjectName(globalError.getObjectName());
                            addError(error);
                            break;
                        }catch(Exception e){}

                    }
                }

            }

            List<FieldError> fieldErrors = errors.getFieldErrors();
            if(null != fieldErrors)
            {
                for(FieldError fieldError : fieldErrors)
                {
                    for(String code : fieldError.getCodes())
                    {
                        try{
                            Error error = new Error();
                            error.setMessage(messageSource.getMessage(code, null, locale));
                            error.setField(fieldError.getField());
                            addError(error);
                            break;
                        }catch(Exception e){}

                    }
                }
            }
        }

    }

    public void addError(Error error)
    {
        if(null == errors)
        {
            errors = new ArrayList<Error>();
        }

        errors.add(error);
    }

    public List<Error> getErrors()
    {
        return errors;
    }

}
