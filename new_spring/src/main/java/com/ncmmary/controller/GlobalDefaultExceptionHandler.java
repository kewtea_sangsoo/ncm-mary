package com.ncmmary.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.context.MessageSource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import java.util.Locale;
import java.util.List;
import java.util.ArrayList;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@ControllerAdvice
public class GlobalDefaultExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(GlobalDefaultExceptionHandler.class);

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler
    @RequestMapping(produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public RestApiResponseBody handleException(Exception ex, Locale locale)
    {
        //ex.printStackTrace();

        RestApiResponseBody.Error error = new RestApiResponseBody.Error();
        error.setType(ex.getClass().getSimpleName());
        error.setMessage(ex.getMessage());

        RestApiResponseBody responseBody
            = new RestApiResponseBody(messageSource, locale);
        responseBody.addError(error);

        return responseBody;

    }

    /*

    // If you want to customize respose to client who accepts other media type ...
    
    @ExceptionHandler
    @RequestMapping(produces="!"+MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public ResponseEntity<String> handleException(HttpServletRequest request, HttpServletResponse response, Exception ex, Locale locale)
    {

        // ...
        
    }

    */
}
