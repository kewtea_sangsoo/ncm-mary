package com.ncmmary.exception;

public class PermissionDeniedException extends RuntimeException
{
    public PermissionDeniedException()
    {
        super();
    }

    public PermissionDeniedException(Throwable cause)
    {
        super(cause);
    }

    public PermissionDeniedException(String message)
    {
        super(message);
    }
}
