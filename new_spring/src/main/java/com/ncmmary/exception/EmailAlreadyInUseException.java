package com.ncmmary.exception;

public class EmailAlreadyInUseException extends RuntimeException
{
    public EmailAlreadyInUseException()
    {
        super();
    }
}
