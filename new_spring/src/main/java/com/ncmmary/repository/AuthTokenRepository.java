package com.ncmmary.repository;

import com.ncmmary.entity.AuthToken;
import com.ncmmary.repository.custom.AuthTokenRepositoryCustom;
import com.ncmmary.repository.impl.AuthTokenRepositoryImpl;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Collection;

public interface AuthTokenRepository extends AuthTokenRepositoryCustom, MyRepository<AuthToken, String>
{
    public static final int DEFAULT_N = 20;

    public static class OrderBy
    {
        public static final short ID = 0;
        public static final short DEFAULT = ID;
    }
}


