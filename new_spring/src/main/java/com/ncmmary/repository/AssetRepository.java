package com.ncmmary.repository;

import com.ncmmary.entity.Asset;
import com.ncmmary.repository.custom.AssetRepositoryCustom;
import com.ncmmary.repository.impl.AssetRepositoryImpl;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Collection;

public interface AssetRepository extends AssetRepositoryCustom, MyRepository<Asset, Long>
{
    public static final int DEFAULT_N = 20;

    public static class OrderBy
    {
        public static final short ID = 0;
        public static final short DEFAULT = ID;
        public static final short CREATED_TIME = 1;
        public static final short LAST_UPDATED_TIME = 2;
    }
}

