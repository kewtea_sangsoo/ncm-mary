package com.ncmmary.repository;

import com.ncmmary.entity.AssetRental;
import com.ncmmary.repository.custom.AssetRentalRepositoryCustom;
import com.ncmmary.repository.impl.AssetRentalRepositoryImpl;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Collection;

public interface AssetRentalRepository extends AssetRentalRepositoryCustom, MyRepository<AssetRental, Long>
{
    public static final int DEFAULT_N = 20;

}
