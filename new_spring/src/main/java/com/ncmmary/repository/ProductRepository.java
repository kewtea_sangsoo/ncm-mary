package com.ncmmary.repository;

import com.ncmmary.entity.Product;
import com.ncmmary.repository.custom.ProductRepositoryCustom;
import com.ncmmary.repository.impl.ProductRepositoryImpl;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Collection;

public interface ProductRepository extends ProductRepositoryCustom, MyRepository<Product, String>
{

}
