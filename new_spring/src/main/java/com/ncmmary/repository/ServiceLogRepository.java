package com.ncmmary.repository;

import com.ncmmary.entity.ServiceLog;
import com.ncmmary.repository.custom.ServiceLogRepositoryCustom;
import com.ncmmary.repository.impl.ServiceLogRepositoryImpl;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Collection;

public interface ServiceLogRepository extends ServiceLogRepositoryCustom, MyRepository<ServiceLog, String>
{

}
