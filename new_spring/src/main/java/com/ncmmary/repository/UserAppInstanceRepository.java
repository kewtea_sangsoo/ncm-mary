package com.ncmmary.repository;

import com.ncmmary.entity.UserAppInstance;
import com.ncmmary.repository.custom.UserAppInstanceRepositoryCustom;
import com.ncmmary.repository.impl.UserAppInstanceRepositoryImpl;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Collection;

public interface UserAppInstanceRepository extends UserAppInstanceRepositoryCustom, MyRepository<UserAppInstance, Long>
{
    public static class OrderBy
    {
        public static final short ID = 0;
        public static final short REGISTERED_TIME = 1;
    }


}
