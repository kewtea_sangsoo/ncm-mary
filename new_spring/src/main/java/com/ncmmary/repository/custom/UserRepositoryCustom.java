package com.ncmmary.repository.custom;

import com.ncmmary.entity.User;

import java.util.List;
import java.util.Collection;

import org.springframework.data.repository.NoRepositoryBean;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import com.querydsl.core.types.Predicate;


@NoRepositoryBean
public interface UserRepositoryCustom{
    public Pageable getPageable(int page, int size, Short orderBy, boolean desc);

    public Predicate getPredicate(Collection<Long> ids,
                                    Collection<Short> roles,
                                    Collection<Short> statuses,
                                    Long createdTimeLowerBound,
                                    Long createdTimeUpperBound,
                                    Long lastUpdatedTimeLowerBound,
                                    Long lastUpdatedTimeUpperBound,
                                    Long pId, 
                                    Long pCreatedTime,
                                    Long pLastUpdatedTime,
                                    Short orderBy, 
                                    boolean desc);
}
