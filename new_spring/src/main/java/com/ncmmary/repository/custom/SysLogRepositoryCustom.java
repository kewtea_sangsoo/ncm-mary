package com.ncmmary.repository.custom;

import com.ncmmary.entity.SysLog;

import java.util.List;
import java.util.Collection;

import org.springframework.data.repository.NoRepositoryBean;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import com.querydsl.core.types.Predicate;


@NoRepositoryBean
public interface SysLogRepositoryCustom{
    public Pageable getPageable(int page, int size, Short orderBy, boolean desc);

    public Predicate getPredicate(Collection<Long> ids, 
                                    Collection<Short> types,
                                    Collection<Short> statuses,
                                    Collection<String> targetDeviceIds,
                                    Collection<String> moduleIds,
                                    Double powerLowerBound,
                                    Double powerUpperBound,
                                    Double latLowerBound, 
                                    Double latUpperBound, 
                                    Double lonLowerBound, 
                                    Double lonUpperBound, 
                                    Long timeLowerBound,
                                    Long timeUpperBound,
                                    Long pId,
                                    Long pTime,
                                    Short orderBy,
                                    boolean desc);

}
