package com.ncmmary.repository;

import com.ncmmary.entity.Order;
import com.ncmmary.repository.custom.OrderRepositoryCustom;
import com.ncmmary.repository.impl.OrderRepositoryImpl;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Collection;

public interface OrderRepository extends OrderRepositoryCustom, MyRepository<Order, String>
{

}
