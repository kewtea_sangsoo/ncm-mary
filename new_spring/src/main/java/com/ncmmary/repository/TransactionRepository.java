package com.ncmmary.repository;

import com.ncmmary.entity.Transaction;
import com.ncmmary.repository.custom.TransactionRepositoryCustom;
import com.ncmmary.repository.impl.TransactionRepositoryImpl;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Collection;

public interface TransactionRepository extends TransactionRepositoryCustom, MyRepository<Transaction, String>
{

}
