package com.ncmmary.repository;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

import com.querydsl.core.types.Predicate;

import java.util.Optional;
import java.io.Serializable;


@NoRepositoryBean
public interface MyRepository<T, ID extends Serializable> extends CrudRepository<T, ID>, QuerydslPredicateExecutor<T> 
{
    public <S extends T> S save(S entity, boolean saveOrUpdate);
    public <S extends T> S persist(S entity);
    public <S extends T> S merge(S entity);
    public <S extends T> void detach(S entity);
    public Iterable<T> findAll(Predicate predicate);
    public Page<T> findAll(Predicate predicate, Pageable pageable);
    public Optional<T> findById(ID id);
}
