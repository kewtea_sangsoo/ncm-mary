package com.ncmmary.repository;

import com.ncmmary.entity.UserAppInstanceAssociation;
import com.ncmmary.entity.UserAppInstanceAssociationId;
import com.ncmmary.repository.custom.UserAppInstanceAssociationRepositoryCustom;
import com.ncmmary.repository.impl.UserAppInstanceAssociationRepositoryImpl;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Collection;

public interface UserAppInstanceAssociationRepository extends UserAppInstanceAssociationRepositoryCustom, MyRepository<UserAppInstanceAssociation, UserAppInstanceAssociationId>
{
    public static class OrderBy
    {
        public static final short ID = 0;
        public static final short REGISTERED_TIME = 1;
    }


}
