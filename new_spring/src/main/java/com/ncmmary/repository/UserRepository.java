package com.ncmmary.repository;

import com.ncmmary.entity.User;
import com.ncmmary.repository.custom.UserRepositoryCustom;
import com.ncmmary.repository.impl.UserRepositoryImpl;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Collection;
import java.util.Optional;


public interface UserRepository extends UserRepositoryCustom, MyRepository<User, Long>
{
    public static final int DEFAULT_N = 20;

    public static class OrderBy
    {
        public static final short ID = 0;
        public static final short DEFAULT = ID;
        public static final short CREATED_TIME = 1;
        public static final short LAST_UPDATED_TIME = 2;
    }

    public List<User> findByIdInAndStatusIn(Collection<Long> userIds, Collection<Short> statuses);
    public Optional<User> findByMobilePhone(String mobilePhone);
    public Optional<User> findByEmail(String email);
    public Optional<User> findByUniqueName(String uniqueName);

}
