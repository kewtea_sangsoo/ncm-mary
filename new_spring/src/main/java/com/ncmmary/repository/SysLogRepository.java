package com.ncmmary.repository;

import com.ncmmary.entity.SysLog;
import com.ncmmary.repository.custom.SysLogRepositoryCustom;
import com.ncmmary.repository.impl.SysLogRepositoryImpl;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Collection;

public interface SysLogRepository extends SysLogRepositoryCustom, MyRepository<SysLog, Long>
{
    public static final int DEFAULT_N = 20;

    public static class OrderBy
    {
        public static final short ID = 0;
        public static final short DEFAULT = ID;
        //public static final short CREATED_TIME = 1;
        //public static final short LAST_UPDATED_TIME = 2;
        public static final short LOG_TIME = 3;
    }

}
