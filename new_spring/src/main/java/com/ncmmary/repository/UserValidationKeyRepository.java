package com.ncmmary.repository;

import com.ncmmary.entity.UserValidationKey;
import com.ncmmary.repository.custom.UserValidationKeyRepositoryCustom;
import com.ncmmary.repository.impl.UserValidationKeyRepositoryImpl;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Collection;

public interface UserValidationKeyRepository extends UserValidationKeyRepositoryCustom, MyRepository<UserValidationKey, Long>
{

}
