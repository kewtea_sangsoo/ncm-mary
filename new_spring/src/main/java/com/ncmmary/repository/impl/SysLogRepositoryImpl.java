package com.ncmmary.repository.impl;

import com.ncmmary.entity.QSysLog;
import com.ncmmary.entity.SysLog;
import com.ncmmary.repository.SysLogRepository;
import com.ncmmary.repository.custom.SysLogRepositoryCustom;

import org.springframework.data.jpa.repository.support.QuerydslJpaRepository;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.Set;
import java.util.Iterator;


public class SysLogRepositoryImpl implements SysLogRepositoryCustom 
{
    private static final Logger log 
        = LoggerFactory.getLogger(SysLogRepositoryImpl.class);

    public Pageable getPageable(int page, int size, Short orderBy, boolean desc)
    {
        Pageable pageable = null;

        if (null != orderBy)
        {
            List<String> properties = null;

            if (SysLogRepository.OrderBy.ID == orderBy)
            {
                properties = Arrays.asList("id"); 
            } else if (SysLogRepository.OrderBy.LOG_TIME == orderBy) {
                properties = Arrays.asList("time", "id");
            } else {
                throw new IllegalArgumentException("The 'orderBy' value is illegal!");
                    
            }

            pageable = PageRequest.of(page, size, desc ? Sort.Direction.DESC : Sort.Direction.ASC, properties.toArray(new String[0]));
        }

        if (null == pageable) {
            pageable = new PageRequest(page, size);
        }

        return pageable;
    }

    public Predicate getPredicate(Collection<Long> ids, 
                                    Collection<Short> types,
                                    Collection<Short> statuses,
                                    Collection<String> targetDeviceIds,
                                    Collection<String> moduleIds,
                                    Double powerLowerBound,
                                    Double powerUpperBound,
                                    Double latLowerBound, 
                                    Double latUpperBound, 
                                    Double lonLowerBound, 
                                    Double lonUpperBound, 
                                    Long timeLowerBound,
                                    Long timeUpperBound,
                                    Long pId,
                                    Long pTime,
                                    Short orderBy,
                                    boolean desc)
    {
        Predicate predicate = null;

        QSysLog sysLog = QSysLog.sysLog;

        if (null != ids && 0 < ids.size())
        {
            predicate = sysLog.id.in(ids).and(predicate);
        }

        if (null != types && 0 < types.size())
        {
            predicate = sysLog.type.in(types).and(predicate);
        }

        if (null != statuses && 0 < statuses.size())
        {
            predicate = sysLog.status.in(statuses).and(predicate);
        }

        if (null != targetDeviceIds && 0 < targetDeviceIds.size())
        {
            predicate = sysLog.targetDeviceId.in(targetDeviceIds).and(predicate);
        }

        if (null != moduleIds && 0 < moduleIds.size())
        {
            predicate = sysLog.moduleId.in(moduleIds).and(predicate);
        }

        if (null != powerLowerBound)
        {
            predicate = sysLog.power.goe(powerLowerBound).and(predicate);
        }

        if (null != powerUpperBound)
        {
            predicate = sysLog.power.lt(powerUpperBound).and(predicate);
        }

        if (null != latLowerBound)
        {
            predicate = sysLog.latitude.goe(latLowerBound).and(predicate);
        }

        if (null != latUpperBound)
        {
            predicate = sysLog.latitude.lt(latUpperBound).and(predicate);
        }

        if (null != lonLowerBound)
        {
            predicate = sysLog.longitude.goe(lonLowerBound).and(predicate);
        }

        if (null != lonUpperBound)
        {
            predicate = sysLog.longitude.lt(lonUpperBound).and(predicate);
        }

        if (null != timeLowerBound)
        {
            predicate = sysLog.time.goe(timeLowerBound).and(predicate);
        }

        if (null != timeUpperBound)
        {
            predicate = sysLog.time.lt(timeUpperBound).and(predicate);
        }

        if (null != orderBy) {

            if (SysLogRepository.OrderBy.ID == orderBy) {

                if (desc)
                {
                    if (null != pId)
                    {
                        predicate = sysLog.id.lt(pId).and(predicate);
                    }
                }
                else
                {
                    if (null != pId)
                    {
                        predicate = sysLog.id.gt(pId).and(predicate);
                    }
                }
            } else if (SysLogRepository.OrderBy.LOG_TIME == orderBy) {
                if (desc)
                {
                    if (null != pTime)
                    {
                        
                        BooleanExpression expr = sysLog.time.lt(pTime);
                        if (null != pId)
                        {
                            expr = expr.or(sysLog.time.eq(pTime).and(sysLog.id.lt(pId)));
                        }

                        predicate = expr.and(predicate);
                    }
                }
                else
                {
                    if (null != pTime)
                    {
                        BooleanExpression expr = sysLog.time.gt(pTime);
                        if (null != pId)
                        {
                            expr = expr.or(sysLog.time.eq(pTime).and(sysLog.id.gt(pId)));
                        }

                        predicate = expr.and(predicate);
                    }
                }
            }
        }

        return predicate;
    }

}


