package com.ncmmary.repository.impl;

import com.ncmmary.entity.QUserValidationKey;
import com.ncmmary.entity.UserValidationKey;
import com.ncmmary.repository.UserValidationKeyRepository;
import com.ncmmary.repository.custom.UserValidationKeyRepositoryCustom;

import org.springframework.data.jpa.repository.support.QuerydslJpaRepository;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.Set;
import java.util.Iterator;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import com.querydsl.core.types.Predicate;

public class UserValidationKeyRepositoryImpl implements UserValidationKeyRepositoryCustom 
{
     public Pageable getPageable(int page, int size, Short orderBy, boolean desc)
    {
        Pageable pageable = null;

        pageable = new PageRequest(page, size);

        return pageable;
    }

    public Predicate getPredicate(Collection<Long> ids)
    {

        Predicate predicate = null;

        QUserValidationKey userValidationKey = QUserValidationKey.userValidationKey;

        if (null != ids && 0 < ids.size())
        {
            //predicate = userValidationKey.id.in(ids).and(predicate);
        }

        return predicate;
    }

   
}
