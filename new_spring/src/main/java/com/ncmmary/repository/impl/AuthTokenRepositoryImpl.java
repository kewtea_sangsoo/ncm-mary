package com.ncmmary.repository.impl;

import com.ncmmary.entity.QAuthToken;
import com.ncmmary.entity.AuthToken;
import com.ncmmary.repository.AuthTokenRepository;
import com.ncmmary.repository.custom.AuthTokenRepositoryCustom;

import org.springframework.data.jpa.repository.support.QuerydslJpaRepository;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.Set;
import java.util.Iterator;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import com.querydsl.core.types.Predicate;

public class AuthTokenRepositoryImpl implements AuthTokenRepositoryCustom 
{
    public Pageable getPageable(int page, int size, Short orderBy, boolean desc)
    {

        Pageable pageable = null;

        pageable = new PageRequest(page, size);

        return pageable;
    }

    public Predicate getPredicate()
    {
        Predicate predicate = null;

        QAuthToken authToken = QAuthToken.authToken;


        return predicate;
    }

}


