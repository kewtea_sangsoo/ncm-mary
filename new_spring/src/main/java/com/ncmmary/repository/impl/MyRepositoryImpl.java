package com.ncmmary.repository.impl;

import com.ncmmary.repository.MyRepository;

//import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.jpa.repository.support.QuerydslJpaRepository;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import java.io.Serializable;

//@Repository
public class MyRepositoryImpl<T, ID extends Serializable> extends QuerydslJpaRepository<T, ID> implements MyRepository<T, ID>
{
    private final EntityManager entityManager;

    public MyRepositoryImpl(JpaEntityInformation entityInformation, EntityManager entityManager)
    {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
    }

    @Transactional
    public <S extends T> S save(S entity, boolean saveOrUpdate)
    {
        if(saveOrUpdate)
        {
            return save(entity);
        }
        
        return persist(entity);
    }

    @Transactional
    public <S extends T> S persist(S entity)
    {
        entityManager.persist(entity);
        return entity;
    }

    @Transactional
    public <S extends T> S merge(S entity)
    {
        return entityManager.merge(entity);
    }

    @Transactional
    public <S extends T> void detach(S entity)
    {
        entityManager.detach(entity);
    }

}

