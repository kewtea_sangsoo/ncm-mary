package com.ncmmary.repository.impl;

import com.ncmmary.entity.QUser;
import com.ncmmary.entity.User;
import com.ncmmary.repository.UserRepository;
import com.ncmmary.repository.custom.UserRepositoryCustom;

import org.springframework.data.jpa.repository.support.QuerydslJpaRepository;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.Set;
import java.util.Iterator;

public class UserRepositoryImpl implements UserRepositoryCustom 
{
    private static final Logger log 
        = LoggerFactory.getLogger(UserRepositoryImpl.class);

     public Pageable getPageable(int page, int size, Short orderBy, boolean desc)
    {
        Pageable pageable = null;

        if (null != orderBy)
        {
            List<String> properties = null;

            if (UserRepository.OrderBy.ID == orderBy)
            {
                properties = Arrays.asList("id");
            } else if (UserRepository.OrderBy.CREATED_TIME == orderBy) {
                properties = Arrays.asList("createdWhen", "id");
            } else if (UserRepository.OrderBy.LAST_UPDATED_TIME == orderBy) {
                properties = Arrays.asList("updatedWhen", "id");
            } else {
                throw new IllegalArgumentException("The 'orderBy' value is illegal!");
            }

            pageable = PageRequest.of(page, size, desc ? Sort.Direction.DESC : Sort.Direction.ASC, properties.toArray(new String[0]));

        }

        if (null == pageable) {
            pageable = new PageRequest(page, size);
        }

        return pageable;
    }

    public Predicate getPredicate(Collection<Long> ids,
                                    Collection<Short> roles,
                                    Collection<Short> statuses,
                                    Long createdTimeLowerBound,
                                    Long createdTimeUpperBound,
                                    Long lastUpdatedTimeLowerBound,
                                    Long lastUpdatedTimeUpperBound,
                                    Long pId, 
                                    Long pCreatedTime,
                                    Long pLastUpdatedTime,
                                    Short orderBy, 
                                    boolean desc)
    {

        Predicate predicate = null;

        QUser user = QUser.user;

        if (null != ids && 0 < ids.size())
        {
            predicate = user.id.in(ids).and(predicate);
        }

        if (null != roles && 0 < roles.size())
        {
            predicate = user.role.in(roles).and(predicate);
        }

        if (null != statuses && 0 < statuses.size())
        {
            predicate = user.status.in(statuses).and(predicate);
        }

        if (null != createdTimeLowerBound)
        {
            predicate = user.createdWhen.goe(createdTimeLowerBound).and(predicate);
        }

        if (null != createdTimeUpperBound)
        {
            predicate = user.createdWhen.lt(createdTimeUpperBound).and(predicate);
        }

        if (null != lastUpdatedTimeUpperBound)
        {
            predicate = user.updatedWhen.lt(lastUpdatedTimeUpperBound).and(predicate);
        }

        if (null != orderBy) {

            if (UserRepository.OrderBy.ID == orderBy) {

                if (desc) 
                {
                    if (null != pId)
                    {
                        predicate = user.id.lt(pId).and(predicate);
                    } 
                }
                else
                {
                    if (null != pId)
                    {
                        predicate = user.id.gt(pId).and(predicate);
                    }
                }
            } else if (UserRepository.OrderBy.CREATED_TIME == orderBy) {
                if (desc)
                {
                    if (null != pCreatedTime) 
                    {
                        BooleanExpression expr = user.createdWhen.lt(pCreatedTime);
                        if (null != pId)
                        {
                            expr = expr.or(user.createdWhen.eq(pCreatedTime).and(user.id.lt(pId)));
                        }

                        predicate = expr.and(predicate);
                    }
                }
                else
                {
                    if (null != pCreatedTime)
                    {
                        BooleanExpression expr = user.createdWhen.gt(pCreatedTime);
                        if (null != pId)
                        {
                            expr = expr.or(user.createdWhen.eq(pCreatedTime).and(user.id.gt(pId)));
                        }

                        predicate = expr.and(predicate);
                    }
                }
            } else if (UserRepository.OrderBy.LAST_UPDATED_TIME == orderBy) {
                if (desc)
                {
                    if (null != pLastUpdatedTime)
                    {
                        BooleanExpression expr = user.updatedWhen.lt(pLastUpdatedTime);
                        if (null != pId)
                        {
                            expr = expr.or(user.updatedWhen.eq(pLastUpdatedTime).and(user.id.lt(pId)));
                        }

                        predicate = expr.and(predicate);

                    }
                }
                else
                {
                    if (null != pLastUpdatedTime)
                    {
                        BooleanExpression expr = user.updatedWhen.gt(pLastUpdatedTime);
                        if (null != pId)
                        {
                            expr = expr.or(user.updatedWhen.eq(pLastUpdatedTime).and(user.id.gt(pId)));
                        }

                        predicate = expr.and(predicate);
                    }
                }
            }
        }

        return predicate;
    }

   
}
