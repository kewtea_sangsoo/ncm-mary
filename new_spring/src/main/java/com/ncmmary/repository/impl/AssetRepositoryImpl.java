package com.ncmmary.repository.impl;

import com.ncmmary.entity.QAsset;
import com.ncmmary.entity.Asset;
import com.ncmmary.repository.AssetRepository;
import com.ncmmary.repository.custom.AssetRepositoryCustom;

import org.springframework.data.jpa.repository.support.QuerydslJpaRepository;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.Set;
import java.util.Iterator;


public class AssetRepositoryImpl implements AssetRepositoryCustom 
{
    private static final Logger log 
        = LoggerFactory.getLogger(AssetRepositoryImpl.class);

    public Pageable getPageable(int page, int size, Short orderBy, boolean desc)
    {

        Pageable pageable = null;

        if (null != orderBy)
        {
            List<String> properties = null;

            if (AssetRepository.OrderBy.ID == orderBy)
            {
                properties = Arrays.asList("id");
               
            } else if (AssetRepository.OrderBy.CREATED_TIME == orderBy)
            {
                properties = Arrays.asList("createdWhen", "id");
            } else if (AssetRepository.OrderBy.LAST_UPDATED_TIME == orderBy)
            {
                properties = Arrays.asList("updatedWhen", "id");
            } else {
                throw new IllegalArgumentException("The 'orderBy' value is illegal!");
                    
            }

            pageable = PageRequest.of(page, size, desc ? Sort.Direction.DESC : Sort.Direction.ASC, properties.toArray(new String[0]));
        }

        if (null == pageable) {
            pageable = new PageRequest(page, size);
        }

        return pageable;
    }

    public Predicate getPredicate(Collection<Long> ids, 
                                    Collection<Short> types, 
                                    Collection<Short> statuses, 
                                    Double latLowerBound, 
                                    Double latUpperBound, 
                                    Double lonLowerBound, 
                                    Double lonUpperBound, 
                                    Long createdTimeLowerBound,
                                    Long createdTimeUpperBound,
                                    Long lastUpdatedTimeLowerBound,
                                    Long lastUpdatedTimeUpperBound,
                                    Long pId, 
                                    Long pCreatedTime,
                                    Long pLastUpdatedTime,
                                    Short orderBy, 
                                    boolean desc)
    {
        Predicate predicate = null;

        QAsset asset = QAsset.asset;

        if (null != ids && 0 < ids.size())
        {
            predicate = asset.id.in(ids).and(predicate);
        }

        if (null != types && 0 < types.size())
        {
            predicate = asset.type.in(types).and(predicate);
        }

        if (null != statuses && 0 < statuses.size())
        {
            predicate = asset.status.in(statuses).and(predicate);
        }

        if (null != latLowerBound)
        {
            predicate = asset.latitude.goe(latLowerBound).and(predicate);
        }

        if (null != latUpperBound)
        {
            predicate = asset.latitude.lt(latUpperBound).and(predicate);
        }

        if (null != lonLowerBound)
        {
            predicate = asset.longitude.goe(lonLowerBound).and(predicate);
        }

        if (null != lonUpperBound)
        {
            predicate = asset.longitude.lt(lonUpperBound).and(predicate);
        }

        if (null != createdTimeLowerBound)
        {
            predicate = asset.createdWhen.goe(createdTimeLowerBound).and(predicate);
        }

        if (null != createdTimeUpperBound)
        {
            predicate = asset.createdWhen.lt(createdTimeUpperBound).and(predicate);
        }

        if (null != lastUpdatedTimeLowerBound)
        {
            predicate = asset.updatedWhen.goe(lastUpdatedTimeLowerBound).and(predicate);
        }

        if (null != lastUpdatedTimeUpperBound)
        {
            predicate = asset.updatedWhen.lt(lastUpdatedTimeUpperBound).and(predicate);
        }

        if (null != orderBy) {

            if (AssetRepository.OrderBy.ID == orderBy) {

                if (desc) 
                {
                    if (null != pId)
                    {
                        predicate = asset.id.lt(pId).and(predicate);
                    } 
                }
                else
                {
                    if (null != pId)
                    {
                        predicate = asset.id.gt(pId).and(predicate);
                    }
                }
            } else if (AssetRepository.OrderBy.CREATED_TIME == orderBy) {
                if (desc)
                {
                    if (null != pCreatedTime) 
                    {
                        BooleanExpression expr = asset.createdWhen.lt(pCreatedTime);
                        if (null != pId)
                        {
                            expr = expr.or(asset.createdWhen.eq(pCreatedTime).and(asset.id.lt(pId)));
                        }

                        predicate = expr.and(predicate);
                    }
                }
                else
                {
                    if (null != pCreatedTime)
                    {
                        BooleanExpression expr = asset.createdWhen.gt(pCreatedTime);
                        if (null != pId)
                        {
                            expr = expr.or(asset.createdWhen.eq(pCreatedTime).and(asset.id.gt(pId)));
                        }

                        predicate = expr.and(predicate);
                    }
                }
            } else if (AssetRepository.OrderBy.LAST_UPDATED_TIME == orderBy) {
                if (desc)
                {
                    if (null != pLastUpdatedTime)
                    {
                        BooleanExpression expr = asset.updatedWhen.lt(pLastUpdatedTime);
                        if (null != pId)
                        {
                            expr = expr.or(asset.updatedWhen.eq(pLastUpdatedTime).and(asset.id.lt(pId)));
                        }

                        predicate = expr.and(predicate);

                    }
                }
                else
                {
                    if (null != pLastUpdatedTime)
                    {
                        BooleanExpression expr = asset.updatedWhen.gt(pLastUpdatedTime);
                        if (null != pId)
                        {
                            expr = expr.or(asset.updatedWhen.eq(pLastUpdatedTime).and(asset.id.gt(pId)));
                        }

                        predicate = expr.and(predicate);
                    }
                }
            }
        }

        return predicate;
    }

}


