package com.ncmmary.interceptor;

import org.springframework.web.context.request.WebRequestInterceptor;
import org.springframework.web.context.request.WebRequest;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.context.request.RequestAttributes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.Map;

public class PageContextInterceptor implements WebRequestInterceptor
{
    private static final Logger log = LoggerFactory.getLogger(PageContextInterceptor.class);

    public void afterCompletion(WebRequest request, Exception ex)
    {

    }

    public void postHandle(WebRequest request, ModelMap model)
    {
    }

    public void preHandle(WebRequest request)
    {

        Map pathVariables 
            = (Map)request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE, RequestAttributes.SCOPE_REQUEST); // ?: This attribute is not required to be supported by all HandlerMapping implementations
        log.info("pathVariables : " + pathVariables);
    }

}
