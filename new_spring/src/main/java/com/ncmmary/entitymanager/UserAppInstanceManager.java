package com.ncmmary.entitymanager;

import com.ncmmary.core.Clock;

import com.ncmmary.entity.UserAppInstance;
import com.ncmmary.entity.IdGenerator;
import com.ncmmary.repository.UserAppInstanceRepository;

import com.querydsl.core.types.Predicate;

import lombok.Data;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import org.springframework.validation.Errors;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Collection;


@Service
public class UserAppInstanceManager
{
    private static final Logger log
        = LoggerFactory.getLogger(UserAppInstanceManager.class);

    @Autowired
    private Clock clock;

    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    private UserAppInstanceRepository userAppInstanceRepository;

    @Transactional
    public UserAppInstance register(String appInstanceId, Short status, Long createdWhen)
    {
        UserAppInstance userAppInstance = new UserAppInstance();
        userAppInstance.setId(idGenerator.genIdLong());
        userAppInstance.setAppInstanceId(appInstanceId);
        userAppInstance.setStatus(status);
        userAppInstance.setStatusLastUpdatedWhen(createdWhen);
        userAppInstance.setCreatedWhen(createdWhen);

        userAppInstance = userAppInstanceRepository.persist(userAppInstance);

        return userAppInstance;
    }

}


