package com.ncmmary.entitymanager;

import com.ncmmary.exception.NotFoundException;

import com.ncmmary.core.Clock;

import com.ncmmary.entity.AuthToken;
import com.ncmmary.entity.IdGenerator;

import com.ncmmary.repository.AuthTokenRepository;

import com.ncmmary.parameters.RevokeAuthTokensForm;

import com.querydsl.core.types.Predicate;

import lombok.Data;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Collection;


@Service
public class AuthTokenManager
{
    private static final Logger log 
        = LoggerFactory.getLogger(AuthTokenManager.class);

    @Autowired
    private Clock clock;

    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    private AuthTokenRepository authTokenRepository;


    public static final long EXPIRATION_PERIOD_IN_MILLIS = Long.MAX_VALUE;

    public Long getExpirationPeriodInMillis()
    {
        return EXPIRATION_PERIOD_IN_MILLIS;
    }

    public AuthToken getById(String id)
    {
        AuthToken authToken = authTokenRepository.findById(id).orElse(null);

        return authToken;
    }

    @Transactional
    public AuthToken register(Short type, Long userId, Short status, Long createdWhen)
    {
        AuthToken authToken = new AuthToken();
        authToken.setId(idGenerator.genId());
        authToken.setUserId(userId);
        //authToken.setType(AuthToken.Type.);
        authToken.setStatus(status);
        authToken.setStatusLastUpdatedWhen(createdWhen);
        authToken.setCreatedWhen(createdWhen);

        Long expiredWhen = Long.MAX_VALUE;
        Long _expiredWhen = getExpirationPeriodInMillis() + authToken.getCreatedWhen();
        if (0 < _expiredWhen) {
            expiredWhen = _expiredWhen;
        }
        authToken.setExpiredWhen(expiredWhen);

        authToken = authTokenRepository.persist(authToken);

        return authToken;
    }

    public Collection<AuthToken> revoke(RevokeAuthTokensForm form, Errors errors)
    {
        Collection<AuthToken> authTokens = null;

        Collection<Long> userIds = form.getUserIds();
        Collection<String> authTokenIds = form.getAuthTokenIds();



        return authTokens;
    }


    @Transactional
    private AuthToken changeStatus(String id,
                                    Short status,
                                    Long statusLastUpdatedWhen,
                                    boolean ignoreLoop) throws NotFoundException
    {
        AuthToken authToken = getById(id);

        if (null == authToken)
        {
            throw new NotFoundException();
        }

        authToken.setStatus(status);
        authToken.setStatusLastUpdatedWhen(statusLastUpdatedWhen);

        authToken = authTokenRepository.save(authToken);

        return authToken;
    }

    @Transactional
    private AuthToken update(String id,
                                Boolean userIdModified,
                                Long userId,
                                Boolean typeModified,
                                Short type) throws NotFoundException
    {
        AuthToken authToken = getById(id);

        if (null == authToken) {
            throw new NotFoundException();
        }

        if (null != userIdModified && userIdModified) {
            authToken.setUserId(userId);
        }

        if (null != typeModified && typeModified) {
            authToken.setType(type);
        }

        return authToken;
    }

    @Transactional
    public AuthToken registerOrUpdate(final String id, final Short type, final Long userId, final Short status, Long when)
    {
        AuthToken authToken = null;

        try {
            authToken = update(id, true, userId, true, type);
            if (!authToken.getStatus().equals(status)) {
                authToken = changeStatus(authToken.getId(), status, when, true);
            }

        } catch(NotFoundException e) {

            authToken = register(type, userId, status, when);

        }

        return authToken;

    }
}


