package com.ncmmary.entitymanager;

import com.ncmmary.core.Clock;

import com.ncmmary.entity.UserAppInstanceAssociation;
import com.ncmmary.entity.UserAppInstanceAssociationId;

import com.ncmmary.repository.UserAppInstanceAssociationRepository;

import com.querydsl.core.types.Predicate;

import lombok.Data;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import org.springframework.validation.Errors;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Collection;


@Service
public class UserAppInstanceAssociationManager
{
    private static final Logger log
        = LoggerFactory.getLogger(UserAppInstanceAssociationManager.class);

    @Autowired
    private Clock clock;

    @Autowired
    private UserAppInstanceAssociationRepository userAppInstanceAssociationRepository;


    public UserAppInstanceAssociation register(Long userId, Long userAppInstanceId, Short status, Long createdWhen)
    {
        UserAppInstanceAssociationId userAppInstanceAssociationId 
            = new UserAppInstanceAssociationId();
        userAppInstanceAssociationId.setUserId(userId);
        userAppInstanceAssociationId.setUserAppInstanceId(userAppInstanceId);

        UserAppInstanceAssociation userAppInstanceAssociation 
            = new UserAppInstanceAssociation();

        userAppInstanceAssociation.setId(userAppInstanceAssociationId);
        userAppInstanceAssociation.setStatus(status);
        userAppInstanceAssociation.setStatusLastUpdatedWhen(createdWhen);
        userAppInstanceAssociation.setCreatedWhen(createdWhen);

        userAppInstanceAssociation 
            = userAppInstanceAssociationRepository
                .persist(userAppInstanceAssociation);

        return userAppInstanceAssociation;
    }
}

