package com.ncmmary.util;

import com.nexmo.client.NexmoClient;
import com.nexmo.client.sms.MessageStatus;
import com.nexmo.client.sms.SmsSubmissionResponse;
import com.nexmo.client.sms.messages.TextMessage;

import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Component
public class SMS
{
    private static final Logger log = LoggerFactory.getLogger(SMS.class);

    @Autowired
    private Environment env;

    public void sendMessage(String to, String message)
    {
        String from = env.getProperty("ncmmary.company.name");
        String apiKey = env.getProperty("ncmmary.nexmo.api-key");
        String apiSecret = env.getProperty("ncmmary.nexmo.api-secret");
        NexmoClient client = NexmoClient.builder().apiKey(apiKey).apiSecret(apiSecret).build();

        TextMessage textMessage = new TextMessage(from, to, message);

        SmsSubmissionResponse response = client.getSmsClient().submitMessage(textMessage);

    }


}


