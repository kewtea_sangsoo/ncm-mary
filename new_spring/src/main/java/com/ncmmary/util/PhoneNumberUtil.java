package com.ncmmary.util;

import com.ncmmary.exception.InvalidPhoneNumberException;

//import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.google.i18n.phonenumbers.NumberParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PhoneNumberUtil
{
    private static final Logger log = LoggerFactory.getLogger(PhoneNumberUtil.class);

    public static String canonicalize(String phoneNumber) 
        throws InvalidPhoneNumberException
    {
        String phoneNumberCanonicalized = null;

        com.google.i18n.phonenumbers.PhoneNumberUtil phoneNumberUtil 
            = com.google.i18n.phonenumbers.PhoneNumberUtil.getInstance();

        try{
            PhoneNumber pn = phoneNumberUtil.parse(phoneNumber, null);
            if (!phoneNumberUtil.isValidNumber(pn)) {
                throw new InvalidPhoneNumberException();
            }

            phoneNumberCanonicalized 
                = phoneNumberUtil.format(pn, PhoneNumberFormat.E164);

        }catch(NumberParseException e) {
            throw new InvalidPhoneNumberException();
        }

        return phoneNumberCanonicalized;

    }

}



