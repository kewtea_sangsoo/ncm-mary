package com.ncmmary.util;

import com.ncmmary.exception.FileCreationException;

import java.security.Security;
import java.security.SecureRandom;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.net.URL;
import java.net.URI;
import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Catchall
{
    private static final Logger log = LoggerFactory.getLogger(Catchall.class);

    public static Long generateRandomLong(long time)
    {
        SecureRandom random = new SecureRandom();
        random.setSeed(time);
        return random.nextLong();
    }

    public static String generateRandomHexString(long time)
    {
        SecureRandom random = new SecureRandom();
        byte[] rBytes = new byte[4];
        random.nextBytes(rBytes);

        StringBuffer sb = new StringBuffer();
        for(byte b : rBytes)
        {
            String hex = Integer.toHexString(0xff & b);
            if(hex.length() == 1)
            {
                sb.append('0');
            }
            sb.append(hex);
        }
        
        String hexCurrentTime = Long.toHexString(time);
        int padding = 16 - hexCurrentTime.length();
        for(int i=0; i<padding; i++)
        {
            sb.append('0');
        }
        sb.append(hexCurrentTime);

        return new String(sb);
    }

    public static Collection<String> getURLsFromText(String text)
    {
        
    ArrayList links = new ArrayList();
    if(null!=text)
    {
     
     //String regex = "\\(?\\b(http://|www[.])[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]";
     String regex ="\\(?\\b(http://|www[.])\\S*";
     Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
     Matcher m = p.matcher(text);
     while(m.find()) {
         String urlStr = m.group();
         if (urlStr.startsWith("(") && urlStr.endsWith(")"))
         {
             urlStr = urlStr.substring(1, urlStr.length() - 1);
         }
         links.add(urlStr);
     }
    }
     return links;
    }

    public static int getURLDepth(String url)
                        throws URISyntaxException
    {
        int depth = 0;
        URI uri = (new URI(url)).normalize();
        URI _uri = null;

        if(!uri.getPath().endsWith("/"))
        {
            _uri = uri.resolve(".").normalize();
            if(uri.toString().length() > _uri.toString().length())
            {
                depth++;
                uri = _uri;
            }
        }
            

        while(true)
        {
            _uri = uri.resolve("..").normalize();

            if(uri.toString().length() > _uri.toString().length())
            {
                depth++;
                uri = _uri;
            }
            else
            {
                break;        
            }

        }
        return depth;
    }

    public static List<String> getParentURLs(String url, int depth)
                                throws URISyntaxException
    {
        URI uri = (new URI(url)).normalize();
        URI _uri = null;

        List<String> parentURLs = new ArrayList<String>();

        if(!uri.getPath().endsWith("/"))
        {
            _uri = uri.resolve(".").normalize();
            if(uri.toString().length() > _uri.toString().length())
            {
                parentURLs.add(_uri.toString());
                uri = _uri;
            }
        }
            

        for(int i = 0; i<depth; i++)
        {
            _uri = uri.resolve("..").normalize();

            if(uri.toString().length() > _uri.toString().length())
            {
                parentURLs.add(_uri.toString());
            }
            else
            {
                break;        
            }

            uri = _uri;
        }

        return parentURLs;
    }

    public static String getFilenameWithoutExtension(File file)
    {
        String filename = file.getName();

        int lastPeriodIndex = filename.lastIndexOf(".");
        if(0<lastPeriodIndex)
        {
            filename = filename.substring(0, lastPeriodIndex);
        }
        
        return filename;
    }

    public static String getFileExtension(File file)
    {
        String ext = null;
        String filename = file.getName();

        int lastPeriodIndex = filename.lastIndexOf(".");
        if(0<lastPeriodIndex)
        {
            ext = filename.substring(lastPeriodIndex);
        }

        return ext;
    }

    public static File createDir(File parentDir, String dirNameBase) throws FileCreationException
    {
        final int maxNRetry = 99;
        File file = null;
        
        for(int i=0; i<maxNRetry; i++)
        {
            String dirName  = dirNameBase;
            if(0<i)
            {
                dirName = dirNameBase + "(" + i +")";
            }

            try{
                file = new File(parentDir, dirName);
                if(file.mkdir())
                {
                    break;
                }
            }catch(Exception e){}
        }

        if(null == file || !file.isDirectory())
        {
            throw new FileCreationException();
        }

        return file;
    }


    public static File createEmptyFile(File dir, String defaultFilename) throws FileCreationException
    {
        boolean success = false;
        final int maxNRetry = 9999;

        File file = null;

        File defaultFile = new File(dir, defaultFilename);

        String fileExtension = getFileExtension(defaultFile);

        String filename = defaultFilename;
        String filenameBase = getFilenameWithoutExtension(defaultFile);

        for(int i = 0; i<maxNRetry; i++)
        {
            if(0 < i)
            {
                filename = filenameBase + "(" + i+ ")"+  (null != fileExtension ? fileExtension : "");            
            }

            try{
                file = new File(dir, filename);
                if(file.createNewFile())
                {
                    success = true;
                    break;
                }
            }catch(Exception e){}
        }

        if(null == file || !file.isFile() || !success)
        {
            throw new FileCreationException();
        }

        return file;
    }

    public static String getCanonicalURLForm(URL url)
    {
        String canonicalURLForm = null;
        String protocol = url.getProtocol();
        String host = url.getHost();
        int port = url.getPort();
        String path = url.getPath();
        //String queryString = url.getQueryString();
        canonicalURLForm = protocol + "://" +  host + (port != -1 ? ":"+port : "") + (null != path ? path : "");
        return canonicalURLForm;
    }

    public static boolean isImageFile(File file)
    {
        boolean isImage = false;
        String ext = Catchall.getFileExtension(file);
        if(null != ext && (ext.equalsIgnoreCase(".png") ||
            ext.equalsIgnoreCase(".jpg") || ext.equalsIgnoreCase(".jpeg")))
        {
            isImage = true;
        }
        return isImage;
    }

    public static boolean isVideoFile(File file)
    {
        boolean isVideo = false;
        
        String ext = Catchall.getFileExtension(file);
        if(null != ext && (ext.equalsIgnoreCase(".avi") || ext.equalsIgnoreCase(".mkv") ||
            ext.equalsIgnoreCase(".mp4") || ext.equalsIgnoreCase(".mpg") || ext.equalsIgnoreCase(".mpeg") || ext.equalsIgnoreCase(".mov") || ext.equalsIgnoreCase(".flv") || ext.equalsIgnoreCase(".f4v")))
        {
            isVideo= true;
        }

        return isVideo;
    }

    public static boolean isArchiveFile(File file)
    {
        boolean isArchive = false;
        if(isZipFile(file))
        {
            isArchive = true;
        }

        return isArchive;
    }
    public static boolean isZipFile(File file)
    {
        boolean isZipFile = false;
    
        String ext = Catchall.getFileExtension(file);
        if(null != ext && ext.equalsIgnoreCase(".zip"))
        {
            isZipFile= true;
        }
        return isZipFile;
    }

    public static boolean isTarFile(File file)
    {
        boolean isTarFile = false;
        String ext = Catchall.getFileExtension(file); 
        if(null != ext && ext.equalsIgnoreCase(".tar"))
        {
            isTarFile= true;
        }
        return isTarFile;
    }


    public static void stream(InputStream inputStream, File outFile) throws Exception
    {
        FileOutputStream fout = null;
        try{
            
            fout = new FileOutputStream(outFile);

            byte bytes[] = new byte[1024];
            int cnt;
            while((cnt = inputStream.read(bytes, 0, 1024)) != -1)
            {
                fout.write(bytes, 0, cnt);
            }

        }catch(Exception e){

        }finally{
            if(null != inputStream)
            {
                try{
                    inputStream.close();
                }catch(Exception e){
                }
            }

            if(null != fout)
            {
                try{
                    fout.close();
                }catch(Exception e){
                }
            }
        }

    }

}
