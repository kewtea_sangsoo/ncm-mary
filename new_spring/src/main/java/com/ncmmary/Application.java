package com.ncmmary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.thymeleaf.spring5.view.ThymeleafViewResolver;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;
import org.thymeleaf.TemplateEngine;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import org.springframework.boot.web.server.ErrorPage;
import org.springframework.http.HttpStatus;

import org.springframework.context.annotation.AdviceMode;

@ComponentScan
@EnableAspectJAutoProxy
//@EnableJpaRepositories
@EnableJpaRepositories(basePackageClasses=com.ncmmary.repository.UserRepository.class, repositoryBaseClass = com.ncmmary.repository.impl.MyRepositoryImpl.class)
@EnableTransactionManagement(mode=AdviceMode.ASPECTJ)
@EnableScheduling
@EnableCaching(mode=AdviceMode.ASPECTJ) 
@EnableAsync
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

    }

    // Spring Boot Global Error Handling

    /*
    @Bean
    public EmbeddedServletContainerCustomizer containerCustomizer(){
        return new MyCustomizer();
    }

    private static class MyCustomizer implements EmbeddedServletContainerCustomizer{
        @Override
        public void customize(ConfigurableEmbeddedServletContainer container){
            container.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/404"));
            container.addErrorPages(new ErrorPage(HttpStatus.FORBIDDEN, "/403"));
        }
    }
    */
}
