package com.ncmmary.auth;

import com.ncmmary.exception.PermissionDeniedException;
import com.ncmmary.exception.UserAuthenticationException;
import com.ncmmary.exception.UserNotExistException;
import com.ncmmary.exception.UnauthorizedAccessException;

import com.ncmmary.entity.User;
import com.ncmmary.entity.AuthToken;
import com.ncmmary.entitymanager.UserManager;
import com.ncmmary.entitymanager.AuthTokenManager;

import com.ncmmary.security.AuthTokenAuthenticationToken;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.AuthenticationException;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Service
public class AuthService
{
    private static final Logger log = LoggerFactory.getLogger(AuthService.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserManager userManager;

    @Autowired
    private AuthTokenManager authTokenManager;

    public Authentication login(String authTokenId) throws UserAuthenticationException
    {
        AuthToken authToken = authTokenManager.getById(authTokenId);
        
        AuthTokenAuthenticationToken authTokenAuthenticationToken 
            = new AuthTokenAuthenticationToken(
                authToken.getUserId(), authToken.getId());

        Authentication auth = null;

        try{
            auth = authenticationManager
                    .authenticate(authTokenAuthenticationToken);

            SecurityContextHolder.getContext().setAuthentication(auth);

        }catch(AuthenticationException ae){ // Maybe UsernameNotFoundException is not thrown currently
            throw new UserAuthenticationException();
        }

        return auth;

    }

    public Authentication login(String email, String password) throws UserAuthenticationException
    {
        UsernamePasswordAuthenticationToken token 
            = new UsernamePasswordAuthenticationToken(email, password);

        Authentication auth = null;

        try{
            auth = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(auth);
        }catch(AuthenticationException ae){ // Maybe UsernameNotFoundException is not thrown currently
            throw new UserAuthenticationException();
        }

        return auth;
    }

}
