package com.ncmmary.shards;

public class DataSourceLookupKeyHolder
{
    private static final ThreadLocal<DataSourceLookupKey> lookupKey
        = new ThreadLocal<DataSourceLookupKey>();

    public static void set(DataSourceLookupKey key)
    {
        lookupKey.set(key);
    }

    public static DataSourceLookupKey get()
    {
        return lookupKey.get();
    }

    public static void remove()
    {
        lookupKey.remove();
    }
}

