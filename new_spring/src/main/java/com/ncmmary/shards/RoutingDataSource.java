package com.ncmmary.shards;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.context.annotation.EnableLoadTimeWeaving;
import static org.springframework.context.annotation.EnableLoadTimeWeaving.AspectJWeaving.*;
import org.springframework.context.annotation.DependsOn;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.boot.jdbc.XADataSourceWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.MutablePropertyValues;
//import org.springframework.boot.bind.RelaxedDataBinder;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.boot.context.properties.bind.Bindable;
import org.springframework.stereotype.Service;

import org.springframework.core.env.Environment;


import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ConcurrentHashMap;
import javax.sql.DataSource;
import javax.sql.XADataSource;
import javax.annotation.PostConstruct;

import org.springframework.context.annotation.DependsOn;
import org.springframework.context.ApplicationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.mysql.cj.jdbc.MysqlXADataSource;
import com.atomikos.jdbc.AtomikosDataSourceBean;


public class RoutingDataSource extends AbstractRoutingDataSource 
{
    private static final Logger log = LoggerFactory.getLogger(RoutingDataSource.class);

    private Environment env;

    @Autowired
    private ApplicationContext applicationContext;

    private DataSource setupDataSource; 

    private Map<DataSourceLookupKey, DataSource> targetDataSources;

    public RoutingDataSource(XADataSourceWrapper wrapper, Environment env)
    {
        this.env = env;

        targetDataSources = new ConcurrentHashMap<DataSourceLookupKey, DataSource>();

        // Set the setup-datasource
        MysqlXADataSource xaDataSource = new MysqlXADataSource();
        try {
            xaDataSource.setPinGlobalTxToPhysicalConnection(true);
        }catch(Exception e){
            throw new IllegalStateException();
        }

        Binder.get(env).bind("ncmmary.setup-datasource", Bindable.ofInstance(xaDataSource));

        try{
            AtomikosDataSourceBean wrapped = (AtomikosDataSourceBean)wrapper.wrapDataSource(xaDataSource);
            wrapped.setUniqueResourceName("setupDataSource");
            setupDataSource = wrapped;

        }catch(Exception e){
            throw new IllegalStateException();
        }
    }

    @PostConstruct
    public void init()
    {
        log.info("@PostConstruct init()");

        applicationContext.publishEvent(new DataSourceInitializedEvent(this));

    }

    @Override
    public void afterPropertiesSet(){

    }

    @Override
    protected DataSourceLookupKey determineCurrentLookupKey()
    {
        return DataSourceLookupKeyHolder.get();
    }

    @Override
    protected synchronized DataSource determineTargetDataSource() {
        DataSource dataSource = null;
        DataSourceLookupKey lookupKey = determineCurrentLookupKey();

        if(null != lookupKey)
        {
            dataSource = (DataSource)targetDataSources.get(lookupKey);
            if(null == dataSource)
            {
                // Get the most promising datasource
                DataSourceLookupKeyHolder.remove();


                // restore the original lookupKey
                DataSourceLookupKeyHolder.set(lookupKey);
            }
        }
        else
        {
            dataSource = setupDataSource;
        }

		return dataSource;
    }


    @Scheduled(fixedDelay=3000)
    public void refreshTargetDataSources()
    {
        log.info("refresh target datasources");
    }
}
