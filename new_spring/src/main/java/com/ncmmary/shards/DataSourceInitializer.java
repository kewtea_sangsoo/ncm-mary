package com.ncmmary.shards;

import org.springframework.context.event.EventListener;
import org.springframework.context.ApplicationEvent;

import org.springframework.context.annotation.Configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.ApplicationListener;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.EventListener;

import com.ncmmary.shards.DataSourceInitializedEvent;

import javax.sql.DataSource;

public class DataSourceInitializer 
{

    private static final Logger log = LoggerFactory.getLogger(DataSourceInitializer.class);

    public void initializeDataSources(DataSource setupDataSource)
    {
        log.info("initializeDataSources");
        initializeSetupDataSource(setupDataSource);
    }

    public void initializeSetupDataSource(DataSource dataSource)
    {
        log.info("initializeSetupDataSource");
    }

}
