package com.ncmmary.entity;

import com.ncmmary.core.Clock;
import com.ncmmary.util.Catchall;

public class IdGenerator
{
    private static final String PSEUDO_ID_PREFIX = "!";

    private Clock clock;

    public IdGenerator(Clock clock)
    {
        this.clock = clock;
    }
    
    public String genId()
    {
        return Catchall.generateRandomHexString(clock.getTime());
    }

    public Long genIdLong()
    {
        return Catchall.generateRandomLong(clock.getTime());
    }

    public String genPseudoId()
    {
        return PSEUDO_ID_PREFIX + genId().substring(PSEUDO_ID_PREFIX.length());
    }

    public boolean isPseudoId(String id)
    {
        boolean isPseudoId = false;

        if(null != id && id.startsWith(PSEUDO_ID_PREFIX))
        {
            isPseudoId = true;
        }

        return isPseudoId;
    }

    public boolean isEmptyId(String id)
    {
        boolean empty = false;

        if(null == id || isPseudoId(id))
        {
            empty = true;
        }

        return empty;
    }
}
    
