package com.ncmmary.config;

import springfox.documentation.swagger2.annotations.EnableSwagger2;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.builders.ApiInfoBuilder;
import static springfox.documentation.builders.PathSelectors.*;
import static springfox.documentation.builders.RequestHandlerSelectors.*;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.builders.ParameterBuilder;
import static com.google.common.base.Predicates.*;
import com.google.common.base.Predicate;

import org.springframework.stereotype.Controller;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiModelProperty;

import java.util.Arrays;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Configuration
@EnableSwagger2
public class Swagger2SpringBootConfig
{
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    public static @interface SecuredApi {};


    @Bean
    public Docket main()
    {
        return new Docket(DocumentationType.SWAGGER_2)
            .groupName("all")
            .apiInfo(mainInfo())
            .select()
                .apis(basePackage("com.ncmmary.controller"))
                .apis(withClassAnnotation(Controller.class))
                .apis(withMethodAnnotation(ApiOperation.class))
            .build()
            .globalOperationParameters(
                Arrays.asList(
                    new ParameterBuilder().name("X-CSRF-TOKEN").description("CSRF-token. Some APIs require it (usually 'POST' method requests). To obtain the token value refer the 'get-csrf-token' API (in 'Security' section).").modelRef(new ModelRef("string")).parameterType("header").required(false).build()));
            
    }

    private ApiInfo mainInfo()
    {
        return new ApiInfoBuilder()
                .title("NCM Mary APIs")
                .contact("Hosuk Song(hosuk@dmalt.co)")
                .version("0.1")
                .build();
    }

}
