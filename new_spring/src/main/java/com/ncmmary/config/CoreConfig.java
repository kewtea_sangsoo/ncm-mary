package com.ncmmary.config;

import com.ncmmary.core.*;

import com.ncmmary.entity.IdGenerator;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.context.annotation.EnableLoadTimeWeaving;
import static org.springframework.context.annotation.EnableLoadTimeWeaving.AspectJWeaving.*;
import org.springframework.context.annotation.DependsOn;

import java.util.concurrent.ExecutorService;

import org.springframework.context.annotation.DependsOn;

@Configuration
@EnableSpringConfigured
public class CoreConfig
{
    @Bean
    public Clock clock()
    {
        return new Clock();
    }

    @Bean
    public IdGenerator idGenerator(Clock clock)
    {
        return new IdGenerator(clock);
    }

}
