package com.ncmmary.config;

import com.ncmmary.interceptor.PageContextInterceptor;

import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.FormatterRegistry;


@Configuration
//@EnableWebMvc --> Comment out to keep Spring Boot MVC features
public class WebConfig extends WebMvcConfigurerAdapter
{
    @Override
    public void addInterceptors(InterceptorRegistry registry)
    {
        //registry.addInterceptor(new PageContextInterceptor());
        registry.addWebRequestInterceptor(new PageContextInterceptor()).addPathPatterns("/**");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry)
    {
        //registry.addResourceHandler(ResourceManager.CACHE_ENTRY_URL_ROOT + "/**").resourceChain(true).addResolver(cacheableResourceResolver);
    }

    @Override
    public void addFormatters(FormatterRegistry registry)
    {
        //Add formatters and/or converters
        
    }
}
