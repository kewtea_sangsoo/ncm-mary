package com.ncmmary.config;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.context.annotation.EnableLoadTimeWeaving;
import static org.springframework.context.annotation.EnableLoadTimeWeaving.AspectJWeaving.*;
import org.springframework.context.annotation.DependsOn;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.boot.jdbc.XADataSourceWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.MutablePropertyValues;

import org.springframework.core.env.Environment;


import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ConcurrentHashMap;
import javax.sql.DataSource;
import javax.sql.XADataSource;

import org.springframework.context.annotation.DependsOn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.mysql.cj.jdbc.MysqlXADataSource;
import com.atomikos.jdbc.AtomikosDataSourceBean;

import org.springframework.context.ApplicationListener;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.EventListener;

import com.ncmmary.shards.DataSourceInitializedEvent;

import com.ncmmary.shards.DataSourceInitializer;

@Configuration
public class DatabaseShardsConfig implements ApplicationListener
{
    private static final Logger log = LoggerFactory.getLogger(DatabaseShardsConfig.class);

    @Autowired
    private Environment env;

    @Bean
    public com.ncmmary.shards.DataSourceInitializer dataSourceInitializer()
    {
        return new com.ncmmary.shards.DataSourceInitializer();
    }

    @Bean
    public DataSource routingDataSource(XADataSourceWrapper wrapper)
    {
        return new com.ncmmary.shards.RoutingDataSource(wrapper, env);
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event){
        if(event instanceof DataSourceInitializedEvent)
        {
            log.info("!event : " +(DataSourceInitializedEvent)event);


            String initialize = env.getProperty("ncmmary.datasource.initialize");
            log.info("env.getProperty(\"ncmmary.datasource.initialize\") : " + initialize);

            if(new Boolean(initialize))
            {
                (new DataSourceInitializer()).initializeDataSources((DataSource)event.getSource());
            }
        }
    }
}
