package com.ncmmary.config;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.WebDataBinder;

@ControllerAdvice
public class GlobalControllerAdvice
{

    @InitBinder
    protected void initBinder(WebDataBinder binder)
    {

    }
}
