package com.ncmmary.config;

import com.ncmmary.security.CustomAccessDeniedHandler;

import org.springframework.context.annotation.Bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.context.annotation.Configuration;


import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;



import java.security.Security;
import java.security.SecureRandom;


//@EnableWebSecurity //Do not switch off the default webapp security settings in Spring Boot. Just customize it
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter
{
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        //http.csrf().disable();
        
        http.exceptionHandling().accessDeniedHandler(accessDeniedHandler());

        http
            .authorizeRequests()
                .antMatchers("/admin").hasRole("ADMIN")
                .anyRequest().permitAll()
                .and()
            .formLogin()
                .loginPage("/login")
                .usernameParameter("signin-email")
                .passwordParameter("signin-password")
                .defaultSuccessUrl("/")
                //.loginProcessingUrl("/login")
                .failureUrl("/login?error")
                .permitAll()
                .and()
            .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/")
                //.logoutSuccessHandler(...) // if this is specified, logoutSuccessUrl() is ignored
                .permitAll();
    }

    @Bean
    public PasswordEncoder passwordEncoder()
    {
        return new BCryptPasswordEncoder(10, new SecureRandom());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
        return new CustomAccessDeniedHandler();
    }

}
