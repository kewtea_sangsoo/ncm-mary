package com.ncmmary.config;

import com.ncmmary.security.AuthTokenAuthenticationProvider;
import com.ncmmary.security.AuthTokenAuthenticationToken;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.context.annotation.Configuration;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;



@Configuration
public class AuthenticationManagerConfig extends GlobalAuthenticationConfigurerAdapter
{

    @Autowired
    private AuthTokenAuthenticationProvider authTokenAuthenticationProvider;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDetailsService  userDetailsService;

    @Override
    public void init(AuthenticationManagerBuilder auth) throws Exception
    {
        auth
            .authenticationProvider(authTokenAuthenticationProvider)
            .userDetailsService(userDetailsService)
            .passwordEncoder(passwordEncoder);
    }
}

