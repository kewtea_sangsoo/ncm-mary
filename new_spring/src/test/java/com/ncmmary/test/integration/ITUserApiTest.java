import com.ncmmary.Application;
import com.ncmmary.core.Clock;
import com.ncmmary.entity.IdGenerator;
import com.ncmmary.entity.AuthToken;
import com.ncmmary.entity.User;
import com.ncmmary.entity.UserValidationKey;
import com.ncmmary.entitymanager.UserValidationKeyManager;
import com.ncmmary.repository.UserRepository;
import com.ncmmary.controller.RestApiResponseBody;
import com.ncmmary.controller.UserController.*;
import com.ncmmary.controller.SecurityController.*;
import com.ncmmary.controller.UserValidationKeyController.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import org.junit.Rule;
import org.junit.runner.RunWith;

import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.mock.web.MockHttpSession;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentationConfigurer;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;


//import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.web.servlet.htmlunit.webdriver.MockMvcHtmlUnitDriverBuilder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.setup.MockMvcConfigurer;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.http.MediaType;
import javax.servlet.http.Cookie;

import java.util.List;
import java.util.Collection;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@TestPropertySource(locations="classpath:/config/application.properties")
public class ITUserApiTest
{
    private static final Logger log = LoggerFactory.getLogger(ITUserApiTest.class);

    private WebDriver driver;

    private MockMvc mockMvc;

    @Autowired
    private Environment env;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    private UserValidationKeyManager userValidationKeyManager;

    @Rule
    public JUnitRestDocumentation restDocumentation
        = new JUnitRestDocumentation("target/asciidoc/snippets");

    @Before
    public void setup() throws Exception
    {
        MockMvcRestDocumentationConfigurer mockMvcRestDocumentationConfigurer
            = documentationConfiguration(this.restDocumentation);
        
        MockMvcConfigurer mockMvcOperationPreprocessorsConfigurer 
            = mockMvcRestDocumentationConfigurer.operationPreprocessors().withResponseDefaults(prettyPrint());
        MockMvcConfigurer uriConfigurer = mockMvcRestDocumentationConfigurer.uris().withHost(env.getProperty("ncmmary.api.host"));

        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
                .apply(springSecurity())
                .apply(mockMvcOperationPreprocessorsConfigurer)
                .apply(uriConfigurer)
                .build();

        this.driver = MockMvcHtmlUnitDriverBuilder.webAppContextSetup(this.wac, springSecurity()).build();
    }

    private GetCsrfTokenRestApiResponseBody testGetCsrfTokenApiUsingMockMvc(MockHttpSession mockHttpSession) throws Exception
    {
        // CSRF 토큰 가져오기
        MvcResult result 
            = this.mockMvc.perform(get("/api/security/get-csrf-token")
                .session(mockHttpSession))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        GetCsrfTokenRestApiResponseBody getCsrfTokenRestApiResponseBody 
            = mapper.readValue(contentAsString, GetCsrfTokenRestApiResponseBody.class);

        String csrfToken = getCsrfTokenRestApiResponseBody.getToken();
        String csrfTokenParameterName = getCsrfTokenRestApiResponseBody.getParameterName();
        String csrfTokenHeaderName = getCsrfTokenRestApiResponseBody.getHeaderName();
        Assert.assertNotNull(csrfToken);
        Assert.assertNotNull(csrfTokenParameterName);
        Assert.assertNotNull(csrfTokenHeaderName);

        return getCsrfTokenRestApiResponseBody;
    }

    @Test
    public void testUserApisMockMvc() throws Exception
    {
        MockHttpSession mockHttpSession = new MockHttpSession(this.wac.getServletContext(), "mock-session-id");

        ObjectMapper mapper = new ObjectMapper();
        Cookie[] cookies = { new Cookie("JSESSIONID", "84EB475291ED0E6ED3FB964BAFF5") }; // Pseudo cookies

        MvcResult result = null;
        String contentAsString = null;

        GetCsrfTokenRestApiResponseBody getCsrfTokenRestApiResponseBody 
            = testGetCsrfTokenApiUsingMockMvc(mockHttpSession);
        String csrfToken = getCsrfTokenRestApiResponseBody.getToken();
        String csrfTokenParameterName = getCsrfTokenRestApiResponseBody.getParameterName();
        String csrfTokenHeaderName = getCsrfTokenRestApiResponseBody.getHeaderName();

        log.info("!!! csrfToken: " + csrfToken);
        log.info("!!! csrfTokenHeaderName: " + csrfTokenHeaderName);


        // 성공
        result = this.mockMvc
                    .perform(
                        post("/api/user/register")
                        .header(csrfTokenHeaderName, csrfToken)
                        .session(mockHttpSession)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                        .param("mobilePhone", "+82 10 6893 0218"))
                    .andExpect(status().isOk())
                    .andReturn();

        contentAsString = result.getResponse().getContentAsString();

        RegisterUserRestApiResponseBody registerUserRestApiResponseBody = mapper.readValue(contentAsString, RegisterUserRestApiResponseBody.class); 

        User registeredUser = registerUserRestApiResponseBody.getUser();
        Assert.assertNotNull(registeredUser);
        AuthToken authToken = registerUserRestApiResponseBody.getAuthToken();
        Assert.assertNotNull(authToken);
        String authTokenId = authToken.getId();
        Assert.assertNotNull(authTokenId);

        UserValidationKey userValidationKey = userValidationKeyManager.getById(registeredUser.getId());
        Assert.assertNotNull(userValidationKey);


        // 인증코드 입력
        getCsrfTokenRestApiResponseBody 
            = testGetCsrfTokenApiUsingMockMvc(mockHttpSession);
        csrfToken = getCsrfTokenRestApiResponseBody.getToken();
        csrfTokenParameterName = getCsrfTokenRestApiResponseBody.getParameterName();
        csrfTokenHeaderName = getCsrfTokenRestApiResponseBody.getHeaderName();

        log.info("!!! csrfToken: " + csrfToken);
        log.info("!!! csrfTokenHeaderName: " + csrfTokenHeaderName);


        result = this.mockMvc
                    .perform(
                        post("/api/validate-user")
                        .header(csrfTokenHeaderName, csrfToken)
                        .session(mockHttpSession)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                        .param("login", Boolean.toString(true))
                        .param("code", userValidationKey.getValue())
                        .param("userId", registeredUser.getId().toString()))
                    .andDo(document("validateUserUserUsingPOST", preprocessResponse(prettyPrint())))
                    .andExpect(status().isOk())
                    .andReturn();

        contentAsString = result.getResponse().getContentAsString();

        ValidateUserRestApiResponseBody validateUserRestApiResponseBody = mapper.readValue(contentAsString, ValidateUserRestApiResponseBody.class);
        
        User loggedInUser = validateUserRestApiResponseBody.getUser();
        Assert.assertNotNull(loggedInUser);
        authToken = validateUserRestApiResponseBody.getAuthToken();
        Assert.assertNotNull(authToken);
        authTokenId = authToken.getId();
        Assert.assertNotNull(authTokenId);



        // 로그인
        result = this.mockMvc
                    .perform(
                        post("/api/login")
                        .header(csrfTokenHeaderName, csrfToken)
                        .session(mockHttpSession)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                        .param("authToken", authTokenId))
                    .andDo(document("loginUserUsingPOST", preprocessResponse(prettyPrint())))
                    .andExpect(status().isOk())
                    .andReturn();

                /*
        result = this.mockMvc
                    .perform(
                        post("/api/login")
                        .header(csrfTokenHeaderName, csrfToken)
                        .session(mockHttpSession)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                        .param("username", email)
                        .param("password", password))
                    .andDo(document("loginUserUsingPOST", preprocessResponse(prettyPrint())))
                    .andExpect(status().isOk())
                    .andReturn();
                    */

        
        contentAsString = result.getResponse().getContentAsString();

        LoginUserRestApiResponseBody loginUserRestApiResponseBody 
            = mapper.readValue(contentAsString, LoginUserRestApiResponseBody.class);

        loggedInUser = loginUserRestApiResponseBody.getUser();
        Assert.assertNotNull(loggedInUser);
        Assert.assertEquals(loggedInUser.getId(), registeredUser.getId());


        // ME, 로그인된 사용자 본인 정보.

        result = this.mockMvc.perform(get("/api/me").session(mockHttpSession).cookie(cookies))
                .andDo(document("meUsingGET", preprocessResponse(prettyPrint())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();



        // 로그아웃
        //로그아웃을 위해 인증코드 재발급 (세션 리플레시 체크)

        result 
            = this.mockMvc.perform(get("/api/security/get-csrf-token")
                .cookie(cookies)
                .session(mockHttpSession))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();


        contentAsString = result.getResponse().getContentAsString();

        getCsrfTokenRestApiResponseBody 
            = mapper.readValue(contentAsString, GetCsrfTokenRestApiResponseBody.class);

        csrfToken = getCsrfTokenRestApiResponseBody.getToken();
        csrfTokenParameterName = getCsrfTokenRestApiResponseBody.getParameterName();
        csrfTokenHeaderName = getCsrfTokenRestApiResponseBody.getHeaderName();
        Assert.assertNotNull(csrfToken);
        Assert.assertNotNull(csrfTokenParameterName);
        Assert.assertNotNull(csrfTokenHeaderName);


        result = this.mockMvc
                    .perform(
                        post("/api/logout")
                        .header(csrfTokenHeaderName, csrfToken)
                        .session(mockHttpSession)
                        //.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                    )
                    .andDo(document("logoutUserUsingPOST", preprocessResponse(prettyPrint())))
                    .andExpect(status().isOk())
                    .andReturn();


        // 탈퇴를 위해 인증코드 재발급 (세션 리플레시 체크)
        try{
            Thread.sleep(3000);

        }catch(Exception e) {

        }
        getCsrfTokenRestApiResponseBody = testGetCsrfTokenApiUsingMockMvc(mockHttpSession);

        csrfToken = getCsrfTokenRestApiResponseBody.getToken();
        csrfTokenParameterName = getCsrfTokenRestApiResponseBody.getParameterName();
        csrfTokenHeaderName = getCsrfTokenRestApiResponseBody.getHeaderName();

        log.info("!!! csrfToken: " + csrfToken);
        log.info("!!! csrfTokenHeaderName: " + csrfTokenHeaderName);
        result = this.mockMvc
                    .perform(
                        post("/api/issue-new-user-validation-key").with(csrf().asHeader())
                        //.header(csrfTokenHeaderName, csrfToken)
                        .session(mockHttpSession)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                        .param("mobilePhone", loggedInUser.getMobilePhone()))
                    .andDo(document("issueNewUserValidationKeyUsingPOST", preprocessResponse(prettyPrint())))
                    .andExpect(status().isOk())
                    .andReturn();

        UserValidationKey newUserValidationKey = userValidationKeyManager.getById(loggedInUser.getId());
        Assert.assertNotNull(newUserValidationKey);
        Assert.assertNotEquals(newUserValidationKey.getValue(), userValidationKey.getValue());

        // 탈퇴
        //
        getCsrfTokenRestApiResponseBody = testGetCsrfTokenApiUsingMockMvc(mockHttpSession);

        csrfToken = getCsrfTokenRestApiResponseBody.getToken();
        csrfTokenParameterName = getCsrfTokenRestApiResponseBody.getParameterName();
        csrfTokenHeaderName = getCsrfTokenRestApiResponseBody.getHeaderName();

        result = this.mockMvc
                    .perform(
                        post("/api/user/withdraw").with(csrf().asHeader())
                        //.header(csrfTokenHeaderName, csrfToken)
                        .session(mockHttpSession)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                        .param("id", loggedInUser.getId().toString())
                        .param("password", newUserValidationKey.getValue()))
                    .andDo(document("withdrawUserUsingPOST", preprocessResponse(prettyPrint())))
                    .andExpect(status().isOk())
                    .andReturn();

        contentAsString = result.getResponse().getContentAsString();
        WithdrawUserRestApiResponseBody withdrawUserRestApiResponseBody 
            = mapper.readValue(contentAsString, WithdrawUserRestApiResponseBody.class);

        User withdrawnUser = withdrawUserRestApiResponseBody.getUser();
        Assert.assertNotNull(withdrawnUser);
        Assert.assertEquals(Short.valueOf(User.Status.WITHDRAWN), withdrawnUser.getStatus());

    }

}

