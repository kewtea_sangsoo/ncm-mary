package com.ncmmary.test;
import com.ncmmary.Application;
import com.ncmmary.core.Clock;
import com.ncmmary.config.Swagger2SpringBootConfig;
import com.ncmmary.entity.User;
import com.ncmmary.entity.IdGenerator;
import com.ncmmary.entity.*;
import com.ncmmary.entitymanager.UserManager;
import com.ncmmary.controller.UserController.*;
import com.ncmmary.controller.AssetController.*;
import com.ncmmary.controller.SysLogController.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.core.env.Environment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import java.io.BufferedWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;


import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentationConfigurer;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.springframework.test.web.servlet.setup.MockMvcConfigurer;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.*;
import org.springframework.test.web.servlet.htmlunit.webdriver.MockMvcHtmlUnitDriverBuilder;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@TestPropertySource(locations="classpath:/config/application.properties")
public class ITSwagger2MarkupTest {

    private static final Logger LOG = LoggerFactory.getLogger(ITSwagger2MarkupTest.class);

    private WebDriver driver;
    private MockMvc mockMvc;

    @Autowired
    private Clock clock;

    @Autowired
    private Environment env;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    private UserManager userManager;

    @Rule
    public JUnitRestDocumentation restDocumentation
        = new JUnitRestDocumentation("target/asciidoc/snippets");

    @Before
    public void setup() throws Exception
    {
        MockMvcRestDocumentationConfigurer mockMvcRestDocumentationConfigurer
            = documentationConfiguration(this.restDocumentation);
        
        MockMvcConfigurer mockMvcOperationPreprocessorsConfigurer 
            = mockMvcRestDocumentationConfigurer.operationPreprocessors().withResponseDefaults(prettyPrint());
        MockMvcConfigurer uriConfigurer = mockMvcRestDocumentationConfigurer.uris().withHost(env.getProperty("ncmmary.api.host"));

        this.mockMvc 
            = MockMvcBuilders.webAppContextSetup(this.wac)
                /*.apply(springSecurity())*/
                .apply(mockMvcOperationPreprocessorsConfigurer)
                .apply(uriConfigurer)
                //.alwaysDo(document("{methodName}",
                 //   preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint())))
                .build();

        //this.driver = MockMvcHtmlUnitDriverBuilder.webAppContextSetup(this.wac, springSecurity()).build();
    }

    @Test
    public void registerUser() throws Exception {

        String email = "hosuk2@dmalt.co";
        String mobilePhone = "+821068930217";
        String password = "1234";
        String passwordConfirm = password;
        String appInstanceId = "app-instance-id-" + idGenerator.genId();
        MvcResult result
            = this.mockMvc.perform(post("/api/user/register")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .param("email", email)
                .param("mobilePhone", mobilePhone)
                .param("password", password)
                .param("passwordConfirm", passwordConfirm)
                .param("appInstanceId", appInstanceId))
                .andDo(document("registerUserUsingPOST", preprocessResponse(prettyPrint())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        RegisterUserRestApiResponseBody responseBody
            = mapper.readValue(
                contentAsString, 
                RegisterUserRestApiResponseBody.class);
        User user = responseBody.getUser();
        Assert.assertNotNull(user);
        
        this.mockMvc.perform(post("/api/user/update")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
            .param("id", user.getId().toString())
            .param("passwordModified", Boolean.toString(true)))
            .andDo(document("updateUserUsingPOST", preprocessResponse(prettyPrint())))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andReturn();

        this.mockMvc.perform(get("/api/users")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(document("getUsersUsingGET", preprocessResponse(prettyPrint())))
                .andExpect(status().isOk());

        /*
        this.mockMvc.perform(post("/api/login")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .param("username", email)
                .param("password", password))
                //.andDo(document("loginUserUsingPOST", preprocessResponse(prettyPrint())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();

        this.mockMvc.perform(get("/api/me"))
                .andDo(document("meUsingGET", preprocessResponse(prettyPrint())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();
        */

        this.mockMvc.perform(post("/api/user/delete")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                    .param("userIds", user.getId().toString()))
                .andDo(document("deleteUsersUsingPOST", preprocessResponse(prettyPrint())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();

        user = userManager.getById(user.getId());
        Assert.assertEquals(Short.valueOf(User.Status.DELETED), user.getStatus());
        
    }

    @Test
    public void testAssetApis() throws Exception {

        Asset asset = null;

        final Double LAT = 37.50;
        final Double LON = 127.5;
        final Double r = 2000.0; // 2000 M
        final int N = 100;

        for (int i=0; i<N; i++) {
            Random random = new Random(clock.getTime());
            Double lat = LAT + random.nextDouble() * 3;
            Double lon = LON + random.nextDouble() / 2;
            
            MvcResult result
                = this.mockMvc.perform(post("/api/asset/register")
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                    .param("type", Short.toString(Asset.Type.BIKE))
                    .param("latitude", lat.toString())
                    .param("longitude", lon.toString()))
                    .andDo(document("registerAssetUsingPOST", preprocessResponse(prettyPrint())))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                    .andReturn();

            String contentAsString = result.getResponse().getContentAsString();
            ObjectMapper mapper = new ObjectMapper();
            RegisterAssetRestApiResponseBody responseBody 
                = mapper.readValue(
                    contentAsString, 
                    RegisterAssetRestApiResponseBody.class);
            asset = responseBody.getAsset();
            Assert.assertNotNull(asset);
        }

        this.mockMvc.perform(post("/api/asset/update")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .param("id", asset.getId().toString())
                .param("type", Short.toString(Asset.Type.TRACKER))
                .param("typeModified", Boolean.toString(true))
                .param("status", Short.toString(Asset.Status.UNAVAILABLE))
                .param("statusModified", Boolean.toString(true))
                .param("longitudeModified", Boolean.toString(false))
                .param("latitude", LAT.toString())
                .param("latitudeModified", Boolean.toString(true))
                .param("hasBatteryModified", Boolean.toString(true))
                .param("hasBattery", Boolean.toString(true))
                .param("batteryModified", Boolean.toString(true))
                .param("battery", "75.5"))
                .andDo(document("updateAssetUsingPOST", preprocessResponse(prettyPrint())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();

        this.mockMvc.perform(get("/api/assets")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .param("t", Short.toString(Asset.Type.BIKE))
                .param("lon", LON.toString())
                .param("lat", LAT.toString())
                .param("r", r.toString()))
                .andDo(document("getAssetsUsingGET", preprocessResponse(prettyPrint())))
                .andExpect(status().isOk());
    }

    @Test
    public void registerSysLog() throws Exception {

        Random random = new Random(clock.getTime());

        final Double LAT = 37.50;
        final Double LON = 127.5;

        String targetDeviceId = idGenerator.genId();
        String moduleId = idGenerator.genId();
        Double power = 60.0;
        Double longitude = LON + random.nextDouble() / 2;
        Double latitude = LAT + random.nextDouble() * 3;
        Long time = clock.getTime() - (random.nextInt() % 10000);

        MvcResult result 
            = this.mockMvc.perform(post("/api/syslog/register")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .param("type", Short.toString(SysLog.Type.FOO))
                .param("targetDeviceId", targetDeviceId)
                .param("moduleId", moduleId)
                .param("power", Double.toString(power))
                .param("longitude", Double.toString(longitude))
                .param("latitude", Double.toString(latitude))
                .param("time", Long.toString(time)))
                .andDo(document("registerSysLogUsingPOST", preprocessResponse(prettyPrint())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        RegisterSysLogRestApiResponseBody responseBody 
            = mapper.readValue(
                contentAsString, 
                RegisterSysLogRestApiResponseBody.class);
        SysLog sysLog = responseBody.getSysLog();
        Assert.assertNotNull(sysLog);

        this.mockMvc.perform(post("/api/syslog/update")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .param("id", sysLog.getId().toString())
                .param("type", Short.toString(SysLog.Type.FOO))
                .param("typeModified", Boolean.toString(true)))
                .andDo(document("updateSysLogUsingPOST", preprocessResponse(prettyPrint())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();

        this.mockMvc.perform(get("/api/syslogs").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andDo(document("getSysLogsUsingGET", preprocessResponse(prettyPrint())))
            .andExpect(status().isOk());

    }

    @Test
    public void createSpringfoxSwaggerJson() throws Exception {
        String outputDir = System.getProperty("io.springfox.staticdocs.outputDir");
        MvcResult mvcResult = this.mockMvc.perform(get("/v2/api-docs?group=all"))
                .andExpect(status().isOk())
                .andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        String swaggerJson = response.getContentAsString();
        Files.createDirectories(Paths.get(outputDir));
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(outputDir, "swagger.json"), StandardCharsets.UTF_8)){
            writer.write(swaggerJson);
        }
    }
}
