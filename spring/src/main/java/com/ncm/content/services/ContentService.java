package com.ncm.content.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ncm.content.models.Content;
import com.ncm.content.repositories.ContentRepository;

@Service
public class ContentService {
	
	@Autowired
	ContentRepository contentRepo;
	
	//
	
	public List<Content> getAllContents() {
		return contentRepo.findAll();
	}
	
//	public List<Content> getContentsByUserId(long userId) {
//		return contentRepo.findByUserId(userId);
//	}
	
	public Content updateContent(Content newContent) {
		return contentRepo.save(newContent);
	}
	
	public void deleteContent(long contentId) throws Exception {
		contentRepo.delete(contentId);
	}

}
