package com.ncm.content.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "contents")
public class Content {

	public Content() {}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;

	@Column
	public String type; // cube, bike, ...

	@Column(name="request_type")
	public String requestType; // door_open, door_close, ... 

	@Column
	public long status; // 0: requested , 1: completed

	@Column(name="created_time")
	public long createdTime;

	@Column(name="last_modified_time")
	public long lastModifiedTime;
	
	@Column(name="read_count")
	public long readCount;

}
