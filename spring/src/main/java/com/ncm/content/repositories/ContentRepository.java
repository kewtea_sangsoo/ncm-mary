package com.ncm.content.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ncm.content.models.Content;

@Repository
public interface ContentRepository extends JpaRepository<Content, Long> {

	// by id
	Content findById(long id);

	// by "cube", "bike", ...
	List<Content> findByType(String type);

}
