package com.ncm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.ncm.content.models.Content;
import com.ncm.content.repositories.ContentRepository;

@Component
public class TestDataLoader implements ApplicationListener<ContextRefreshedEvent> {
	
	@Autowired
	private Environment environment;
	
	@Autowired
	ContentRepository contentRepo;
	
	//
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		
		System.out.println("Test == = = = == = = = ");
		System.out.println("Test == = = = == = = = ");
		System.out.println("Test == = = = == = = = ");
		System.out.println("Test == = = = == = = = ");
		System.out.println("Test == = = = == = = = ");
		System.out.println("Test == = = = == = = = ");
		System.out.println("Test == = = = == = = = ");
		System.out.println("Test == = = = == = = = ");
		System.out.println("Test == = = = == = = = ");
		
		// 무조건 로딩하는 관리자 계정
//		User admin = userRepo.findByEmail("eow.jongwook0724@gmail.com");
//		if (admin == null) {
//			admin = new User();
//			admin.email = "eow.jongwook0724@gmail.com";
//			admin.password = passwordEncoder.encode("dlatl@2018");
//			admin.userType = "admin";
////			admin.phoneNumber = "";
//			admin = userRepo.save(admin);
//		}
		
//		boolean isDev = false;
//		for(String profile : environment.getActiveProfiles()){
//            if (profile.equals("dev")) isDev = true;
//        }
//		if (!isDev) return;
		
		//
		
		Content content = new Content();
		content.type = "cube";
		content.requestType = "door_open_1";
		content.status = 0L;
		content.createdTime = System.currentTimeMillis();
		content.lastModifiedTime = System.currentTimeMillis();
		content = contentRepo.save(content);
		System.out.println(content.type);
		
	}
	
}
