// https://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 6
  },
  env: {
    browser: true,
    jquery: true
  },
  // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
  // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
  extends: ['plugin:vue/essential', 'eslint:recommended'],
  // required to lint *.vue files
  plugins: [
    'vue',
    'jquery'
  ],
  // check if imports actually resolve
  settings: {
    'import/resolver': {
      webpack: {
        config: 'build/webpack.base.conf.js'
      }
    }
  },
  // add your custom rules here
  rules: {
    // don't require .vue extension when importing
    // 'import/extensions': ['error', 'always', {
    //   js: 'never',
    //   vue: 'never'
    // }],
    // disallow reassignment of function parameters
    // disallow parameter object manipulation except for specific exclusions
    'no-param-reassign': ['error', {
      props: true,
      ignorePropertyModificationsFor: [
        'state', // for vuex state
        'acc', // for reduce accumulators
        'e' // for e.returnvalue
      ]
    }],
    // allow optionalDependencies
    // 'import/no-extraneous-dependencies': ['error', {
    //   optionalDependencies: ['test/unit/index.js']
    // }],
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // Custom
    'indent': ['error', 4, {
        'MemberExpression': 'off',
        'FunctionDeclaration': { 'parameters': 'off' },
        'SwitchCase': 1
    }],
    'linebreak-style': ['error', 'unix'],
    'quotes': ['error', 'single'],
    'semi': ['error', 'always'],
    'comma-dangle': 'off',
    'no-cond-assign': ['error', 'always'],
    'no-console': 'off',
    'no-use-before-define': 'off',
    'space-before-function-paren': ['error', 'never'],
    'max-len': ['error', {'code': 100, 'ignoreComments': true}],
    'keyword-spacing': ['error', {
        'overrides': {
          'catch': { 'before': true, 'after': false }
        }
      }
    ],
    'space-before-blocks': ['error', 'always'],
    'comma-spacing': ['error', { 'before': false, 'after': true }],
    'key-spacing': ['error', {}],
    'object-curly-spacing': ['error', 'always', {'arraysInObjects': true}],
    'camelcase': 'error',
    'func-call-spacing': ['error', 'never'],
    'no-var': 'off',
    'no-unused-vars': 'off',
    'no-useless-concat': 'error',
    'semi-style': ['error', 'last'],
    'semi-spacing': ['error', { 'before': false, 'after': true }],
    'init-declarations': ['error', 'always'],
    'no-multi-spaces': 'error',
    'func-names': 'off',
    'prefer-template': 'off',
    'prefer-const': 'off',
    'prefer-arrow-callback': 'off',
    'space-infix-ops': 'off',
  },
  globals: {
    'google': true,
  }
}
