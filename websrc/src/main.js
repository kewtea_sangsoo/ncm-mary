// <script src="https://cdn.jsdelivr.net/npm/sockjs-client@1/dist/sockjs.min.js"></script>
// <script src="lib/stomp.min.js"></script>

// import SockJS from 'sockjs';
// require('sockjs');

import Vue from 'vue';
import VueRouter from 'vue-router';
import 'expose-loader?$!expose-loader?jQuery!jquery';

import App from './App.vue';

import Index from './views/Index.vue';
import Home from './views/Home.vue';
import Intro from './views/Intro.vue';
import Cube from './views/Cube.vue';
import Bike from './views/Bike.vue';
import Kick from './views/Kick.vue';
import Map from './views/Map.vue';

const routes = [
    { path: '/', component: Index },
    { path: '/home', component: Home },
    { path: '/intro', component: Intro },
    { path: '/cube', component: Cube },
    { path: '/bike', component: Bike },
    { path: '/kick', component: Kick },
    { path: '/map', component: Map },
];
const router = new VueRouter({
    mode: 'history',
    routes,
});

Vue.config.productionTip = false;
Vue.use(VueRouter);

new Vue({
    components: { App },
    template: '<App/>',
    router,
}).$mount('#app');
